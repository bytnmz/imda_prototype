const PACKAGE = require('./package.json');
let assetPath = PACKAGE.assetPath;

const path = require('path');
const fs = require('fs')
const webpack = require('webpack');
const multiJsonLoader = require('multi-json-loader');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

console.log('Running production build......');

// Create page data
const siteData = multiJsonLoader.loadFiles('./src/_data');

// Recursively created nested JSON object based on the folder structure in the folder
generateJSON('./src/_data', siteData);

function generateJSON(startPath, parentObj) {
  const files = fs.readdirSync(startPath);

  for(let i = 0; i < files.length; i++){
    const filename = path.join(startPath,files[i]);
    const stat = fs.lstatSync(filename);

    if (stat.isDirectory()){
      parentObj[`${files[i]}`] = multiJsonLoader.loadFiles(filename);
      generateJSON(filename, parentObj[`${files[i]}`]);
    }
  }
}

function getIndexPugFiles(startPath,filter){
  let results = [];

  if (!fs.existsSync(startPath)){
      console.log("no dir ",startPath);
      return;
  }

  const files = fs.readdirSync(startPath);
  for(let i = 0; i < files.length; i++){
    const filename = path.join(startPath,files[i]);
    const stat = fs.lstatSync(filename);
    if (stat.isDirectory()){
      // recursively created nested array of file names
      results = results.concat(getIndexPugFiles(filename,filter));
    }
    else if ((filename.indexOf(filter) >= 0) && (filename.indexOf('_modules') === -1) && (filename.indexOf('_layouts') === -1)) {
      let actualFilename = filename.replace('src/','');
      actualFilename = actualFilename.replace(/src\\/g, '');
      results.push(actualFilename);
    }
  }
  return results;
}

function generateHtmlPlugins(templateDir) {
  // Read files in template directory
  const templateFiles = getIndexPugFiles(templateDir,'.pug');

  return templateFiles.map(item => {
    // Split names and extension
    const parts = item.split('.');
    const name = parts[0]
    const extension = parts[1];

    // Create new HTMLWebpackPlugin with options
    return new HtmlWebpackPlugin({
      filename: `${name}.html`,
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
      cache: true,
      minify: false,
      hash: false,
      inject: false,
      alwaysWriteToDisk: true,
      data: siteData
    })
  })
}

const htmlPlugins = generateHtmlPlugins('./src');

module.exports = (env) => {
  let copy;
  if(env.deploy) {
    assetPath = PACKAGE.deploymentPath;
    copy = [
      {from:'src/_icomoon/fonts',to:`${assetPath}/fonts`},
      {from:'**/*',ignore: ['Cwp/**/*', '{**/\_*,**/\_*/**}','**/*.pug'],context: 'src/'}
    ];
    console.log('Running build for deployment......');
  }
  else {
    copy = [
      {from:'src/_icomoon/fonts',to:`${assetPath}/fonts`},
      {from:'src/_images',to:`${assetPath}/images`},
      {from:'**/*',ignore: ['Cwp/**/*','{**/\_*,**/\_*/**}','**/*.pug'],context: 'src/'}
    ];
  }

  const config = {
    mode: process.env.NODE_ENV,
    entry: {
      "main": path.resolve(__dirname, 'index.js'),
      "main-microsite": path.resolve(__dirname, 'index-microsite.js')
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: `${assetPath}/scripts/[name].js`,
      publicPath: "/"
    },
    plugins: [
      new CleanWebpackPlugin(),
      new CopyWebpackPlugin(copy),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        $j: 'jquery'
      }),
      new MiniCssExtractPlugin({
        filename: `${assetPath}/styles/[name].css`
      }),
      new ImageminPlugin({
        test: /\.(jpe?g|png|gif|svg)$/i,
        plugins: [
          imageminMozjpeg({
            quality: 70,
            progressive: true
          })
        ]
      })
    ].concat(htmlPlugins),
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: "babel-loader",
            options: {
              cacheDirectory: true
            }
          }
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                url: false
              }
            },
            'postcss-loader',
            'fast-sass-loader',
          ],
        },
        {
          test: /\.pug$/,
          use: {
            loader: "pug-loader",
            options: {
              pretty: true
            }
          }
        }
      ]
    },
    resolve: {
      modules: [
        "node_modules"
      ],
      alias: {
        'vue$': 'vue/dist/vue.min.js',
        src: path.resolve(__dirname, 'src')
      },
      extensions: ['*', '.js', '.vue', '.json']
    }
  }

  if(env.deploy) {
    config.optimization = {
      minimizer: [
        new UglifyJsPlugin({
          test: /\.js(\?.*)?$/i,
          extractComments: true,
          sourceMap: true
        })
      ]
    }
  }

  return config;
}
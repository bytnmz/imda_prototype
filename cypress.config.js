module.exports = {
  // The rest of the Cypress config options go here...
  projectId: "besa9e",

  experimentalWebKitSupport: true,
  e2e: {
    baseUrl: 'https://apsdadvedgwap90proto.azurewebsites.net/',
    // baseUrl: 'http://localhost:3000/',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
};

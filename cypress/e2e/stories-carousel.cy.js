describe('Page loads', () => {
  it('Loading the home page', () => {
    cy.visit('/business');
  });
});

// Desktop test cases
describe('Stories Carousel Behavior', () => {
  it('Carousel loops infinitely', () => {
    cy.viewport(1280, 768);

    cy.wait(500);

    cy.scrollTo(0, 2300);

    // Prev arrow enabled on load
    cy.get('.stories-carousel .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');

    // Click on next button, go to slide 2
    cy.get('.stories-carousel .slick-next').click({
      scrollBehavior: false
    });
    cy.get('.stories-carousel [data-slick-index="1"]').should('have.class', 'slick-active');
    // cy.get('.stories-carousel .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');

    // cy.wait(1500);

    // // Click on next button, go to slide 3
    // cy.get('.stories-carousel .slick-next').click({
    //   scrollBehavior: false
    // });
    // cy.get('.stories-carousel [data-slick-index="2"]').should('have.class', 'slick-active');
    // cy.get('.stories-carousel .slick-next').should('have.class', 'slick-disabled').should('have.css', 'cursor', 'not-allowed');

    // cy.wait(1500);

    // // Click on prev button, go to slide 2
    // cy.get('.stories-carousel .slick-prev').click({
    //   scrollBehavior: false
    // });
    // cy.get('.stories-carousel [data-slick-index="1"]').should('have.class', 'slick-active');
    // cy.get('.stories-carousel .slick-next').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');
  });
});
describe('Page loads', () => {
  it('Loading the home page', () => {
    cy.visit('/business');
  });
});

// Desktop and mobile test cases
describe('Tab functionality', () => {
  it('Tab switches properly', () => {
    cy.viewport(1024, 768);

    cy.wait(500);
    
    cy.scrollTo(0, 1000);
    
    // active state is set
    cy.get('.home-tabs__nav nav a:first-child').should('have.class', 'active');
    cy.get('.articles-with-questionnaire').should('be.visible');
    cy.get('.article-gallery-carousel').should('not.be.visible');
    cy.get('.digital-blocks').should('not.be.visible');
    
    cy.get('.home-tabs__nav nav a:nth-child(2)').click({
      scrollBehavior: false
    });
    
    cy.get('.home-tabs__nav .ui-handle').invoke('attr', 'style').should('eq', 'left: 33.3333%;');
    cy.get('.articles-with-questionnaire').should('not.be.visible');
    cy.get('.article-gallery-carousel').should('be.visible');
    cy.get('.digital-blocks').should('not.be.visible');
  });
});

describe('Article teaser hidden', () => {
  it('Article teaser aragraph hidden in mobile', () => {
    cy.viewport('iphone-xr');

    cy.get('.article-gallery-carousel__texts [data-slick-index="0"] .article-card__text p').should('not.be.visible');
  });
});

// Desktop and mobile test cases
describe('Article Carousel Behavior', () => {
  it('Carousel loops infinitely', () => {
    cy.viewport(1024, 1200);

    cy.wait(1500);

    cy.scrollTo(0, 1000);

    // Prev arrow enabled on load
    cy.get('.article-gallery-carousel .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');

    // Click on next button, go to slide 2
    cy.get('.article-gallery-carousel .slick-next').click({
      scrollBehavior: false
    });

    cy.get('.article-gallery-carousel__texts [data-slick-index="1"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel__images [data-slick-index="1"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');

    // cy.wait(1500);

    // // Click on next button, go to slide 3
    // cy.get('.article-gallery-carousel .slick-next').click({
    //   scrollBehavior: false
    // });
    // cy.get('.article-gallery-carousel__texts [data-slick-index="2"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel__images [data-slick-index="2"]').should('have.class', 'slick-active');

    // cy.wait(1500);

    // // Click on next button, go to slide 4
    // cy.get('.article-gallery-carousel .slick-next').click({
    //   scrollBehavior: false
    // });
    // cy.get('.article-gallery-carousel__texts [data-slick-index="3"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel__images [data-slick-index="3"]').should('have.class', 'slick-active');

    // cy.wait(1500);
    
    // // Click on next button, go to slide 5
    // cy.get('.article-gallery-carousel .slick-next').click({
    //   scrollBehavior: false
    // });
    // cy.get('.article-gallery-carousel__texts [data-slick-index="4"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel__images [data-slick-index="4"]').should('have.class', 'slick-active');

    // cy.wait(1500);
    
    // // Click on next button, go to slide 6
    // cy.get('.article-gallery-carousel .slick-next').click({
    //   scrollBehavior: false
    // });
    // cy.get('.article-gallery-carousel__texts [data-slick-index="5"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel__images [data-slick-index="5"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel .slick-next').should('have.class', 'slick-disabled').should('have.css', 'cursor', 'not-allowed');

    // cy.wait(1500);

    // // Click on prev button, go to slide 3
    // cy.get('.article-gallery-carousel .slick-prev').click({
    //   scrollBehavior: false
    // });
    // cy.get('.article-gallery-carousel__texts [data-slick-index="4"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel__images [data-slick-index="4"]').should('have.class', 'slick-active');
    // cy.get('.article-gallery-carousel .slick-next').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');
  });
});
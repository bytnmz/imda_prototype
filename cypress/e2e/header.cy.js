describe('Page loads', () => {
  it('Loading the home page', () => {
    cy.visit('/business');
  });
});

// desktop test cases
describe('Header opacity', () => {
  it('Header turns white on scroll', () => {
    // cy.wait(1000);
    cy.viewport(1024, 768);

    cy.scrollTo(0, 51);

    cy.get('.site-header').should('not.have.class', 'site-header--top');

    cy.scrollTo(0, 0);

    cy.get('.site-header').should('have.class', 'site-header--top');
  });
});

describe('Search bar toggle', () => {
  it('Search opens/closes on button click', () => {
    // cy.wait(1000);
    cy.viewport(1024, 768);

    cy.get('.btn-toggle-search').click({
      scrollBehavior: false
    });
    cy.get('.site-search__input').should('be.visible');
    cy.get('.site-header').should('not.have.class', 'site-header--top');

    cy.get('.btn-toggle-search').click({
      scrollBehavior: false
    });
    cy.get('.site-search__input').should('not.be.visible');
    cy.get('.site-header').should('have.class', 'site-header--top');
  });
});

describe('Search bar validation', () => {
  it('Search input shows error if empty', () => {
    // cy.wait(1000);
    cy.viewport(1024, 768);

    cy.get('.btn-toggle-search').click({
      scrollBehavior: false
    });
    cy.get('.site-search__input').should('be.visible');

    // test validation input
    cy.get('.site-search__form .btn-search').click({
      scrollBehavior: false
    });
    cy.get('.site-search__form .error-msg').should('be.visible');
    cy.get('.site-search__form input').type('hello');
    cy.get('.site-search__form .btn-search').type('{enter}');

    cy.url().should('include', '/search-results');

    cy.visit('/business');
  });
});

describe('Profile navigation toggle', () => {
  it('Profile dropdown opens/closes on click', () => {
    // cy.wait(1000);
    cy.viewport(1024, 768);

    // open the dropdown
    cy.get('.site-header__profile button').click({
      scrollBehavior: false
    });
    cy.get('.site-header__profile nav').should('be.visible');

    // close the dropdown
    cy.get('.site-header__profile button').click({
      scrollBehavior: false
    });
    cy.get('.site-header__profile nav').should('not.be.visible');
  });
});

// mobile test cases
describe('Mobile menu toggle', () => {
  it('Mobile nav opens/closes on button click', () => {
    // cy.wait(1000);
    cy.viewport('iphone-xr');

    cy.get('.btn-toggle-menu').should('be.visible');
    
    // open menu
    cy.get('.btn-toggle-menu').click({
      scrollBehavior: false
    });
    cy.get('.site-header').should('not.have.class', 'site-header--top');
    cy.get('.site-header__inner').should('be.visible');
    
    // test profile dropdown
    cy.get('.site-header__profile button').should('be.visible');
    
    // open the dropdown
    cy.get('.site-header__profile button').click({
      scrollBehavior: false
    });
    cy.get('.site-header__profile nav').should('be.visible');
    
    // close the dropdown
    cy.get('.site-header__profile button').click({
      scrollBehavior: false
    });
    cy.get('.site-header__profile nav').should('not.be.visible');

    // close menu
    cy.get('.btn-toggle-menu').click({
      scrollBehavior: false
    });
    cy.get('.site-header').should('have.class', 'site-header--top');
    cy.get('.site-header__inner').should('not.be.visible');
  });
});
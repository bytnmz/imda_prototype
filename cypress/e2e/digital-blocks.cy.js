describe('Page loads', () => {
  it('Loading the home page', () => {
    cy.visit('/business');
  });
});

// Desktop and mobile test cases
describe('Tab functionality', () => {
  it('Tab switches properly', () => {
    cy.viewport(1024, 768);

    cy.wait(500);
    
    cy.scrollTo(0, 1000);
    
    // active state is set
    cy.get('.home-tabs__nav nav a:first-child').should('have.class', 'active');
    cy.get('.articles-with-questionnaire').should('be.visible');
    cy.get('.article-gallery-carousel').should('not.be.visible');
    cy.get('.digital-blocks').should('not.be.visible');
    
    cy.get('.home-tabs__nav nav a:nth-child(3)').click({
      scrollBehavior: false
    });
    
    cy.get('.home-tabs__nav .ui-handle').invoke('attr', 'style').should('eq', 'left: 66.6667%;');
    cy.get('.articles-with-questionnaire').should('not.be.visible');
    cy.get('.article-gallery-carousel').should('not.be.visible');
    cy.get('.digital-blocks').should('be.visible');
  });
});

describe('Digital Blocks Carousel Behavior', () => {
  it('Button states should change with carousel navigation to show non infinite carousel', () => {
    cy.viewport(1024, 768);

    cy.wait(500);

    cy.scrollTo(0,1500);

    // Prev arrow enabled on load
    cy.get('.digital-blocks .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');

    // Click on next button, go to slide 2
    cy.get('.digital-blocks .slick-next').click({
      scrollBehavior: false
    });
    cy.get('.digital-blocks [data-slick-index="1"]').should('have.class', 'slick-active');
    // cy.get('.digital-blocks .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');

    // cy.wait(500);

    // // Click on next button, go to slide 3
    // cy.get('.digital-blocks .slick-next').click({
    //   scrollBehavior: false
    // });
    // cy.get('.digital-blocks [data-slick-index="2"]').should('have.class', 'slick-active');
    // cy.get('.digital-blocks .slick-next').should('have.class', 'slick-disabled').should('have.css', 'cursor', 'not-allowed');

    // cy.wait(500);

    // // Click on prev button, go to slide 2
    // cy.get('.digital-blocks .slick-prev').click({
    //   scrollBehavior: false
    // });
    // cy.get('.digital-blocks [data-slick-index="1"]').should('have.class', 'slick-active');
    // cy.get('.digital-blocks .slick-next').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');
  });
});
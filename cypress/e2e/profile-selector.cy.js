describe('Page loads', () => {
  it('Loading the landing page', () => {
    cy.visit('/');
  });
});

// desktop test cases
describe('Desktop: Profile hover interactions', () => {
  it('Hovering on the profile link', () => {
    // cy.wait(1000);

    cy.viewport(1024, 768);
    
    cy.get('[data-info="#profile1"]').trigger('mouseover');

    // active state set
    cy.get('[data-info="#profile1"]').parent().should('have.class', 'active');

    // profile description active
    cy.get('#profile1 .profile-selector__profiles__text').should('be.visible');
  });
});

// mobile test cases
describe('Mobile: Opening/closing profile guide modal', () => {
  it('Tapping on the info icon and button in modal', () => {
    // cy.wait(1000);

    cy.viewport('iphone-xr');
    
    // click on icon to open modal
    cy.get('.profile-selector__info').click({
      scrollBehavior: false
    });

    // profile description active
    cy.get('#profileGuide').should('be.visible');
    
    // aria hidden to false
    cy.get('#profileGuide').invoke('attr', 'aria-hidden').should('eq', 'false');

    // click on button to close modal
    cy.get('#profileGuide footer button').should('be.visible');
    cy.get('#profileGuide footer button').click({
      scrollBehavior: false
    });

    // profile description active
    cy.get('#profileGuide').should('not.be.visible');
    
    // aria hidden to false
    cy.get('#profileGuide').invoke('attr', 'aria-hidden').should('eq', 'true');
  });
});
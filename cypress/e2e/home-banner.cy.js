describe('Page loads', () => {
  it('Loading the home page', () => {
    cy.visit('/business');
  });
});

// Desktop and mobile test cases
describe('Banner Carousel Behavior', () => {
  it('Button states should change with carousel navigation to show non infinite carousel', () => {
    cy.viewport(1024, 768);

    cy.wait(500);

    // Prev arrow enabled on load
    cy.get('.home-banner .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');

    // Click on next button, go to slide 2
    cy.get('.home-banner .slick-next').click({
      scrollBehavior: false
    });
    cy.get('.home-banner [data-slick-index="1"]').should('have.class', 'slick-active');
    // cy.get('.home-banner .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');

    // cy.wait(500);

    // // Click on next button, go to slide 3
    // cy.get('.home-banner .slick-next').click({
    //   scrollBehavior: false
    // });
    // cy.get('.home-banner [data-slick-index="2"]').should('have.class', 'slick-active');

    // cy.wait(500);

    // // Click on next button, go to slide 4
    // cy.get('.home-banner .slick-next').click({
    //   scrollBehavior: false
    // });
    // cy.get('.home-banner [data-slick-index="3"]').should('have.class', 'slick-active');
    // cy.get('.home-banner .slick-next').should('have.class', 'slick-disabled').should('have.css', 'cursor', 'not-allowed');

    // cy.wait(500);

    // // Click on prev button, go to slide 3
    // cy.get('.home-banner .slick-prev').click({
    //   scrollBehavior: false
    // });
    // cy.get('.home-banner [data-slick-index="2"]').should('have.class', 'slick-active');
    // cy.get('.home-banner .slick-next').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');
  });
});
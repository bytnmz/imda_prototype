describe('Page loads', () => {
  it('Loading the home page', () => {
    cy.visit('/business');
  });
});

// desktop and mobile test cases
describe('Partners Carousel Behavior', () => {
  it('Carouesl loops infinitely', () => {
    cy.viewport(1024, 768);
    
    cy.scrollTo(0, 2800);
    
    // Prev buttons not disabled on load
    cy.get('.partners__group--subsidiary .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');
    cy.get('.partners__group--related-sites .slick-prev').should('not.have.class', 'slick-disabled').should('have.css', 'cursor', 'pointer');
    
    cy.get('.partners__group--subsidiary .slick-next').click({
      scrollBehavior: false
    });
    cy.get('.partners__group--related-sites .slick-next').click({
      scrollBehavior: false
    });

    // auto slide to second slide
    cy.get('.partners__group--subsidiary [data-slick-index="1"]').should('have.class', 'slick-active');
    cy.get('.partners__group--related-sites [data-slick-index="1"]').should('have.class', 'slick-active');
  });
});
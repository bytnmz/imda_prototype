describe('Page loads', () => {
  it('Loading the home page', () => {
    cy.visit('/business');
  });
});

// desktop and mobile test cases
describe('Image display', () => {
  it('Image is hidden in mobile and displayed in desktop', () => {
    cy.viewport(1024, 768);
    
    cy.get('.call-to-actions picture').should('be.visible');

    cy.viewport('iphone-xr');
    
    cy.get('.call-to-actions picture').should('not.be.visible');
  });
});
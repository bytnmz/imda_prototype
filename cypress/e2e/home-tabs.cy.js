describe('Page loads', () => {
  it('Loading the home page', () => {
    cy.visit('/business');
  });
});

// Desktop and mobile test cases
describe('Tab functionality', () => {
  it('Tab switches properly', () => {
    cy.viewport(1024, 768);

    cy.wait(500);
    
    cy.scrollTo(0, 1000);
    
    // active state is set
    cy.get('.home-tabs__nav nav a:first-child').should('have.class', 'active');
    cy.get('.articles-with-questionnaire').should('be.visible');
    cy.get('.article-gallery-carousel').should('not.be.visible');
    cy.get('.digital-blocks').should('not.be.visible');
    
    
    cy.get('.home-tabs__nav nav a:nth-child(2)').click({
      scrollBehavior: false
    });
    
    cy.get('.home-tabs__nav .ui-handle').invoke('attr', 'style').should('eq', 'left: 33.3333%;');
    cy.get('.articles-with-questionnaire').should('not.be.visible');
    cy.get('.article-gallery-carousel').should('be.visible');
    cy.get('.digital-blocks').should('not.be.visible');

    cy.get('.home-tabs__nav nav a:nth-child(3)').click({
      scrollBehavior: false
    });

    cy.get('.home-tabs__nav .ui-handle').invoke('attr', 'style').should('eq', 'left: 66.6667%;');
    cy.get('.articles-with-questionnaire').should('not.be.visible');
    cy.get('.article-gallery-carousel').should('not.be.visible');
    cy.get('.digital-blocks').should('be.visible');

    cy.get('.home-tabs__nav nav a:first-child').click({
      scrollBehavior: false
    });

    cy.get('.home-tabs__nav .ui-handle').invoke('attr', 'style').should('eq', 'left: 0%;');
    cy.get('.articles-with-questionnaire').should('be.visible');
    cy.get('.article-gallery-carousel').should('not.be.visible');
    cy.get('.digital-blocks').should('not.be.visible');
  });
});
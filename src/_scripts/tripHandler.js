import Trip from 'trip.js';
import Cookies from 'js-cookie';
import { setBodyPos, setBodyToFixed } from './helpers';

export function tripHandler() {
  const hash = window.location.hash;

  if (typeof steps !== 'undefined' && steps !== null) {
    let tripSteps = [];
    // if no hover and mobile tablet size
    if($(window).width() >= 1024) {
      tripSteps = [...steps];
    }
    else {
      tripSteps = (typeof mobileSteps !== 'undefined' && mobileSteps !== null) ? [...mobileSteps] : [...steps];
    }

    let tripStarted =  false;
    let closeOnResize = false;
    let tripEnded = false;
    // const itemName = !$('body').hasClass('is-homepage') ? `tripClosed${window.location.pathname.replace(/-/g, '')}` : 'tripClosed/homepage';
    const itemName = `tripClosed`;
    if(!localStorage.getItem(itemName) && !localStorage.getItem('visitedHome') && !Cookies.get('visited_homepage') && !Cookies.get('closed_trip')) {
      let options = {
        "delay": -1,
        "tripTheme": 'white',
        "tripBlockHTML": '<div class="trip-block"><a href="#" class="trip-close"></a><div class="trip-header"></div><div class="trip-content"></div><div class="trip-progress-steps"></div><div class="trip-navigation"><a class="trip-skip btn btn-primary btn-primary--inverse" href="#"></a><a class="trip-next btn btn-primary" href="#"></a></div><div class="trip-progress-bar"></div></div>',
        "showNavigation": true,
        "backToTopWhenEnded":  hash.length ? false:true,
        "enableAnimation": false,
        "nextLabel": "Next <svg height='48' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 84 48' style='enable-background:new 0 0 84 48;' xml:space='preserve'><g id='line'><line class='arrow-body' fill='none' stroke-width='4.8' stroke-miterlimit='10' x1='0' y1='23.8' x2='65.4' y2='23.8' pathLength='1'></line></g><g id='head'><path class='arrow-head' fill='none' stroke-width='4.8' stroke-miterlimit='10' d='M59.2,45.6L79.6,24L59.2,2.4' pathLength='1'></path></g></svg>",
        "skipLabel": "Skip tour <svg height='48' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 84 48' style='enable-background:new 0 0 84 48;' xml:space='preserve'><g id='line'><line class='arrow-body' fill='none' stroke-width='4.8' stroke-miterlimit='10' x1='0' y1='23.8' x2='65.4' y2='23.8' pathLength='1'></line></g><g id='head'><path class='arrow-head' fill='none' stroke-width='4.8' stroke-miterlimit='10' d='M59.2,45.6L79.6,24L59.2,2.4' pathLength='1'></path></g></svg>",
        "finishLabel": $('.profile-selector').length ? "Done <svg height='48' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 84 48' style='enable-background:new 0 0 84 48;' xml:space='preserve'><g id='line'><line class='arrow-body' fill='none' stroke-width='4.8' stroke-miterlimit='10' x1='0' y1='23.8' x2='65.4' y2='23.8' pathLength='1'></line></g><g id='head'><path class='arrow-head' fill='none' stroke-width='4.8' stroke-miterlimit='10' d='M59.2,45.6L79.6,24L59.2,2.4' pathLength='1'></path></g></svg>" : "Ok, got it <svg height='48' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 84 48' style='enable-background:new 0 0 84 48;' xml:space='preserve'><g id='line'><line class='arrow-body' fill='none' stroke-width='4.8' stroke-miterlimit='10' x1='0' y1='23.8' x2='65.4' y2='23.8' pathLength='1'></line></g><g id='head'><path class='arrow-head' fill='none' stroke-width='4.8' stroke-miterlimit='10' d='M59.2,45.6L79.6,24L59.2,2.4' pathLength='1'></path></g></svg>",
        "onTripChange": (tripIndex, tripObject) => {
          if (tripIndex == tripSteps.length - 1) {
            $('.trip-skip').hide();
          }

          if(tripIndex === 0) {
            $('.site-header').addClass('trip-active');
            
            if($('.site-header').length && $(window).width() < 1024) {
              $('.site-header').removeClass('site-header--top');
            }
          }

          if(parseInt($('.trip-block').css('left').replace('px', '')) < 10) {
            $('.trip-block').removeClass('pin-right');
            $('.trip-block').css('left', '10px');
            $('.trip-block').css('right', 'auto');
            $('.trip-block').addClass('pin-left');
          }
          else {
            $('.trip-block').removeClass('pin-left');
          }

          if(!$('.trip-block').hasClass('pin-left')) {
            if(parseInt($('.trip-block').css('left').replace('px', '')) + $('.trip-block').outerWidth() > $(window).width() - 10) {
              $('.trip-block').css('left', `auto`);
              $('.trip-block').css('right', '10px');
              $('.trip-block').addClass('pin-right');
            }
            else {
              $('.trip-block').removeClass('pin-right');
            }
          }
        },
        "onEnd": (tripIndex, tripObject) => {
          if(!closeOnResize) {
            localStorage.setItem(itemName, 'closed');
            Cookies.set('closed_trip', true, { expires: 365 });
            tripEnded = true;
          }

          if(tripIndex === 0) {
            if($('.site-header').length && $(window).width() < 1024) {
              $('.site-header').addClass('site-header--top');
              $('.site-header__inner').removeClass('active');
            }
          }

          if($('.site-header').hasClass('trip-active')) {
            $('.site-header').removeClass('trip-active');
          }
          
          setBodyPos();
          window.emitter.emit('tourEnded');
        },
        "onTripClose": (tripIndex, tripObject) => {
          if(!closeOnResize) {
            localStorage.setItem(itemName, 'closed');
            Cookies.set('closed_trip', true, { expires: 365 });
            tripEnded = true;
          }

          if(tripIndex === 0) {
            if($('.site-header').length && $(window).width() < 1024) {
              $('.site-header').addClass('site-header--top');
              $('.site-header__inner').removeClass('active');
            }
          }
          
          setBodyPos();
          window.emitter.emit('tourEnded');
        }
      };

      const trip = [];

      for(let i = 0; i < tripSteps.length; i++) {
        const step = {
          sel: $(`${tripSteps[i].element}`),
          content: `${tripSteps[i].content}`,
          expose: true,
          position: tripSteps[i].position
        }

        trip.push(step);
      }

      let tripJS = new Trip(trip, options);
      // avoid conflict with anchorNav
      setTimeout(()=>{
        tripJS.start();
        tripStarted = true;
        setBodyToFixed();
      },hash.length ? 700:400)
      
      let interval;
      $(window).on('resize', () => {
        if(!tripEnded) {
          clearTimeout(interval);

          interval = setTimeout(() => {
            if($(window).width() >= 1024) {
              tripSteps = [...steps];
            }
            else {
              tripSteps = (typeof mobileSteps !== 'undefined' && mobileSteps !== null) ? [...mobileSteps] : [...steps];
            }

            const newTrip = [];

            for(let i = 0; i < tripSteps.length; i++) {
              const step = {
                sel: $(`${tripSteps[i].element}`),
                content: `${tripSteps[i].content}`,
                expose: true,
                position: tripSteps[i].position
              }

              newTrip.push(step);
            }

            if(!tripStarted) {
              tripJS = new Trip(newTrip, options);
              tripJS.start();
              tripStarted = true;
            }
            else {
              closeOnResize = true;
              tripJS.stop();
              closeOnResize = false;
              tripStarted = false;
              tripJS = new Trip(newTrip, options);
              tripJS.start();
              tripStarted = true;
              setBodyToFixed();
            }
          }, 500);
        }
      });
    }
  }
}
// Main javascript entry point
// Should handle bootstrapping/starting application

"use strict";

import $ from "jquery";
import "jquery-match-height";
import Emitter from "tiny-emitter";
import tablesawJquery from "tablesaw/dist/tablesaw.jquery";

import { checkHover, setVh, viewportAnimate } from "./utils";
import { tripHandler } from "./tripHandler";
import { profileCookieHandler, userDataTrigger } from "./profileCookieHandler";

import CookieMessage from "../_modules/molecules/cookie-message/cookie-message";
import ImageCtaListing from "../_modules/molecules/image-cta-listing/image-cta-listing";
import ImageRte from "../_modules/molecules/image-rte/image-rte";
import TopicCardListing from "../_modules/molecules/topic-card-listing/topic-card-listing";
import SiteHeader from "../_modules/organisms/site-header/site-header";
import Partners from "../_modules/organisms/partners/partners";
import Gallery from "../_modules/organisms/gallery/gallery";
import StoryListing from "../_modules/organisms/story-listing/story-listing";
import ProgrammeListing from "../_modules/organisms/programme-listing/programme-listing";
import NewsListing from "../_modules/organisms/news-listing/news-listing";
import EventsListing from "../_modules/organisms/events-listing/events-listing";
import DetailAside from "../_modules/organisms/detail-aside/detail-aside";
import Featured from "../_modules/organisms/featured/featured";
import AnchorNav from "../_modules/organisms/anchor-nav/anchor-nav";
import RegulationListing from "../_modules/organisms/regulation-listing/regulation-listing";
import VideoPlayer from "../_modules/molecules/video-player/video-player";
import Newsroom from "../_modules/organisms/newsroom/newsroom";
import SearchResults from "../_modules/organisms/search-results/search-results";
import DigitalsolListing from "../_modules/organisms/digitalsol-listing/digitalsol-listing";
import AccordionList from "../_modules/organisms/accordion-list/accordion-list";
import StandardContentCarousel from "../_modules/organisms/standard-content-carousel/standard-content-carousel";
import StoriesCarousel from "../_modules/organisms/stories-carousel/stories-carousel";
import DigitalBlocks from "../_modules/organisms/digital-blocks/digital-blocks";
import ArticleGalleryCarousel from "../_modules/organisms/article-gallery-carousel/article-gallery-carousel";
import HomeTabs from "../_modules/organisms/home-tabs/home-tabs";
import HomeBanner from "../_modules/organisms/home-banner/home-banner";
import ProfileLanding from "../_modules/organisms/profile-landing/profile-landing";
import ArticleListingWithQuestionnaire from "../_modules/organisms/article-listing-with-questionnaire/article-listing-with-questionnaire";
import PageBanner from "../_modules/organisms/page-banner/page-banner";
import Rte from "../_modules/molecules/rte/rte";
import InnovativeTechListing from "../_modules/organisms/innovative-tech-listing/innovative-tech-listing";
import CallToActions from "../_modules/organisms/call-to-actions/call-to-actions";
import TwoColCarousel from "../_modules/organisms/two-col-carousel/two-col-carousel";
import ThreeColImgcard from "../_modules/organisms/three-col-imgcard/three-col-imgcard";
import RelatedPosts from "../_modules/organisms/related-posts/related-posts";
import AccordionSliders from "../_modules/organisms/accordion-sliders/accordion-sliders";
import CollapsibleContent from "../_modules/molecules/collapsible-content/collapsible-content";
import FeaturedStories from "../_modules/organisms/featured-stories/featured-stories";
import SuccessStories from "../_modules/organisms/success-stories/success-stories";
import ThoughtLeadership from "../_modules/organisms/thought-leadership/thought-leadership";
import BrowseInterest from "../_modules/organisms/browse-interest/browse-interest";
import EventCardCarousel from "../_modules/organisms/event-card-carousel/event-card-carousel";
import TextCarousel from "../_modules/organisms/text-carousel/text-carousel";
import EventSpeakers from "../_modules/organisms/event-speakers/event-speakers";
import AnchorNavEvents from "../_modules/organisms/anchor-nav-events/anchor-nav-events";
import EventDetailsSection from "../_modules/organisms/event-details-section/event-details-section";
import EventDetailsBanner from "../_modules/organisms/event-details-banner/event-details-banner";
import EventsLandingBanner from "../_modules/organisms/events-landing-banner/events-landing-banner";
import FeaturedEventsCarousel from "../_modules/organisms/featured-events-carousel/featured-events-carousel";
import EventsSearch from "../_modules/organisms/events-search/events-search";
import BackToTop from "../_modules/atoms/back-to-top/back-to-top";
import FullBleedAccordions from "../_modules/organisms/full-bleed-accordions/full-bleed-accordions";
import Footnote from "../_modules/molecules/footnote/footnote";

$(() => {
  checkHover();
  setVh();

  const config = {
    breakpoints: {
      tablet: 768,
      desktop: 1024,
      lgDesktop: 1280,
    },
  };

  window.emitter = new Emitter();

  profileCookieHandler();
  window.onload = userDataTrigger;

  $(".match-height, .has-match-height").matchHeight();

  viewportAnimate();

  if (!localStorage.getItem("visitedHome")) {
    // set visited homepage item to check for guided tour condition to show
    if ($("body").hasClass("is-homepage")) {
      // is any homepage
      localStorage.setItem("visitedHome", true);
    }
  }

  if ($(".back-to-top").length) {
    $(".back-to-top").map((i, ele) => {
      new BackToTop($(ele));
    });
  }

  if ($(".site-header .site-header__main").length) {
    new SiteHeader($(".site-header"));
  }

  if ($(".home-tabs").length) {
    $(".home-tabs").map((i, ele) => {
      new HomeTabs($(ele));
    });
  }

  if ($(".page-banner").length) {
    $(".page-banner").map((i, ele) => {
      new PageBanner($(ele));
    });
  }

  if ($(".accordion-list").length) {
    $(".accordion-list").map((i, ele) => {
      new AccordionList($(ele));
    });
  }

  if ($(".profile-selector").length) {
    $(".profile-selector").map((i, ele) => {
      new ProfileLanding($(ele));
    });
  }

  if ($(".home-banner").length) {
    $(".home-banner").map((i, ele) => {
      new HomeBanner($(ele), true, config);
    });
  }

  if ($(".articles-with-questionnaire").length) {
    $(".articles-with-questionnaire").map((i, ele) => {
      new ArticleListingWithQuestionnaire($(ele));
    });
  }

  if ($(".digital-blocks").length) {
    $(".digital-blocks").map((i, ele) => {
      new DigitalBlocks($(ele), true, config);
    });
  }

  if ($(".article-gallery-carousel").length) {
    $(".article-gallery-carousel").map((i, ele) => {
      new ArticleGalleryCarousel($(ele), true, config);
    });
  }

  if ($(".stories-carousel").length) {
    $(".stories-carousel").map((i, ele) => {
      new StoriesCarousel($(ele), true, config);
    });
  }

  if ($(".call-to-actions.no-desktop").length) {
    $(".call-to-actions.no-desktop").map((i, ele) => {
      new CallToActions($(ele));
    });
  }

  if ($(".detail-aside").length) {
    new DetailAside();
  }

  if ($(".detail-wrapper .anchor-nav").length) {
    if ($(".anchor-nav").is(":visible")) {
      new AnchorNav();
    } else {
      const hash = window.location.hash;

      if ($(hash).length) {
        $("html, body").animate({
          scrollTop:
            $(hash).offset().top - $(".site-header").outerHeight() - 30,
        });
      }
    }
  }

  if ($(".event-detail-wrapper .anchor-nav").length) {
    if ($(".anchor-nav").is(":visible")) {
      new AnchorNavEvents();
    } else {
      const hash = window.location.hash;

      if ($(hash).length) {
        $("html, body").animate({
          scrollTop:
            $(hash).offset().top - $(".site-header").outerHeight() - 30,
        });
      }
    }
  }

  if ($(".cookie-message").length) {
    new CookieMessage();
  }

  if ($(".image-cta-listing").length) {
    new ImageCtaListing();
  }

  if ($(".image-rte").length) {
    new ImageRte();
  }

  if ($(".topic-card-listing").length) {
    new TopicCardListing();
  }

  if ($(".standard-content__carousel").length) {
    $(".standard-content__carousel").map((i, ele) => {
      new StandardContentCarousel($(ele), true, config);
    });
  }

  if ($(".featured").length) {
    new Featured();
  }

  if ($(".partners").length) {
    $(".partners").map((i, ele) => {
      new Partners($(ele), true, config);
    });
  }

  if ($(".gallery").length) {
    $(".gallery").map((i, ele) => {
      new Gallery($(ele), true, config);
    });
  }

  if ($(".story-listing").length) {
    new StoryListing(config);
  }

  if ($(".programme-listing").length) {
    new ProgrammeListing();
  }

  if ($(".news-listing").length) {
    new NewsListing();
  }

  if ($(".digitalsol-listing").length) {
    new DigitalsolListing(config);
  }

  if ($(".innovative-tech-listing").length) {
    new InnovativeTechListing();
  }

  if ($(".events-listing").length) {
    new EventsListing();
  }

  if ($(".newsroom").length) {
    new Newsroom();
  }

  if ($(".regulation-listing").length) {
    new RegulationListing();
  }

  if ($(".search-results").length) {
    $(".search-results").map((i, ele) => {
      new SearchResults($(ele));
    });
  }

  if ($(".video-player").length) {
    const iframeAPI = "https://www.youtube.com/iframe_api";

    if (!$('script[src="' + iframeAPI + '"]').length) {
      const firstScriptTag = document.getElementsByTagName("script")[0];
      const tag = document.createElement("script");
      tag.src = iframeAPI;
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    const players = [];
    const videoPlayers = [];

    $(".video-player").map((i, video) => {
      const playerData = {
        id: $(video).find(".video-player__holder").attr("id"),
        videoId: $(video).data("videoId"),
        YTPlayer: null,
      };
      players.push(playerData);

      const videoPlayerInstance = new VideoPlayer($(video), true, playerData);
      videoPlayers.push(videoPlayerInstance);
    });

    window.onYouTubeIframeAPIReady = () => {
      players.map((player, i) => {
        player.YTPlayer = new YT.Player(player.id, {
          videoId: player.videoId,
          events: {
            onReady: (e) => {
              videoPlayers[i].onReady($(".video-player").eq(i));
            },
          },
        });
      });
    };
  }

  if ($(".rte table").length) {
    $(".rte table").map((i, ele) => {
      new Rte($(ele));
    });

    tablesawJquery.init();
  }

  //CTA Widgets
  if ($(".two-col-carousel").length) {
    $(".two-col-carousel").map((i, ele) => {
      new TwoColCarousel($(ele), true, config);
    });
  }

  if ($(".three-col-imgcard").length) {
    $(".three-col-imgcard").map((i, ele) => {
      new ThreeColImgcard($(ele), true, config);
    });
  }

  if ($(".related-posts").length) {
    $(".related-posts").map((i, ele) => {
      new RelatedPosts($(ele), true, config);
    });
  }

  if ($(".collapsible-content").length) {
    $(".collapsible-content").map((i, ele) => {
      new CollapsibleContent($(ele));
    });
  }

  if ($(".accordion-sliders").length) {
    $(".accordion-sliders").map((i, ele) => {
      new AccordionSliders($(ele));
    });
  }

  if ($(".featured-stories").length) {
    $(".featured-stories").map((i, ele) => {
      new FeaturedStories($(ele));
    });
  }

  if ($(".success-stories").length) {
    $(".success-stories").map((i, ele) => {
      new SuccessStories($(ele));
    });
  }

  if ($(".thought-leadership").length) {
    $(".thought-leadership").map((i, ele) => {
      new ThoughtLeadership($(ele));
    });
  }

  if ($(".browse-interest").length) {
    $(".browse-interest").map((i, ele) => {
      new BrowseInterest($(ele));
    });
  }

  if ($(".event-details-banner").length) {
    $(".event-details-banner").map((i, ele) => {
      new EventDetailsBanner($(ele));
    });
  }

  if ($(".event-details-section").length) {
    $(".event-details-section").map((i, ele) => {
      new EventDetailsSection($(ele));
    });
  }

  if ($(".event-card-carousel").length) {
    $(".event-card-carousel").map((i, ele) => {
      new EventCardCarousel($(ele), true, config);
    });
  }

  if ($(".text-carousel").length) {
    $(".text-carousel").map((i, ele) => {
      new TextCarousel($(ele), true, config);
    });
  }

  if ($(".event-speakers").length) {
    $(".event-speakers").map((i, ele) => {
      new EventSpeakers($(ele), true, config);
    });
  }

  if ($(".events-landing-banner").length) {
    $(".events-landing-banner").map((i, ele) => {
      new EventsLandingBanner($(ele), true, config);
    });
  }

  if ($(".featured-events-carousel").length) {
    $(".featured-events-carousel").map((i, ele) => {
      new FeaturedEventsCarousel($(ele), true, config);
    });
  }

  if ($(".events-search").length) {
    $(".events-search").map((i, ele) => {
      new EventsSearch($(ele));
    });
  }

  if ($(".full-bleed-accordions").length) {
    $(".full-bleed-accordions").map((i, ele) => {
      new FullBleedAccordions($(ele));
    });
  }

  if ($(".footnote").length) {
    $(".footnote").map((i, ele) => {
      new Footnote($(ele));
    });
  }

  /**
   * Trip js handler
   */
  tripHandler();
});

function filterCookieItems(cookieArray){
  let newCookieArray = [];

  cookieArray.forEach(item => {
    let cookieItem = {};

    if(parseInt(item.point) >= 5){
      cookieItem.value = item.value;
      newCookieArray.push(cookieItem);
    }
  });

  return newCookieArray;
}

function sortCookieItems(cookieArray){
  return mergeSortTopDown(cookieArray);
}

function mergeSortTopDown(array){
  if(array.length < 2) {
    return array;
  }

  let middle = Math.floor(array.length / 2);
  let left = array.slice(0, middle);
  let right = array.slice(middle);

  return mergeTopDown(mergeSortTopDown(left), mergeSortTopDown(right));
}

function mergeTopDown(left, right) {
  let array = [];

  while(left.length && right.length) {
    if(left[0].point > right[0].point) {
      array.push(left.shift());
    } else {
      array.push(right.shift());
    }
  }
  return array.concat(left.slice()).concat(right.slice());
}

export { filterCookieItems, sortCookieItems };

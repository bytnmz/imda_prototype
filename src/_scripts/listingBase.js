'use strict';

import $ from 'jquery';
import Pagination from '../_modules/molecules/pagination/pagination';

export default class ListingBase {
  constructor($pagination, dataModel = {}, parameters = {}) {
    this.$pagination = $pagination;
    this.pagination = new Pagination(this.$pagination);

    this.data = dataModel;
    this.parameters = parameters;

    this.queries = this.getUrlQueries();
  }

  setupCheckboxes(fieldName, queries, filters) {
    /*
      - used by listing pages which has checkboxes of {fieldName}
      - to be called upon loading the listing pages
      - will set CHECKED to the checkboxes based on the {queries} parameters
      - will expand subgroup if found
      - will return the string (piped-value string from {queries} parameter)
    */
    let valueString, values;

    valueString = (queries[fieldName]) ? queries[fieldName] : '';

    if (valueString == 'all' || valueString == '') {
      $('input[name="' + fieldName + '"]').map((i, ele) => {
        let $this = $(ele);
        let $parent = $this.parent();

        if ($this.val() == 'all') {
          $this.prop('checked', true);
        } else {
          $this.prop('checked', false);
          if ($parent.find('.filters-group__subgroup').length) {
            filters.hideSubGroup($parent);
          }
        }
      });
    } else {
      values = valueString.split('|');
      $('input[name="' + fieldName + '"][value="all"]').prop('checked', false);
      $('input[name="' + fieldName + '"]').map((i, ele) => {
        let $this = $(ele);
        let $parent = $this.parent();
        if (values.indexOf($this.val()) !== -1) {
          $this.prop('checked', true);

          if ($parent.find('.filters-group__subgroup').length) {
            filters.showSubGroup($parent);
            let $subgroupFields = $parent.find('.filters-group__subgroup input');
            $subgroupFields.map((j, field) => {
              let $field = $(field);
              if (values.indexOf($field.val()) !== -1) {
                $field.prop('checked', true);
              } else {
                $field.prop('checked', false);
              }
            });
          }
        } else {
          $this.prop('checked', false);
          if ($parent.find('.filters-group__subgroup').length) {
            filters.hideSubGroup($parent);
          }
        }
      });
    }

    return valueString;
  }

  setupNonAllCheckboxes(fieldName, queries) {
    let valueString, values;
    valueString = (queries[fieldName]) ? queries[fieldName] : '';

    if (valueString != '') {
      values = queries[fieldName].split('|');
      $('input[name="' + fieldName + '"]').map((i, ele) => {
        let $this = $(ele);
        if (values.indexOf($this.val()) !== -1) {
          $this.prop('checked', true);
        }
      });
    } else {
      /* check all of them */
      $('input[name="' + fieldName + '"]').prop('checked', true);
    }

    return valueString;
  }

  updateCheckboxValues($fields, filters) {
    /*
      - used by listing pages with checkboxes
      - to be called every time a checkbox under {$fields} is checked/updated
      - will show the subgroup using the {filters} object
      - will return the piped-value string based on selected checkboxes under {$fields}
    */
    let values = [];
    $fields.toArray().map((ele) => {
      let $this = $(ele);
      if ($this.is(':checked')) {
        values.push($this.val());

        if ($this.parent().find('.filters-group__subgroup').length) {
          let $subgroups = $this.parent().find('.filters-group__subgroup input');

          $subgroups.map((j, subgroup) => {
            let $subgroup = $(subgroup);
            if ($subgroup.is(':checked')) {
              values.push($subgroup.val());
            }
          });
        }
      }
    });

    return values.join('|');
  }

  updateNonAllCheckboxValues($fields) {
    let values = [];
    $fields.toArray().map((ele) => {
      let $this = $(ele);
      if ($this.is(':checked')) {
        values.push($this.val());
      }
    });

    return values.join('|');
  }

  addCheckboxListener(fieldName, dataFieldName, filters, updateDataFs, parentFieldName = null) {
    /*
      - to called to add event listener to the checkbox of {fieldName} in listing page
      - {fieldName} will be exactly the same as URL query parameter
      - will update {dataFieldName} of this.data, which will be sent via AJAX
      - will uncheck all the other checkboxes when "ALL" is being checked
      - will uncheck "ALL" when any of other checkbox is being checked
      - will check "ALL" when all other checkboxes are being unchecked
    */

    let checkingTimeout;
    let subGroupFieldNames = [];
    $('input[name="' + fieldName + '"]').map((i, ele) => {
      let $this = $(ele);
      let subGroupFieldName = null;
      if ($this.parent().find('.filters-group__subgroup').length) {
        /* if this field has subgroup fields */
        subGroupFieldName = $this.parent().find('.filters-group__subgroup input').attr('name');
        subGroupFieldNames.push(subGroupFieldName);
      }
      $this.on('change', e => {
        if ($this.prop('checked') == true) {
          if ($this.val() == 'all') {
            /* uncheck all other checkbox when ALL is checked, and hide all subgroups */
            $('input[name="' + fieldName + '"]').prop('checked', false);
            $this.prop('checked', true);
            if (subGroupFieldNames.length > 0) {
              filters.hideSubGroup($('input[name="' + fieldName + '"]').parent());
            }
            this.data[dataFieldName] = this.updateCheckboxValues($('input[name="' + fieldName + '"]'), filters);
            this.parameters[fieldName] = this.data[dataFieldName];
            this.data.page = 1;
            this.parameters.page = 1;
            updateDataFs();
            this.updateURL();
          } else {
            $('input[name="' + fieldName + '"][value="all"]').prop('checked', false);
            if (subGroupFieldName !== null) {
              filters.showSubGroup($this.parent(), true);
            }
            clearTimeout(checkingTimeout);
            checkingTimeout = setTimeout(() => {
              if (parentFieldName !== null) {
                this.data[dataFieldName] = this.updateCheckboxValues($('input[name="' + parentFieldName + '"]'), filters);
                this.parameters[parentFieldName] = this.data[dataFieldName];
              } else {
                this.data[dataFieldName] = this.updateCheckboxValues($('input[name="' + fieldName + '"]'), filters);
                this.parameters[fieldName] = this.data[dataFieldName];
              }
              this.data.page = 1;
              this.parameters.page = 1;
              updateDataFs();
              this.updateURL();
            }, 300);
          }
        } else {
          if ($this.val() == 'all') {
            /* ALL checkbox cannot be unchecked */
            $this.prop('checked', true);
          } else {
            if ($('input[name="' + fieldName + '"]:checked').length == 0) {
              /* when no checkbox is checked, ALL will be checked automatically */
              $('input[name="' + fieldName + '"][value="all"]').prop('checked', true);
            }
            clearTimeout(checkingTimeout);
            checkingTimeout = setTimeout(() => {
              if (parentFieldName !== null) {
                this.data[dataFieldName] = this.updateCheckboxValues($('input[name="' + parentFieldName + '"]'), filters);
                this.parameters[parentFieldName] = this.data[dataFieldName];
              } else {
                this.data[dataFieldName] = this.updateCheckboxValues($('input[name="' + fieldName + '"]'), filters);
                this.parameters[fieldName] = this.data[dataFieldName];
                if (subGroupFieldName !== null) {
                  filters.hideSubGroup($this.parent());
                }
              }
              this.data.page = 1;
              this.parameters.page = 1;
              updateDataFs();
              this.updateURL();
            }, 300);
          }
        }
      });
    });
  }

  updateData({endpoint, data, callback}) {
    $.ajax({
      url: endpoint,
      // data: data,
      data: JSON.stringify(data),
      method: 'post',
      contentType: 'application/json',
      success: (res) => {
        callback(res);
      },
      error: (err) => {
        console.log(err);
      }
    });
  }

  getUrlQueries() {
    let queryString = {};
    let query = window.location.search.substring(1);
    if (!query.length) {
      return queryString;
    }
    let vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
      let pair = vars[i].split("=");

      /* If first entry with this name */
      if (typeof queryString[pair[0]] === "undefined") {
        queryString[pair[0]] = decodeURIComponent(pair[1]);

        /* If second entry with this name */
      } else if (typeof queryString[pair[0]] === "string") {
        let arr = [ queryString[pair[0]], decodeURIComponent(pair[1])];
        queryString[pair[0]] = arr;

        /* If third or later entry with this name */
      } else {
        queryString[pair[0]].push(decodeURIComponent(pair[1]));
      }
    }
    return queryString;
  }

  updateURL(replaceState = false) {
    let queryString = '?';

    let i = 0;
    for (let param in this.parameters) {
      if (i > 0) {
        queryString += '&';
      }

      queryString += param + '=' + encodeURIComponent(this.parameters[param]);
      i++;
    }

    for (let query in this.queries) {
      if (typeof(this.parameters[query]) == 'undefined') {
        if (i > 0) {
          queryString += '&';
        }

        queryString += query + '=' + encodeURIComponent(this.queries[query]);
        i++;
      }
    }

    if (replaceState) {
      window.history.replaceState(this.data, '', queryString);
    } else {
      window.history.pushState(this.data, '', queryString);
    }
  }

}

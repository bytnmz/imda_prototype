import Carousel from "../_modules/carousel/carousel";

export function initCarousel($carousel = $('[data-carousel]', this.$selector), options, slickInit = true, init = null, before = null, after = null, setPos = null, callback = null) {
  const carousel = new Carousel(
    $carousel, 
    true,
    options,
    init,
    before,
    after,
    setPos,
    slickInit
  );

  if(callback) callback();

  return carousel;
}

export function setBodyToFixed() {
  $('body').css({
    "overflow": "hidden"
  });
}

export function setBodyPos() {
  $('body').css({
    "overflow": "auto"
  });
}

export function stackedCarousel($selector, carouselOptions, maxItems, updateImage = false) {
  let current = 0; // to track current slide to set classnames on breakpoint reinit

  let initSlick = (e, slick) => {
    $(`[data-slick-index=${current}]`, $selector).addClass('slick-slide--next0');
    
    if ($(`[data-slick-index=${current + 1}]`, $selector).length) 
      $(`[data-slick-index=${current + 1}]`, $selector).addClass('slick-slide--next1');
    
    if ($(`[data-slick-index=${current + 2}]`, $selector).length)
      $(`[data-slick-index=${current + 2}]`, $selector).addClass('slick-slide--next2');

    if ($(`[data-slick-index=${current + 3}]`, $selector).length) 
      $(`[data-slick-index=${current + 3}]`, $selector).addClass('slick-slide--next3');
    
    if ($(`[data-slick-index=${current + 4}]`, $selector).length)
      $(`[data-slick-index=${current + 4}]`, $selector).addClass('slick-slide--next4');

    if(updateImage) {
      const imageSrc = `<picture>${$(`[data-slick-index=${current}]`, $selector).find('picture').html()}</picture>`;

      $selector.closest('section').find(`> div:first-child > div`).html(imageSrc);
    }
  };

  const beforeChange = (e, slick, currentSlide, nextSlide) => {
    current = currentSlide;

    // determine forward step
    let isForward = false;

    if(nextSlide > currentSlide) {
      if((nextSlide - currentSlide >= 2)) {
        isForward = false;
      }
      else {
        isForward = true;
      }
    }
    else {
      if((currentSlide - nextSlide >= 2)) {
        isForward = true;
      }
      else {
        isForward = false;
      }
    }

    if(isForward) { 
      // going forward
      $('[data-slick-index]', $selector).map((i,ele) => {
        const slideIndex = parseInt($(ele).data('slick-index'));

        const newIndex = (slideIndex - currentSlide - 1) < 0 ? slideIndex - currentSlide - 1 + maxItems : slideIndex - currentSlide - 1;
        const currentIndex = (slideIndex - currentSlide) < 0 ? slideIndex - currentSlide + maxItems : slideIndex - currentSlide;
        $(ele).addClass(`slick-slide--next${newIndex}`).removeClass(`slick-slide--next${currentIndex}`);
      });

    }
    else {
      // going backwards
      $('[data-slick-index]', $selector).map((i,ele) => {
        const slideIndex = parseInt($(ele).data('slick-index'));
        const step = (maxItems - currentSlide) >= maxItems ? 0 : maxItems - currentSlide;

        const newIndex = (slideIndex + step + 1) >= maxItems ? slideIndex + step + 1 - maxItems : slideIndex + step + 1;
        const currentIndex = (slideIndex + step) >= maxItems ? slideIndex + step - maxItems : slideIndex + step;
        $(ele).addClass(`slick-slide--next${newIndex}`).removeClass(`slick-slide--next${currentIndex}`);
      });   
    }

    if(updateImage) {
      const imageSrc = `<picture>${$(`[data-slick-index=${nextSlide}]`, $selector).find('picture').html()}</picture>`;
      
      $selector.closest('section').find(`> div:first-child > div`).html(imageSrc);
    }
  };

  const carousel = initCarousel($('[data-carousel]', $selector), carouselOptions, true, initSlick, beforeChange);

  return carousel;
}
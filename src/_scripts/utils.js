export function checkHover() {
  if(window.matchMedia("(hover: none)").matches) {
    $('body').addClass('no-hover');
  }
}

export function setVh() {
  // set vh unit to browser
  let vh = $(window).height() * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);

  window.addEventListener("resize", () => {
    vh = $(window).height() * 0.01;
    document.documentElement.style.setProperty("--vh", `${vh}px`);
  });
}

export function viewportAnimate() {
  let scrollTimeout;
  $(window).on('scroll.viewportAnimate', e => {
    clearTimeout(scrollTimeout);
    scrollTimeout = setTimeout(() => {
      const scrollPos = $(window).scrollTop() + $(window).outerHeight();
      $('.viewport-animate').map((i, ele) => {
        if ($(ele).hasClass('box-quote')) {
          if (scrollPos >= $(ele).offset().top + 75) {
            $(ele).addClass('in-view');
          }
        } else {
          if (scrollPos >= $(ele).offset().top + ($(window).outerHeight() / 3)) {
            $(ele).addClass('in-view');
  
            if ($(ele).find('.viewport-animate-child').length) {
              const $children = $(ele).find('.viewport-animate-child');
  
              $children.map((j, child) => {
                const $child = $(child);
                if (scrollPos >= $child.offset().top + 50) {
                  $child.addClass('child-in-view');
                }
              });
            }
          }
        }
      });
    }, 20);
  }).trigger('scroll.viewportAnimate');
}

export function restoreScrollPos() {
  if (history.scrollRestoration) {
    history.scrollRestoration = 'manual';
  } else {
    window.onbeforeunload = function () {
    window.scrollTo(0, 0);
    }
  }
}
// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';

import $ from 'jquery';
import 'jquery-match-height';
import Emitter from 'tiny-emitter';

import { setVh, viewportAnimate } from './utils';
import { profileCookieHandler } from './profileCookieHandler';

import CookieMessage from '../_modules/molecules/cookie-message/cookie-message';
import SiteHeader from '../_modules/organisms/site-header/site-header';
import PageBanner from '../_modules/organisms/page-banner/page-banner';
import VideoPlayer from '../_modules/molecules/video-player/video-player';
import StandardContentCarousel from '../_modules/organisms/standard-content-carousel/standard-content-carousel';
import FilmDirectory from '../_modules/organisms/film-directory/film-directory';

$(() => {
  setVh();

  const config = {
    breakpoints: {
      tablet: 768,
      desktop: 1024,
      lgDesktop: 1280
    }
  };

  window.emitter = new Emitter();

  profileCookieHandler();

  $('.match-height').matchHeight();

  viewportAnimate();

  if ($('.site-header').length) {
    new SiteHeader($('.site-header'));
  }

  if ($('.page-banner').length) {
    $('.page-banner').map((i,ele) => {
      new PageBanner($(ele));
    });
  }

  if ($('.cookie-message').length) {
    new CookieMessage();
  }

  if ($('.standard-content__carousel').length) {
    $('.standard-content__carousel').map((i,ele) => {
      new StandardContentCarousel($(ele), true, config);
    });
  }

  if($('.film-directory').length) {
    $('.film-directory').map((i,ele) => {
      new FilmDirectory($(ele))
    })
  }

  if ($('.video-player').length) {
    const iframeAPI = "https://www.youtube.com/iframe_api";

    if (!$('script[src="' + iframeAPI + '"]').length) {
      const firstScriptTag = document.getElementsByTagName('script')[0];
      const tag = document.createElement('script');
      tag.src = iframeAPI;
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    const players = [];
    const videoPlayers = [];

    $('.video-player').map((i, video) => {
      
      const playerData = {
        id: $(video).find('.video-player__holder').attr('id'),
        videoId: $(video).data('videoId'),
        YTPlayer: null
      };
      players.push(playerData);

      const videoPlayerInstance = new VideoPlayer($(video), true, playerData);
      videoPlayers.push(videoPlayerInstance);
    });

    window.onYouTubeIframeAPIReady = () => {
      players.map((player, i) => {
        player.YTPlayer = new YT.Player(player.id, {
          videoId: player.videoId,
          events: {
            'onReady': (e) => {
              videoPlayers[i].onReady($('.video-player').eq(i));
            }
          }
        });
      });
    };
  }
});

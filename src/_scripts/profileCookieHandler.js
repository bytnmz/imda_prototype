import Cookies from 'js-cookie';
import { profileClassnames } from './globals';

export function profileCookieHandler() {
  let profile = Cookies.get('imda_profile');

  let purpleIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="20" fill="none" viewBox="0 0 24 20">
  <path stroke="#702F8A" stroke-linecap="round" stroke-miterlimit="10" d="M22.305 9.894v8.601c0 .368-.267.658-.597.658H2.293c-.33 0-.598-.295-.598-.658V9.894"/>
  <path stroke="#702F8A" stroke-linecap="round" stroke-miterlimit="10" d="M10.985 13.028 1.531 9.894C1.217 9.786 1 9.47 1 9.102V4.717c0-.362.268-.657.597-.657H22.4c.333 0 .601.29.601.657v4.385c0 .363-.217.684-.531.792l-9.452 3.134M7.61 4.06l.852-2.407c.14-.393.484-.653.867-.653h5.34c.386 0 .73.26.868.653l.85 2.407"/>
  <path stroke="#702F8A" stroke-linecap="round" stroke-miterlimit="10" d="M11.998 11.439c.527 0 .958.475.958 1.056v1.397c0 .581-.431 1.056-.958 1.056-.527 0-.959-.475-.959-1.056v-1.397c0-.58.431-1.056.959-1.056Z"/>
  </svg>`;
  let maroonIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="28" height="21" fill="none" viewBox="0 0 28 21">
  <path stroke="#9E1B64" stroke-linecap="round" stroke-linejoin="round" d="m27.28 7.247-13.139 6.247L1 7.247 14.139 1 27.28 7.247Z"/>
  <path stroke="#9E1B64" stroke-linecap="round" stroke-linejoin="round" d="M22.798 9.548v6.657l-8.331 3.864-8.334-3.864V9.877M2.281 7.576v10.52"/>
  </svg>`;
  let tealIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="26" height="20" fill="none" viewBox="0 0 26 20">
  <path stroke="#008672" stroke-linejoin="round" d="M1.004 19.343v-2.71c-.023-1.069 0-2.536 1.336-4.612.257-.34.913-1.099 1.475-1.409a9.425 9.425 0 0 1 1.195-.563c-.855-.692-2.425-2.464-2.179-4.787.288-2.719 2.46-4.189 4.638-4.26 2.179-.07 4.252 1.162 4.849 3.239.344 1.197.632 3.098-1.054 4.929l-.773.88c.831.445 2.593 1.689 3.127 3.097.668 1.76.703 2.64.738 3.485.028.676.011 1.913 0 2.71"/>
  <path stroke="#008672" stroke-linejoin="round" d="M14.027 14.226c.035-.104.122-.835 1.424-1.984.868-.766 1.435-.855 1.575-.906-.728-.594-2.065-2.115-1.855-4.108.245-2.333 2.094-3.594 3.95-3.654 1.855-.06 3.62.996 4.129 2.778.293 1.027.538 2.659-.898 4.23l-.652.754c.709.383 2.12 1.516 2.674 2.682.611 1.288.582 2.242.611 2.967.024.58.01 1.501 0 2.186"/>
  </svg>`;
  let defaultIcon = `<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g filter="url(#filter0_d_1774_134866)">
  <path d="M22.0039 32V29.4879C21.9829 28.4983 22.0039 27.1387 23.2021 25.2138C23.4334 24.8984 24.022 24.1959 24.5265 23.9088C25.031 23.6217 25.4514 23.4414 25.5986 23.3871C24.8313 22.7455 23.4229 21.1028 23.6436 18.9499C23.9019 16.4302 25.8508 15.0679 27.8058 15.0027C29.7608 14.9374 31.6212 16.0786 32.1572 18.0038C32.4661 19.113 32.7248 20.8751 31.2113 22.5715L30.5176 23.3871C31.2638 23.8003 32.8446 24.9527 33.3239 26.2577C33.923 27.8889 33.9545 28.7049 33.9861 29.4879C34.0113 30.1143 33.9966 31.2604 33.9861 32" stroke="#262626" stroke-linejoin="round"/>
  <circle cx="28" cy="24" r="15.5" stroke="#262626"/>
  </g>
  <defs>
  <filter id="filter0_d_1774_134866" x="0" y="0" width="56" height="56" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
  <feOffset dy="4"/>
  <feGaussianBlur stdDeviation="6"/>
  <feComposite in2="hardAlpha" operator="out"/>
  <feColorMatrix type="matrix" values="0 0 0 0 0.439216 0 0 0 0 0.184314 0 0 0 0 0.541176 0 0 0 0.25 0"/>
  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1774_134866"/>
  <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1774_134866" result="shape"/>
  </filter>
  </defs>
  </svg>
  `;
  
  if(!Cookies.get('imda_profile')) {
    if($('body').hasClass('imda-profile0')) {
      Cookies.set('imda_profile', 'profile0', { expires: 365 });
      Cookies.set('visited_homepage', true, { expires: 365 });
      $('.site-header__mobile-ui .profile-indicator').text('Select a profile');
      $('.site-header__mobile-ui .profile-icon').addClass('hide');
      $('.profile-icon').html(defaultIcon);
      $('.profile-icon').addClass('defaultIcon');
      $('.site-header__profile .profile-icon').empty();
      if (!$('.site-header__profile button').hasClass('purpleShadow')) {
        $('.site-header__profile button').addClass('purpleShadow');
        $('.site-header__profile button').removeClass('maroonShadow turquoiseShadow');
      }
      if (!$('.site-header__profile nav').hasClass('purpleShadow')) {
        $('.site-header__profile nav').addClass('purpleShadow');
        $('.site-header__profile nav').removeClass('maroonShadow turquoiseShadow');
      }
      if(!$('.site-header__mobile-ui .profile-indicator').hasClass('purpleShadow')) {
        $('.site-header__mobile-ui .profile-indicator').addClass('purpleShadow');
        $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow turquoiseShadow');
      }
      if($('.newsletter').length && $('.newsletter picture').find('img').length) {
        let url = $('.newsletter picture').find('img').data('profile1');
        $('.newsletter picture').find('img').attr('src', url);   
        $('.newsletter picture').find('source').attr('srcset', url);
      }
      if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
        $('.horizontal-accordion__image').each((index, element) => {
          let url = $(element).find('img').data('profile1');
          $(element).find('img').attr('src', url);        
        });
      }
      if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
        $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
          const url = $(ele).data('profile1');
          $(ele).attr('src', url);
        });
      }
      if($('ul').length && $('ul > li').length) {
        $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
      }
    }
    if($('body').hasClass('imda-profile1')) {
      Cookies.set('imda_profile', 'profile1', { expires: 365 });
      Cookies.set('visited_homepage', true, { expires: 365 });
      $('.profile-icon').html(purpleIcon);
      $('.profile-icon').removeClass('defaultIcon');
      $('.site-header__profile nav').find('a.active').parent().addClass('active');
      if (!$('.site-header__profile button').hasClass('purpleShadow')) {
        $('.site-header__profile button').addClass('purpleShadow');
        $('.site-header__profile button').removeClass('maroonShadow turquoiseShadow');
      }
      if (!$('.site-header__profile nav').hasClass('purpleShadow')) {
        $('.site-header__profile nav').addClass('purpleShadow');
        $('.site-header__profile nav').removeClass('maroonShadow turquoiseShadow');
      }
      if(!$('.site-header__mobile-ui .profile-indicator').hasClass('purpleShadow')) {
        $('.site-header__mobile-ui .profile-indicator').addClass('purpleShadow');
        $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow turquoiseShadow');
      }
      if($('.newsletter').length && $('.newsletter picture').find('img').length) {
        let url = $('.newsletter picture').find('img').data('profile1');
        $('.newsletter picture').find('img').attr('src', url);   
        $('.newsletter picture').find('source').attr('srcset', url);       
      }
      if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
        $('.horizontal-accordion__image').each((index, element) => {
          let url = $(element).find('img').data('profile1');
          $(element).find('img').attr('src', url);        
        });
      }
      if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
        $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
          const url = $(ele).data('profile1');
          $(ele).attr('src', url);
        });
      }
      if($('ul').length && $('ul > li').length) {
        $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
      }
    }
    else if($('body').hasClass('imda-profile2')) {
      Cookies.set('imda_profile', 'profile2', { expires: 365 });
      Cookies.set('visited_homepage', true, { expires: 365 });
      $('.profile-icon').html(maroonIcon);
      $('.profile-icon').removeClass('defaultIcon');
      $('.site-header__profile nav').find('a.active').parent().addClass('active');
      if (!$('.site-header__profile button').hasClass('maroonShadow')) {
        $('.site-header__profile button').addClass('maroonShadow');
        $('.site-header__profile button').removeClass('purpleShadow turquoiseShadow');
      }
      if (!$('.site-header__profile nav').hasClass('maroonShadow')) {
        $('.site-header__profile nav').addClass('maroonShadow');
        $('.site-header__profile nav').removeClass('purpleShadow turquoiseShadow');
      }
      if(!$('.site-header__mobile-ui .profile-indicator').hasClass('maroonShadow')) {
        $('.site-header__mobile-ui .profile-indicator').addClass('maroonShadow');
        $('.site-header__mobile-ui .profile-indicator').removeClass('purpleShadow turquoiseShadow');
      }
      if($('.newsletter').length && $('.newsletter picture').find('img').length) {
        let url = $('.newsletter picture').find('img').data('profile2');
        $('.newsletter picture').find('img').attr('src', url);
        $('.newsletter picture').find('source').attr('srcset', url);
      }
      if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
        $('.horizontal-accordion__image').each((index, element) => {
          let url = $(element).find('img').data('profile2');
          $(element).find('img').attr('src', url);
        });
      }
      if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
        $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
          const url = $(ele).data('profile2');
          $(ele).attr('src', url);
        });
      }
      if($('ul').length && $('ul > li').length) {
        $('ul:not(.tags-list__list) > li').addClass('maroon');
        $('ul:not(.tags-list__list) > li').removeClass('turquoise');
      }
    }
    else if($('body').hasClass('imda-profile3')) {
      Cookies.set('imda_profile', 'profile3', { expires: 365 });
      Cookies.set('visited_homepage', true, { expires: 365 });
      $('.site-header__profile nav').find('a.active').parent().addClass('active');
      $('.profile-icon').html(tealIcon);
      $('.profile-icon').removeClass('defaultIcon');
      if (!$('.site-header__profile button').hasClass('turquoiseShadow')) {
        $('.site-header__profile button').addClass('turquoiseShadow');
        $('.site-header__profile button').removeClass('maroonShadow purpleShadow');
      }
      if (!$('.site-header__profile nav').hasClass('turquoiseShadow')) {
        $('.site-header__profile nav').addClass('turquoiseShadow');
        $('.site-header__profile nav').removeClass('maroonShadow purpleShadow');
      }
      if(!$('.site-header__mobile-ui .profile-indicator').hasClass('turquoiseShadow')) {
        $('.site-header__mobile-ui .profile-indicator').addClass('turquoiseShadow');
        $('.site-header__mobile-ui .profile-indicator').removeClass('purpleShadow maroonShadow');
      }
      if($('.newsletter').length && $('.newsletter picture').find('img').length) {
        let url = $('.newsletter picture').find('img').data('profile3');
        $('.newsletter picture').find('img').attr('src', url);
        $('.newsletter picture').find('source').attr('srcset', url);
      }
      if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
        $('.horizontal-accordion__image').each((index, element) => {
          let url = $(element).find('img').data('profile3');
          $(element).find('img').attr('src', url);
        });
      }
      if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
        $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
          const url = $(ele).data('profile3');
          $(ele).attr('src', url);
        });
      }
      if($('ul').length && $('ul > li').length) {
        $('ul:not(.tags-list__list) > li').addClass('turquoise');
        $('ul:not(.tags-list__list) > li').removeClass('maroon');
      }
    }
    else {
      //ONLY set default profile cookie when not on profile landing page
      // if (!$('body').hasClass('profile-landing-pg')) {
        //Cookies.set('imda_profile', 'profile1', { expires: 365 });
      Cookies.set('imda_profile', 'profile0', { expires: 365 });

      if(!$('body').hasClass('imda-profile0')) {
        $('.site-header__mobile-ui .profile-indicator').text('Select a profile');
        $('.site-header__mobile-ui .profile-icon').addClass('hide');
        $('.profile-icon').html(defaultIcon);
        $('.profile-icon').addClass('defaultIcon');
        $('.site-header__profile .profile-icon').empty();
        $('body').addClass(profileClassnames[3]);
        if (!$('.site-header__profile button').hasClass('purpleShadow')) {
          $('.site-header__profile button').addClass('purpleShadow');
          $('.site-header__profile button').removeClass('maroonShadow turquoiseShadow');
        }
        if (!$('.site-header__profile nav').hasClass('purpleShadow')) {
          $('.site-header__profile nav').addClass('purpleShadow');
          $('.site-header__profile nav').removeClass('maroonShadow turquoiseShadow');
        }
        if(!$('.site-header__mobile-ui .profile-indicator').hasClass('purpleShadow')) {
          $('.site-header__mobile-ui .profile-indicator').addClass('purpleShadow');
          $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow turquoiseShadow');
        }
        if($('.newsletter').length && $('.newsletter picture').find('img').length) {
          let url = $('.newsletter picture').find('img').data('profile1');
          $('.newsletter picture').find('img').attr('src', url);
          $('.newsletter picture').find('source').attr('srcset', url);
        }
        if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
          $('.horizontal-accordion__image').each((index, element) => {
            let url = $(element).find('img').data('profile1');
            $(element).find('img').attr('src', url);
          });
        }
        if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
          $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
            const url = $(ele).data('profile1');
            $(ele).attr('src', url);
          });
        }
        if($('ul').length && $('ul > li').length) {
          $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
        }
      } 
      // }

      //Set the profile dropdown in site header
      $('.site-header__profile ul li a.active').removeClass('active');

      //$(`.site-header__profile ul li a[href="#profile1"]`).addClass('active');
      // $(`.site-header__profile button span:first-child`).text($(`.site-header__profile ul li a[href="#profile1"]`).text());
      // $('.site-header__mobile-ui .profile-indicator').text($(`.site-header__profile ul li a[href="#profile1"]`).text());
    
      $(`.site-header__profile button span:nth-child(2)`).text('Select a profile');
      //$('.site-header__mobile-ui .profile-indicator').text('Select a profile');
    }
  }
  else {
    // class handler for home page
    if($('body').hasClass('is-homepage')) {
      if($('body').hasClass('imda-profile0')) {
        Cookies.set('imda_profile', 'profile0', { expires: 365 });
        Cookies.set('visited_homepage', true, { expires: 365 });
        $('.site-header__mobile-ui .profile-indicator').text('Select a profile');
        $('.site-header__mobile-ui .profile-icon').addClass('hide');
        $('.profile-icon').html(defaultIcon);
        $('.profile-icon').addClass('defaultIcon');
        $(`.site-header__profile button span:nth-child(2)`).text('Select a profile');
        $('.site-header__mobile-ui .profile-indicator').text('Select a profile');
        $('.site-header__mobile-ui .profile-icon').addClass('hide');
        $('.site-header__profile .profile-icon').empty();
        if (!$('.site-header__profile button').hasClass('purpleShadow')) {
          $('.site-header__profile button').addClass('purpleShadow');
          $('.site-header__profile button').removeClass('maroonShadow turquoiseShadow');
        }
        if (!$('.site-header__profile nav').hasClass('purpleShadow')) {
          $('.site-header__profile nav').addClass('purpleShadow');
          $('.site-header__profile nav').removeClass('maroonShadow turquoiseShadow');
        }
        if(!$('.site-header__mobile-ui .profile-indicator').hasClass('purpleShadow')) {
          $('.site-header__mobile-ui .profile-indicator').addClass('purpleShadow');
          $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow turquoiseShadow');
        }
        if($('.newsletter').length && $('.newsletter picture').find('img').length) {
          let url = $('.newsletter picture').find('img').data('profile1');
          $('.newsletter picture').find('img').attr('src', url);
          $('.newsletter picture').find('source').attr('srcset', url);
        }
        if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
          $('.horizontal-accordion__image').each((index, element) => {
            let url = $(element).find('img').data('profile1');
            $(element).find('img').attr('src', url);
          });
        }
        if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
          $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
            const url = $(ele).data('profile1');
            $(ele).attr('src', url);
          });
        }
        if($('ul').length && $('ul > li').length) {
          $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
        }
      } else { 
        if($('body').hasClass('imda-profile1')) {
          let profileUrl = $('.site-header__profile ul li:nth-child(2)').find('a').attr('href');
          Cookies.set('imda_profile', 'profile1', { expires: 365 });
          Cookies.set('visited_homepage', true, { expires: 365 });
          $('.profile-icon').html(purpleIcon);
          $('.profile-icon').removeClass('defaultIcon');
          $('.site-header__profile nav').find('a.active').parent().addClass('active');
          if (!$('.site-header__profile button').hasClass('purpleShadow')) {
            $('.site-header__profile button').addClass('purpleShadow');
            $('.site-header__profile button').removeClass('maroonShadow turquoiseShadow');
          }
          if (!$('.site-header__profile nav').hasClass('purpleShadow')) {
            $('.site-header__profile nav').addClass('purpleShadow');
            $('.site-header__profile nav').removeClass('maroonShadow turquoiseShadow');
          }
          if(!$('.site-header__mobile-ui .profile-indicator').hasClass('purpleShadow')) {
            $('.site-header__mobile-ui .profile-indicator').addClass('purpleShadow');
            $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow turquoiseShadow');
          }
          if($('.newsletter').length && $('.newsletter picture').find('img').length) {
            let url = $('.newsletter picture').find('img').data('profile1');
            $('.newsletter picture').find('img').attr('src', url);
            $('.newsletter picture').find('source').attr('srcset', url);
          }
          if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
            $('.horizontal-accordion__image').each((index, element) => {
              let url = $(element).find('img').data('profile1');
              $(element).find('img').attr('src', url);
            });
          }
          if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
            $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
              const url = $(ele).data('profile1');
              $(ele).attr('src', url);
            });
          }
          if($('ul').length && $('ul > li').length) {
            $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
          }
          $(`.site-header__profile ul li a[href="${profileUrl}"]`).addClass('active');
          $(`.site-header__profile ul li a[href="${profileUrl}"]`).parent().addClass('active'); 
          $(`.site-header__profile button span:nth-child(2)`).text($(`.site-header__profile ul li a[href="${profileUrl}"]`).text());
        }
        if($('body').hasClass('imda-profile2')) {
          let profileUrl = $('.site-header__profile ul li:nth-child(3)').find('a').attr('href');
          Cookies.set('imda_profile', 'profile2', { expires: 365 });
          Cookies.set('visited_homepage', true, { expires: 365 });
          $('.profile-icon').html(maroonIcon);
          $('.profile-icon').removeClass('defaultIcon');
          $('.site-header__profile nav').find('a.active').parent().addClass('active');
          if (!$('.site-header__profile button').hasClass('maroonShadow')) {
            $('.site-header__profile button').addClass('maroonShadow');
            $('.site-header__profile button').removeClass('purpleShadow turquoiseShadow');
          }
          if (!$('.site-header__profile nav').hasClass('maroonShadow')) {
            $('.site-header__profile nav').addClass('maroonShadow');
            $('.site-header__profile nav').removeClass('purpleShadow turquoiseShadow');
          }
          if(!$('.site-header__mobile-ui .profile-indicator').hasClass('maroonShadow')) {
            $('.site-header__mobile-ui .profile-indicator').addClass('maroonShadow');
            $('.site-header__mobile-ui .profile-indicator').removeClass('purpleShadow turquoiseShadow');
          }
          if($('.newsletter').length && $('.newsletter picture').find('img').length) {
            let url = $('.newsletter picture').find('img').data('profile2');
            $('.newsletter picture').find('img').attr('src', url); 
            $('.newsletter picture').find('source').attr('srcset', url);         
          }
          if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
            $('.horizontal-accordion__image').each((index, element) => {
              let url = $(element).find('img').data('profile2');
              $(element).find('img').attr('src', url);        
            });
          }
          if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
            $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
              const url = $(ele).data('profile2');
              $(ele).attr('src', url);
            });
          }
          if($('ul').length && $('ul > li').length) {
            $('ul:not(.tags-list__list) > li').addClass('maroon');
            $('ul:not(.tags-list__list) > li').removeClass('turquoise');
          }
          $(`.site-header__profile ul li a[href="${profileUrl}"]`).addClass('active');
          $(`.site-header__profile ul li a[href="${profileUrl}"]`).parent().addClass('active'); 
          $(`.site-header__profile button span:nth-child(2)`).text($(`.site-header__profile ul li a[href="${profileUrl}"]`).text());
        }
        if($('body').hasClass('imda-profile3')) {
          let profileUrl = $('.site-header__profile ul li:nth-child(4)').find('a').attr('href');
          Cookies.set('imda_profile', 'profile3', { expires: 365 });
          Cookies.set('visited_homepage', true, { expires: 365 });
          $('.profile-icon').html(tealIcon);
          $('.profile-icon').removeClass('defaultIcon');
          $('.site-header__profile nav').find('a.active').parent().addClass('active');
          if (!$('.site-header__profile button').hasClass('turquoiseShadow')) {
            $('.site-header__profile button').addClass('turquoiseShadow');
            $('.site-header__profile button').removeClass('maroonShadow purpleShadow');
          }
          if (!$('.site-header__profile nav').hasClass('turquoiseShadow')) {
            $('.site-header__profile nav').addClass('turquoiseShadow');
            $('.site-header__profile nav').removeClass('maroonShadow purpleShadow');
          }
          if(!$('.site-header__mobile-ui .profile-indicator').hasClass('turquoiseShadow')) {
            $('.site-header__mobile-ui .profile-indicator').addClass('turquoiseShadow');
            $('.site-header__mobile-ui .profile-indicator').removeClass('purpleShadow maroonShadow');
          }
          if($('.newsletter').length && $('.newsletter picture').find('img').length) {
            let url = $('.newsletter picture').find('img').data('profile3');
            $('.newsletter picture').find('img').attr('src', url);
            $('.newsletter picture').find('source').attr('srcset', url);
          }
          if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
            $('.horizontal-accordion__image').each((index, element) => {
              let url = $(element).find('img').data('profile3');
              $(element).find('img').attr('src', url);
            });
          }
          if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
            $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
              const url = $(ele).data('profile1');
              $(ele).attr('src', url);
            });
          }
          if($('ul').length && $('ul > li').length) {
            $('ul:not(.tags-list__list) > li').addClass('turquoise');
            $('ul:not(.tags-list__list) > li').removeClass('maroon');
          }
          $(`.site-header__profile ul li a[href="${profileUrl}"]`).addClass('active');
          $(`.site-header__profile ul li a[href="${profileUrl}"]`).parent().addClass('active'); 
          $(`.site-header__profile button span:nth-child(2)`).text($(`.site-header__profile ul li a[href="${profileUrl}"]`).text());
        }
        $('.site-header__mobile-ui .profile-indicator').text('');
        $('.site-header__mobile-ui .profile-icon').removeClass('hide');
      }
    } else {
      // class handler for internal pages
      $('.site-header__profile ul li a.active').removeClass('active');
      if (profile === 'profile0') {
        //$(`.site-header__profile ul li a[href="#profile1"]`).addClass('active');
        //$(`.site-header__profile button span:first-child`).text($(`.site-header__profile ul li a[href="#profile1"]`).text());
        $('.site-header__mobile-ui .profile-indicator').text('Select a profile');
        $('.site-header__mobile-ui .profile-icon').addClass('hide');
        $(`.site-header__profile button span:nth-child(2)`).text('Select a profile');
        $('.profile-icon').html(defaultIcon);
        $('.profile-icon').addClass('defaultIcon');
        $('.site-header__profile .profile-icon').empty();
        //$('.site-header__mobile-ui .profile-indicator').text($(`.site-header__profile ul li a[href="#profile1"]`).text());
      } else {
        $(`.site-header__profile ul li a[href="#${profile}"]`).addClass('active');
        $(`.site-header__profile ul li a[href="#${profile}"]`).parent().addClass('active');
        $(`.site-header__profile button span:nth-child(2)`).text($(`.site-header__profile ul li a[href="#${profile}"]`).text());
        $('.site-header__mobile-ui .profile-indicator').text('');
        $('.site-header__mobile-ui .profile-icon').removeClass('hide');
      }
      switch (profile) {

        case 'profile0':
          if(!$('body').hasClass('imda-profile0')) {
            $('body').removeClass(`${profileClassnames[0]} ${profileClassnames[1]} ${profileClassnames[2]}`).addClass(profileClassnames[3]);
          }
          if($('.page-banner--dark--detail').length) {
            $('.page-banner').addClass('page-banner--white');
            $('.page-banner').removeClass('page-banner--purple page-banner--maroon page-banner--teal');
          }
          $('.site-header__mobile-ui .profile-indicator').text('Select a profile');
          $('.site-header__mobile-ui .profile-icon').addClass('hide');
          $('.profile-icon').html(defaultIcon);
          $('.profile-icon').addClass('defaultIcon');
          $('.site-header__profile .profile-icon').empty();
          if (!$('.site-header__profile button').hasClass('purpleShadow')) {
            $('.site-header__profile button').addClass('purpleShadow');
            $('.site-header__profile button').removeClass('maroonShadow turquoiseShadow');
          }
          if (!$('.site-header__profile nav').hasClass('purpleShadow')) {
            $('.site-header__profile nav').addClass('purpleShadow');
            $('.site-header__profile nav').removeClass('maroonShadow turquoiseShadow');
          }
          if(!$('.site-header__mobile-ui .profile-indicator').hasClass('purpleShadow')) {
            $('.site-header__mobile-ui .profile-indicator').addClass('purpleShadow');
            $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow turquoiseShadow');
          }
          if($('.newsletter').length && $('.newsletter picture').find('img').length) {
            let url = $('.newsletter picture').find('img').data('profile1');
            $('.newsletter picture').find('img').attr('src', url);
            $('.newsletter picture').find('source').attr('srcset', url);
          }
          if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
            $('.horizontal-accordion__image').each((index, element) => {
              let url = $(element).find('img').data('profile1');
              $(element).find('img').attr('src', url);
            });
          }
          if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
            $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
              const url = $(ele).data('profile1');
              $(ele).attr('src', url);
            });
          }
          if($('ul').length && $('ul > li').length) {
            $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
          }
          break;

          case 'profile1':
          if(!$('body').hasClass('imda-profile1')) {
            $('body').removeClass(`${profileClassnames[1]} ${profileClassnames[2]} ${profileClassnames[3]}`).addClass(profileClassnames[0]);
          }
          if(!$('body').hasClass('imda-theme--purple')) {
            $('body').addClass('imda-theme--purple');
          }
          if($('.page-banner--dark--detail').length) {
            $('.page-banner').addClass('page-banner--white');
            $('.page-banner').removeClass('page-banner--purple page-banner--maroon page-banner--teal');
          }
          $('.profile-icon').html(purpleIcon);
          $('.profile-icon').removeClass('defaultIcon');
          if (!$('.site-header__profile button').hasClass('purpleShadow')) {
            $('.site-header__profile button').addClass('purpleShadow');
            $('.site-header__profile button').removeClass('maroonShadow turquoiseShadow');
          }
          if (!$('.site-header__profile nav').hasClass('purpleShadow')) {
            $('.site-header__profile nav').addClass('purpleShadow');
            $('.site-header__profile nav').removeClass('maroonShadow turquoiseShadow');
          }
          if(!$('.site-header__mobile-ui .profile-indicator').hasClass('purpleShadow')) {
            $('.site-header__mobile-ui .profile-indicator').addClass('purpleShadow');
            $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow turquoiseShadow');
          }
          if($('.newsletter').length && $('.newsletter picture').find('img').length) {
            let url = $('.newsletter picture').find('img').data('profile1');
            $('.newsletter picture').find('img').attr('src', url);
            $('.newsletter picture').find('source').attr('srcset', url);
          }
          if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
            $('.horizontal-accordion__image').each((index, element) => {
              let url = $(element).find('img').data('profile1');
              $(element).find('img').attr('src', url);
            });
          }
          if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
            $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
              const url = $(ele).data('profile1');
              $(ele).attr('src', url);
            });
          }
          if($('ul').length && $('ul > li').length) {
            $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
          }
          break;
          
          case 'profile2':
          if(!$('body').hasClass('imda-profile2')) {
            $('body').removeClass(`${profileClassnames[0]} ${profileClassnames[2]} ${profileClassnames[3]}`).addClass(profileClassnames[1]);
          }
          if($('.page-banner--dark--detail').length) {
            $('.page-banner').addClass('page-banner--white');
            $('.page-banner').removeClass('page-banner--purple page-banner--maroon page-banner--teal');
          }
          $('.page-banner').removeClass('page-banner--teal');
          $('.profile-icon').html(maroonIcon);
          $('.profile-icon').removeClass('defaultIcon');
          if (!$('.site-header__profile button').hasClass('maroonShadow')) {
            $('.site-header__profile button').addClass('maroonShadow');
            $('.site-header__profile button').removeClass('purpleShadow turquoiseShadow');
          }
          if (!$('.site-header__profile nav').hasClass('maroonShadow')) {
            $('.site-header__profile nav').addClass('maroonShadow');
            $('.site-header__profile nav').removeClass('purpleShadow turquoiseShadow');
          }
          if(!$('.site-header__mobile-ui .profile-indicator').hasClass('maroonShadow')) {
            $('.site-header__mobile-ui .profile-indicator').addClass('maroonShadow');
            $('.site-header__mobile-ui .profile-indicator').removeClass('purpleShadow turquoiseShadow');
          }
          if($('.newsletter').length && $('.newsletter picture').find('img').length) {
            let url = $('.newsletter picture').find('img').data('profile2');
            $('.newsletter picture').find('img').attr('src', url);
            $('.newsletter picture').find('source').attr('srcset', url);
          }
          if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
            $('.horizontal-accordion__image').each((index, element) => {
              let url = $(element).find('img').data('profile2');
              $(element).find('img').attr('src', url);
            });
          }
          if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
            $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
              const url = $(ele).data('profile2');
              $(ele).attr('src', url);
            });
          }
          if($('ul').length && $('ul > li').length) {
            $('ul:not(.tags-list__list) > li').addClass('maroon');
            $('ul:not(.tags-list__list) > li').removeClass('turquoise');
          }
          break;
          
          case 'profile3':
          if(!$('body').hasClass('imda-profile3')) {
            $('body').removeClass(`${profileClassnames[0]} ${profileClassnames[1]} ${profileClassnames[3]}`).addClass(profileClassnames[2]);
          }
          if($('.page-banner--dark--detail').length) {
            $('.page-banner').addClass('page-banner--white');
            $('.page-banner').removeClass('page-banner--purple page-banner--maroon page-banner--teal');
          }
          $('.page-banner').removeClass('page-banner--maroon');
          $('.profile-icon').html(tealIcon);
          $('.profile-icon').removeClass('defaultIcon');
          if (!$('.site-header__profile button').hasClass('turquoiseShadow')) {
            $('.site-header__profile button').addClass('turquoiseShadow');
            $('.site-header__profile button').removeClass('maroonShadow purpleShadow');
          }
          if (!$('.site-header__profile nav').hasClass('turquoiseShadow')) {
            $('.site-header__profile nav').addClass('turquoiseShadow');
            $('.site-header__profile nav').removeClass('maroonShadow purpleShadow');
          }
          if(!$('.site-header__mobile-ui .profile-indicator').hasClass('turquoiseShadow')) {
            $('.site-header__mobile-ui .profile-indicator').addClass('turquoiseShadow');
            $('.site-header__mobile-ui .profile-indicator').removeClass('purpleShadow maroonShadow');
          }
          if($('.newsletter').length && $('.newsletter picture').find('img').length) {
            let url = $('.newsletter picture').find('img').data('profile3');
            $('.newsletter picture').find('img').attr('src', url);
            $('.newsletter picture').find('source').attr('srcset', url);
          }
          if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
            $('.horizontal-accordion__image').each((index, element) => {
              let url = $(element).find('img').data('profile3');
              $(element).find('img').attr('src', url);
            });
          }
          if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
            $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
              const url = $(ele).data('profile3');
              $(ele).attr('src', url);
            });
          }
          if($('ul').length && $('ul > li').length) {
            $('ul:not(.tags-list__list) > li').addClass('turquoise');
            $('ul:not(.tags-list__list) > li').removeClass('maroon');
          }
          break;
      
        default:
          break;
      }
    }
  }
}

export function userDataTrigger() {
  if(Cookies.get('imda_profile')) {
    let profile = Cookies.get('imda_profile');

    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
      'event': 'UserData',
      'imda_profile': `${profile}`
    });
  }
}
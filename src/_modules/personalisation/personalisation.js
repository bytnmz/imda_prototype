'use strict';

import Cookies from 'js-cookie';
import { filterCookieItems, sortCookieItems } from '../../_scripts/cookieHelpers';

export default class Personalisation {
  constructor(config) {
    let industries = (Cookies.get('IMDA_industries_unfiltered')) ? Cookies.get('IMDA_industries_unfiltered') : Cookies.get('IMDA_industries');
    let categories = (Cookies.get('IMDA_categories_unfiltered')) ? Cookies.get('IMDA_categories_unfiltered') : Cookies.get('IMDA_categories');
    let topics = (Cookies.get('IMDA_topics_unfiltered')) ? Cookies.get('IMDA_topics_unfiltered') : Cookies.get('IMDA_topics');
    let supports = (Cookies.get('IMDA_supports_unfiltered')) ? Cookies.get('IMDA_supports_unfiltered') : Cookies.get('IMDA_supports');

    window.cookieIndustries = (industries) ? JSON.parse(industries) : [];
    window.cookieCategories = (categories) ? JSON.parse(categories) : [];
    window.cookieTopics = (topics) ? JSON.parse(topics) : [];
    window.cookieSupports = (supports) ? JSON.parse(supports) : [];

    /*
      - to register the cookie points from loading the detail pages
      - Programme, Grant, Scholarship, Regulation, License
      - Success Story
    */
    if (window.IMDA_Personalise) {
      // check if the personalisation points is found on page load
      this.addCookies();
    }
  }

  addCookies() {
    let industries = window.IMDA_Personalise['industries'];
    let categories = window.IMDA_Personalise['categories'];
    let topics= window.IMDA_Personalise['topics'];

    if (industries) {
      industries.map((industry) => {
        let index = this.getValueIndex(window.cookieIndustries, industry.value);
        if (index > -1) {
          let point = parseInt(window.cookieIndustries[index].point);
          window.cookieIndustries[index].point = point + industry.point;
        } else {
          window.cookieIndustries.push({
            value: industry.value,
            point: industry.point
          });
        }
      });

      const sortedCookie = sortCookieItems(window.cookieIndustries);
      const filteredCookie = filterCookieItems(sortedCookie);

      Cookies.set('IMDA_industries_unfiltered', JSON.stringify(window.cookieIndustries));
      Cookies.set("IMDA_industries", JSON.stringify(filteredCookie));
    }

    if (categories) {
      categories.map((category) => {
        let index = this.getValueIndex(window.cookieCategories, category.value);
        if (index > -1) {
          let point = parseInt(window.cookieCategories[index].point);
          window.cookieCategories[index].point = point + category.point;
        } else {
          window.cookieCategories.push({
            value: category.value,
            point: category.point,
          });
        }
      });

      const sortedCookie = sortCookieItems(window.cookieCategories);
      const filteredCookie = filterCookieItems(sortedCookie);

      Cookies.set("IMDA_categories_unfiltered", JSON.stringify(window.cookieCategories));
      Cookies.set("IMDA_categories", JSON.stringify(filteredCookie));
    }

    if (topics) {
      topics.map((topic) => {
        let index = this.getValueIndex(window.cookieTopics, topic.value);
        if (index > -1) {
          let point = parseInt(window.cookieTopics[index].point);
          window.cookieTopics[index].point = point + topic.point;
        } else {
          window.cookieTopics.push({
            value: topic.value,
            point: topic.point,
          });
        }
      });

      const sortedCookie = sortCookieItems(window.cookieTopics);
      const filteredCookie = filterCookieItems(sortedCookie);

      Cookies.set("IMDA_topics_unfiltered", JSON.stringify(window.cookieTopics));
      Cookies.set("IMDA_topics", JSON.stringify(filteredCookie));
    }
  }

  getValueIndex(cookies, value) {
    let index = -1;
    cookies.map((item, i) => {
      if (item.value == value) {
        index = i;

        return false;
      }
    });

    return index;
  }
}

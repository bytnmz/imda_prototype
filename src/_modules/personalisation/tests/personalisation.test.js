'use strict';

import Personalisation from '../personalisation';

describe('Personalisation View', function() {

  beforeEach(() => {
    this.personalisation = new Personalisation();
  });

  it('Should run a few assertions', () => {
    expect(this.personalisation).toBeDefined();
  });

});

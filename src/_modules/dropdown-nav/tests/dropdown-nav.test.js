'use strict';

import DropdownNav from '../dropdown-nav';

describe('DropdownNav View', function() {

  beforeEach(() => {
    this.dropdownNav = new DropdownNav();
  });

  it('Should run a few assertions', () => {
    expect(this.dropdownNav).toBeDefined();
  });

});

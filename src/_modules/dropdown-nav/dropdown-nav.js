'use strict';

import $ from 'jquery';
import Emitter from 'tiny-emitter';

export default class DropdownNav {
  /* REQUIREMENT:
  - tiny-emitter is required (https://www.npmjs.com/package/tiny-emitter)
  - check HTML structure in dropdown-nav.js
  - initialise by calling new DropdownNav($nav)
  */
  constructor($selector) {
    let $list = $('ul', $selector);
    $list.addClass('dropdown-nav__list');

    this.$dropdown = $selector;
    this.isShown = false;

    // Setup trigger button
    let $triggerButton = $('<button class="dropdown-nav__trigger" type="button"></button>');
    let activeLinkLabel = $selector.find('.active a').text();
    let $label = $(`<span class="label">${activeLinkLabel}</span>`);
    let $icon = $('<span class="icon icon-chevron-down"></span>');

    $triggerButton.append($label);
    $triggerButton.append($icon);

    $triggerButton.prependTo($selector);

    $triggerButton.on('click', e => {
      e.preventDefault();

      if (this.isShown) {
        this.hideList();
      } else {
        window.emitter.emit('dropdownOpened', $selector);
        this.showList();
      }
    });

    // to close the dropdown when other dropdown is being opened
    window.emitter.on('dropdownOpened', ($openedDropdown) => {
      if ($selector != $openedDropdown) {
        this.hideList();
      }
    });

    $(document).on('click.dropdownNav', e => {
      let $eTarget = $(e.target);

      if (!$eTarget.hasClass('dropdown-nav__trigger') && !$eTarget.parents('.dropdown-nav__trigger').length && !$eTarget.hasClass('dropdown-nav__list') && !$eTarget.parents('.dropdown-nav__list').length) {
        this.hideList();
      }
    });
  }

  showList() {
    this.isShown = true;
    this.$dropdown.addClass('list-shown');
  }

  hideList() {
    this.isShown = false;
    this.$dropdown.removeClass('list-shown');
  }
}

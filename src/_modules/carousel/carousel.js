'use strict';

import 'slick-carousel';

export default class Carousel {
  constructor($selector, selfInit = true, options, initSlick = null, before = null, after = null, setPos = null, slickInit = true) {
		this.$selector = $selector;
    this.options = this.$selector.data('options') ? this.$selector.data('options') : options;
		this.desktop = $(window).width() >= 1024 ? true : false;
    if(selfInit) this.init(initSlick, before, after, setPos, slickInit);
  }

  init(init = () => {}, before = (e, slick, currentSlide, nextSlide) => {}, after = (e, slick, currentSlide) => {}, setPos = null, slickInit) {
		const $arrows = this.$selector.parent().find('[data-carousel-arrows]').length ? this.$selector.parent().find('[data-carousel-arrows]') : this.$selector.closest('section').find('[data-carousel-arrows]').length ? this.$selector.closest('section').find('[data-carousel-arrows]') : this.$selector;

		let options = {};

		const defaultOptions = {
			rows: 0,
			prevArrow: '<button type="button" class="slick-prev slick-arrow"><span class="sr-only">Previous</span><span class="icon icon-chevron-left"></span></button>',
      nextArrow: '<button type="button" class="slick-next slick-arrow"><span class="sr-only">Next</span><span class="icon icon-chevron-right"></span></button>',
			appendArrows: $arrows
		};
    
		options = { ...defaultOptions, ...this.options };

		if(this.$selector.parent().find('[data-carousel-dots]').length) {
			const $dots = this.$selector.parent().find('[data-carousel-dots]');
			const dotOption = {
				appendDots: $dots
			};

			options = { ...options, ...dotOption };
		}

		if(this.$selector.data('navfor')) {
			const $navFor = $(`${this.$selector.data('navfor')}`);

			const navForOption = {
				asNavFor: $navFor
			}

			options = { ...options, ...navForOption };
		}

		this.fulloptions = options;

		if(this.$selector.parent().find('[data-carousel-pager]').length) {
			this.$selector.on('init reInit', (event, slick, currentSlide, nextSlide) => {
				this.$selector.parent().find('[data-carousel-pager]').text(`${((currentSlide ? currentSlide : 0) + 1) > 9 ? (currentSlide ? currentSlide : 0) + 1 : `0${(currentSlide ? currentSlide : 0) + 1}`} / ${slick.slideCount > 9 ? slick.slideCount : `0${slick.slideCount}`}`);
			});
			this.$selector.on('beforeChange', (event, slick, currentSlide, nextSlide) => {
				this.$selector.parent().find('[data-carousel-pager]').text(`${((nextSlide ? nextSlide : 0) + 1) > 9 ? (nextSlide ? nextSlide : 0) + 1 : `0${(nextSlide ? nextSlide : 0) + 1}`} / ${slick.slideCount > 9 ? slick.slideCount : `0${slick.slideCount}`}`);
			});
		}

    this.$selector.on('init reInit', (e, slick, currentSlide) => {
      this._onPrint(this.$selector);
			if (init) init(e, slick, currentSlide);
    });

		if(slickInit) {
			this.$selector.slick(options);
		}

    if (before) this.$selector.on('beforeChange', before);

    if (after) this.$selector.on('afterChange', after);

		this.$selector.on('setPosition', (e, slick) => {
			if (setPos) setPos();
		});
  }

  _onPrint($carousel){
		let beforePrint = function () {
			console.log('Functionality to run before printing.');
			$carousel.slick('setPosition');
		};

		let afterPrint = function () {
			console.log('Functionality to run after printing');
			$carousel.slick('setPosition');
		};

		if (window.matchMedia) {
			let mediaQueryList = window.matchMedia('print');

			mediaQueryList.addListener(function (mql) {
				if (mql.matches) {
					beforePrint();
				} else {
					afterPrint();
				}
			});
		}

		window.onbeforeprint = beforePrint;
		window.onafterprint = afterPrint;
	}

	setPosition() {
		this.$selector.slick('setPosition');
	}

	goToSlide(index) {
		this.$selector.slick('slickGoTo', index);
	}

	slickPause() {
		this.$selector.slick('slickPause');
	}
	
	slickPlay() {
		this.$selector.slick('slickPlay');
	}

  slickInit() {
		this.$selector.slick(this.fulloptions);
  }
	
  unslick() {
		this.$selector.slick('unslick');
  }
}

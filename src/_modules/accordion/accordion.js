'use strict';

import $ from 'jquery';

export default class Accordion {
  constructor($selector) {
    let $groups = $selector.find('.accordion-group');

    this.$accordion = $selector;

    $groups.map((i, ele) => {
      let $group = $(ele);

      this._initGroup($group);
    });
  }

  _initGroup($group) {
    let $title = $group.find('.accordion-title');
    let $content = $group.find('.accordion-content');

    $title.on('click', (e) => {
      e.preventDefault();

      let expanded = $group.data('expanded');

      if (expanded) {
        this.collapseGroup($group);
      } else {
        this.expandGroup($group);
      }
    });
  }

  expandGroup($group) {
    let $title = $group.find('.accordion-title');
    let $content = $group.find('.accordion-content');

    let $activeGroup = this.$accordion.find('.accordion-group.expanded');
    if ($activeGroup.length > 0) {
      $activeGroup.map((i, ele) => {
        let $this = $(ele);
        this.collapseGroup($this);
      });
    }

    $group.addClass('expanding');
    $group.data('expanded', true);

    $content.slideDown(() => {
      $('html, body').animate({
        scrollTop: $title.offset().top
      });

      $group.addClass('expanded');
    });
  }

  collapseGroup($group) {
    let $content = $group.find('.accordion-content');
    $content.slideUp(() => {});
    $group.data('expanded', false);
    $group.removeClass('expanded');
    $group.removeClass('expanding');
  }
}

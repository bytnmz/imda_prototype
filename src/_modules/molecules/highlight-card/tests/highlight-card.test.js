'use strict';

import HighlightCard from '../highlight-card';

describe('HighlightCard View', function() {

  beforeEach(() => {
    this.highlightCard = new HighlightCard();
  });

  it('Should run a few assertions', () => {
    expect(this.highlightCard).toBeDefined();
  });

});

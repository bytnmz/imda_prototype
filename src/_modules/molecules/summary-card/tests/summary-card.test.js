'use strict';

import SummaryCard from '../summary-card';

describe('SummaryCard View', function() {

  beforeEach(() => {
    this.summaryCard = new SummaryCard();
  });

  it('Should run a few assertions', () => {
    expect(this.summaryCard).toBeDefined();
  });

});

'use strict';

import NameCard from '../name-card';

describe('NameCard View', function() {

  beforeEach(() => {
    this.nameCard = new NameCard();
  });

  it('Should run a few assertions', () => {
    expect(this.nameCard).toBeDefined();
  });

});

'use strict';

import CookieMessage from '../cookie-message';

describe('CookieMessage View', function() {

  beforeEach(() => {
    this.cookieMessage = new CookieMessage();
  });

  it('Should run a few assertions', () => {
    expect(this.cookieMessage).toBeDefined();
  });

});

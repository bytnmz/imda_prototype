'use strict';

import $ from 'jquery';
import Cookies from 'js-cookie';

export default class CookieMessage {
  constructor() {
    let $cookieMsg = $('.cookie-message');
    let $content = $('.cookie-message__content', $cookieMsg);
    let $closeBtn = $('.btn-close', $cookieMsg);
    let cookieToken = $content.data('token');

    this.$cookieMsg = $cookieMsg;

    $closeBtn.on('click', e => {
      e.preventDefault();

      this.hide();

      Cookies.set('cookieMsg', 'closed', { expires: 30 });
      Cookies.set('cookieToken', cookieToken, { expires: 30 });
    });

    if (!Cookies.get('cookieMsg')) {
      this.show();
    } else {
      if (Cookies.get('cookieToken') != cookieToken) {
        this.show();
      }
    }
  }

  hide() {
    this.$cookieMsg.slideUp();
  }

  show() {
    this.$cookieMsg.slideDown();
  }
}

'use strict';

import PanelCard from '../panel-card';

describe('PanelCard View', function() {

  beforeEach(() => {
    this.panelCard = new PanelCard();
  });

  it('Should run a few assertions', () => {
    expect(this.panelCard).toBeDefined();
  });

});

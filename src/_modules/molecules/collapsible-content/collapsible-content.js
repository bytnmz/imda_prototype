'use strict';

export default class CollapsibleContent {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.state = {
      "expanded": false,
      "expandLabel": $('.collapsible-content__collapsible__handle button', this.$selector).data('expandlabel'),
      "collapseLabel": $('.collapsible-content__collapsible__handle button', this.$selector).data('collapselabel')
    }

    if(selfInit) this.init();
  }

  init() {
    $('.collapsible-content__collapsible__handle button', this.$selector).on('click', (e) => {
      e.preventDefault();

      if(!this.state.expanded) {
        $('.collapsible-content__collapsible__content', this.$selector).slideDown(300, () => {
          $('.collapsible-content__collapsible__handle button', this.$selector).text(this.state.collapseLabel);
        });
      }
      else {
        $('.collapsible-content__collapsible__content', this.$selector).slideUp(300, () => {
          $('.collapsible-content__collapsible__handle button', this.$selector).text(this.state.expandLabel);
        });
      }

      this.state.expanded = !this.state.expanded;
    });
  }
}

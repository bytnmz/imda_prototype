'use strict';

import $ from 'jquery';
import 'slick-carousel';


export default class ContentCarousel {
  constructor() {
    let sliderSpeed = $('.content-carousel').data('slider-speed') || 5000;

    let noOfBanners = $('.content-slide').length;

    if (noOfBanners === 1) {
      $('.slider-nav').hide();
    } else {
      $('.slider-nav').show();
    }

    $('.content-slider', '.content-carousel').slick({
      autoplay: true,
      autoplaySpeed: sliderSpeed,
      adaptiveHeight: true,
      prevArrow:$('.slider-nav--prev'),
      nextArrow:$('.slider-nav--next'),
      appendDots: $('.content-carousel-dots'),
      dots: true,
    });
  }
}

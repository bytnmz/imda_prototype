'use strict';

import $ from 'jquery';

export default class VideoPlayer {
  constructor($selector, selfInit = true, playerData) {
    this.$selector = $selector;

    if(selfInit) this.init(playerData);
  }

  init(playerData) {
    this.$selector.find('.btn-play').on('click', e => {
      this.play();
      playerData.YTPlayer.playVideo();
    });
  }

  onReady($video) {
    $video.addClass('video-ready');
  }

  play() {
    this.$selector.addClass('video-play');
  }
}

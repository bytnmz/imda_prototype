'use strict';

import $ from 'jquery';
import CustomSelect from '../../custom-select/custom-select';
import DropdownNav from '../../dropdown-nav/dropdown-nav';

export default class ListingFilter {
  constructor($selector) {
    let $selections = $('.listing-filter__selection', $selector);
    let $header = $('.listing-filter__header', $selector);
    let $expandBtn = $('.btn-pill', $header);
    let $doneBtn = $('.listing-filter__footer button.btn', $selector);
    let $filters = $('.listing-filter__filters', $selector);
    let $navs = $('.listing-filter__nav', $selector);

    this.expanded = false;

    this.$expandBtn = $expandBtn;
    this.$filters = $filters;

    $expandBtn.on('click', e => {
      e.preventDefault();
      if (this.expanded) {
        this.hideFilters();
      } else {
        this.showFilters();
      }
    });

    if($doneBtn.length) {
      $doneBtn.on('click', e => {
        e.preventDefault();
        if (this.expanded) {
          this.hideFilters();
        }
      });
    }

    $selections.map((i, ele) => {
      let $this = $(ele);

      let selection = new CustomSelect($this, true);
    });

    $navs.map((i, ele) => {
      let $this = $(ele);

      let nav = new DropdownNav($this);
    });
  }

  showFilters() {
    this.$filters.slideDown();
    if($('.news-listing, .events-listing').length) {
      this.$expandBtn.find('.vh').text("Done");
      this.$expandBtn.addClass('expanded');
    }
    else {
      this.$expandBtn.find('.vh').text("Done");
    }
    this.expanded = true;
  }
  
  hideFilters() {
    this.$filters.slideUp();
    if($('.news-listing, .events-listing').length) {
      this.$expandBtn.find('.vh').text("Show");
      this.$expandBtn.removeClass('expanded');
    }
    else {
      this.$expandBtn.find('.vh').text("Show");
    }
    this.expanded = false;
  }
}

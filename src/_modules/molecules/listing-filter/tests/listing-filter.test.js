'use strict';

import ListingFilter from '../listing-filter';

describe('ListingFilter View', function() {

  beforeEach(() => {
    this.listingFilter = new ListingFilter();
  });

  it('Should run a few assertions', () => {
    expect(this.listingFilter).toBeDefined();
  });

});

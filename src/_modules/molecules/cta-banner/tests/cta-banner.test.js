'use strict';

import CtaBanner from '../cta-banner';

describe('CtaBanner View', function() {

  beforeEach(() => {
    this.ctaBanner = new CtaBanner();
  });

  it('Should run a few assertions', () => {
    expect(this.ctaBanner).toBeDefined();
  });

});

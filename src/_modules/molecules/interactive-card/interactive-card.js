'use strict';

import $ from 'jquery';


export default class InteractiveCard {
  constructor() {
    $('.interactive-card').click((e)=>{
      let $this = e.currentTarget;
      let currentItem =  $($this).find('.detail-info-section');

      if (currentItem.parents('.interactive-card').hasClass('interactive-card--expanded')){
        currentItem.slideUp();
        currentItem.parents('.interactive-card').removeClass('interactive-card--expanded');
      } else {
        $('.detail-info-section').each((index, element)=>{
          if ($(element).parents('.interactive-card').hasClass('interactive-card--expanded')) {
            $(element).parents('.interactive-card').removeClass('interactive-card--expanded');
            $(element).slideUp();
          }
        });
        $($this).addClass('interactive-card--expanded');
        $($this).find('.detail-info-section').slideDown();
      }
    }) ;
  }
}

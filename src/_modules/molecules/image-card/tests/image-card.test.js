'use strict';

import ImageCard from '../image-card';

describe('ImageCard View', function() {

  beforeEach(() => {
    this.imageCard = new ImageCard();
  });

  it('Should run a few assertions', () => {
    expect(this.imageCard).toBeDefined();
  });

});

'use strict';

import ImageLink from '../image-link';

describe('ImageLink View', function() {

  beforeEach(() => {
    this.imageLink = new ImageLink();
  });

  it('Should run a few assertions', () => {
    expect(this.imageLink).toBeDefined();
  });

});

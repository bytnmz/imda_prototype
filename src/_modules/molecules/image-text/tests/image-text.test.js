'use strict';

import ImageText from '../image-text';

describe('ImageText View', function() {

  beforeEach(() => {
    this.imageText = new ImageText();
  });

  it('Should run a few assertions', () => {
    expect(this.imageText).toBeDefined();
  });

});

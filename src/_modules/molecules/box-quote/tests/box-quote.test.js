'use strict';

import BoxQuote from '../box-quote';

describe('BoxQuote View', function() {

  beforeEach(() => {
    this.boxQuote = new BoxQuote();
  });

  it('Should run a few assertions', () => {
    expect(this.boxQuote).toBeDefined();
  });

});

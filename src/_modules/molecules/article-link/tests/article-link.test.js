'use strict';

import ArticleLink from '../article-link';

describe('ArticleLink View', function() {

  beforeEach(() => {
    this.articleLink = new ArticleLink();
  });

  it('Should run a few assertions', () => {
    expect(this.articleLink).toBeDefined();
  });

});

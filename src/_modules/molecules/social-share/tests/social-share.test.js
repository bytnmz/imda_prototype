'use strict';

import SocialShare from '../social-share';

describe('SocialShare View', function() {

  beforeEach(() => {
    this.socialShare = new SocialShare();
  });

  it('Should run a few assertions', () => {
    expect(this.socialShare).toBeDefined();
  });

});

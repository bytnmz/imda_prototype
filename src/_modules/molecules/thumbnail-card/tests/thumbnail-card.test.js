'use strict';

import ThumbnailCard from '../thumbnail-card';

describe('ThumbnailCard View', function() {

  beforeEach(() => {
    this.thumbnailCard = new ThumbnailCard();
  });

  it('Should run a few assertions', () => {
    expect(this.thumbnailCard).toBeDefined();
  });

});

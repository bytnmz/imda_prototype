'use strict';

import $ from 'jquery';

export default class SiteNav {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    this.megamenuTop = parseInt($('.site-nav__lvl2').css('top'));
    $('.btn-back', this.$selector).map((i,ele) => {
      $(ele).on('click', e => {
        e.preventDefault();
        this.backLevelOne($(ele));
      });
    });

    $('.site-nav__lvl1 > li', this.$selector).map((i, lvl1Link) => {
      $('> button', $(lvl1Link)).map((i,ele) => {
        $(ele).on('click', (e) => {
          e.preventDefault();

          this.expandLevelTwo($(lvl1Link));
        })
      });

      if ($(lvl1Link).hasClass('with-child')) {
        $('> a', $(lvl1Link)).on('click', (e) => {
          e.preventDefault();

          this.expandLevelTwo($(lvl1Link));
        })
      }
    });

    $(window).on('resize', () => {
      $('.site-nav__lvl2').removeAttr('style');
      this.megamenuTop = parseInt($('.site-nav__lvl2').css('top'));
    });
  }

  expandLevelTwo($level1) {
    $('.site-nav__lvl2').removeAttr("style");

    if ($level1.hasClass('active')) {
      $('.site-nav__lvl1 > li').removeClass('active');
      $('.site-header').removeClass('active');
    } else {
      if (!$('.sgds-masthead-content').hasClass('hide')) {
        let topPosToAdd = $('.sgds-masthead-content').outerHeight();

        $('.site-nav__lvl2').css({
          'top': topPosToAdd + this.megamenuTop
        })
      }

      $('.site-nav__lvl1 > li.active').removeClass('active');
      $level1.addClass('active');
      $('.site-header').addClass('active');
    }
  }

  backLevelOne($backButton) {
    $backButton.closest('.site-nav__lvl2').parent().removeClass('active');
  }
}

'use strict';

import $ from 'jquery';

export default class Notification {
  constructor() {
    let $notification = $('.notification');
    let $closeBtn = $('.btn-close', $notification);

    this.$notification = $notification;

    $closeBtn.on('click', e => {
      this.hide();

      if (!sessionStorage.getItem('notification')) {
        sessionStorage.setItem('notification', 'closed');
      }
    });

    if (!sessionStorage.getItem('notification')) {
      this.show();
    }
  }

  hide() {
    this.$notification.slideUp();
  }

  show() {
    this.$notification.slideDown();
  }
}

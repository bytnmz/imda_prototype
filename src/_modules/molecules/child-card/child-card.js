'use strict';

export default class ChildCard {
  constructor() {
    this.name = 'child-card';
    console.log('%s module', this.name.toLowerCase());
  }
}

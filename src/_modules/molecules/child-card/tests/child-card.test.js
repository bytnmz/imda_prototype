'use strict';

import ChildCard from '../child-card';

describe('ChildCard View', function() {

  beforeEach(() => {
    this.childCard = new ChildCard();
  });

  it('Should run a few assertions', () => {
    expect(this.childCard).toBeDefined();
  });

});

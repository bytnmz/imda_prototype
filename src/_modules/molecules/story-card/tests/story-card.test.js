'use strict';

import StoryCard from '../story-card';

describe('StoryCard View', function() {

  beforeEach(() => {
    this.storyCard = new StoryCard();
  });

  it('Should run a few assertions', () => {
    expect(this.storyCard).toBeDefined();
  });

});

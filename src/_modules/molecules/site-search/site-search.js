'use strict';

import $ from 'jquery';
import dot from "dot";
import Cookies from "js-cookie";

export default class SiteSearch {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.isOpened = false;
    this.action =  $('.site-search__form', this.$selector).data('action');
    this.$suggestionsTemplate = $("#globalSearchSuggestionsTemplate").html();
    this.$popularTemplate = $("#globalSearchPopularTemplate").html();
    this.$categoriesTemplate = $("#globalSearchCategoriesTemplate").html();
    this.searchTermsData = JSON.parse($('#popularSearchTermsData', this.$selector).html());
    this.endpoint = $('.site-search__form', this.$selector).data('endpoint');

    if(selfInit) this.init();
  }

  init() {
    this.renderPopularTerms(Cookies.get('imda_profile'));
    this.renderCategories(Cookies.get('imda_profile'));

    $('.btn-search', this.$selector).on('click', e => {
      this.handleSearchSubmit();
    });

    let keyupTimeout;

    const suggestionsCallback = (res) => {
      this.renderSuggestions(res);
    };

    $('.site-search__input', this.$selector).on('focus', e => {
      this.$selector.addClass("active");
    });

    $('.site-search__input', this.$selector).on('keyup', e => {
      e.preventDefault();
      clearTimeout(keyupTimeout);

      if (e.keyCode == 13 || e.which == 13) {
        this.handleSearchSubmit();
      }
      else {
        if ($('.site-search__input', this.$selector).val().length > 0) {
          $('.site-search__input', this.$selector).parent().find(".error-msg").remove();
          $(".site-search__form", this.$selector).removeClass(
            "has-error"
          );

          // fetch suggestions
          setTimeout(() => {
            this.updateData({
              endpoint: this.endpoint,
              data: { keyword: $('.site-search__input', this.$selector).val(), profile: Cookies.get('imda_profile') ? Cookies.get('imda_profile') : 'profile0'},
              callback: suggestionsCallback,
            });
          }, 250);
        } else {
          $('.site-search__results > div', this.$selector).hide();
          $('.site-search__results__popular', this.$selector).show();
          this.$selector.removeClass("active");
        }
      }
    });

    $(".btn-back, .btn-cancel", this.$selector).on("click", (e) => {
      e.preventDefault();

      $('.site-search__input', this.$selector).val("");
      $('.site-search__results > div', this.$selector).hide();
      $('.site-search__results__popular', this.$selector).show();
      this.$selector.removeClass("active");
    });

    window.emitter.on('profileChanged', (data) => {
      this.renderPopularTerms(data);
      this.renderCategories(data);
    });
  }

  open() {
    this.isOpened = true;
    this.$selector.find('input').focus();
  }

  close() {
    this.isOpened = false;
    this.$selector.find('.error-msg').remove();

    $('.site-search__results > div', this.$selector).hide();
    $('.site-search__results__popular', this.$selector).show();
    this.$selector.removeClass("active");
  }

  renderPopularTerms(profile) {
    const dotTemplate = dot.template(this.$popularTemplate)(this.searchTermsData[profile].popularSearch);
    $('.site-search__results__popular ul', this.$selector).html(dotTemplate);
    $('.site-search__results__no-results__popular', this.$selector).html(dotTemplate);
  }

  renderCategories(profile) {
    const dotTemplate = dot.template(this.$categoriesTemplate)(this.searchTermsData[profile].categories);
    $('.site-search__results__categories', this.$selector).html(dotTemplate);
  }

  renderSuggestions(data) {
    $('.site-search__results > div', this.$selector).hide();

    if(data.items.length) {
      $('.site-search__results__suggestions', this.$selector).show();
      
      const dotTemplate = dot.template(this.$suggestionsTemplate)(data);
      $('.site-search__results__suggestions', this.$selector).html(dotTemplate);
      $('.site-search__results__suggestions .btn', this.$selector).attr('href', `${this.action}?${$('.site-search__input', this.$selector).attr('name')}=${encodeURIComponent($('.site-search__input', this.$selector).val())}`);
    }
    else {
      $('.site-search__results__no-results', this.$selector).show();
    }

    this.$selector.addClass("active");
  }

  handleSearchSubmit() {
    const $errorMsg = $('.site-search__input', this.$selector).parent().find(".error-msg");

    if ($('.site-search__input', this.$selector).prop("required") && !this._searchValidated()) {
      if (!$errorMsg.length) {
        $('.site-search__input', this.$selector).after(
          '<p class="error-msg">Please enter keyword to search.</p>'
        );
        $(".site-search__form", this.$selector).addClass("has-error");
      }
    } else {
      $('.site-search__input', this.$selector).parent().find(".error-msg").remove();
      $(".site-search__form", this.$selector).removeClass("has-error");
      window.location.href = `${this.action}?${$('.site-search__input', this.$selector).attr('name')}=${encodeURIComponent($('.site-search__input', this.$selector).val())}`;
    }

    this.$selector.removeClass("active");
  }

  _searchValidated() {
    /* check if it's empty field */
    if ($('.site-search__input', this.$selector).val().trim(" ").length == 0) {
      return false;
    }
    return true;
  }

  updateData({ endpoint, data, callback }) {
    $.ajax({
      url: endpoint,
      data: JSON.stringify(data),
      method: "post",
      contentType: "application/json",
      success: (res) => {
        callback(res);
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
}

'use strict';

import $ from 'jquery';

export default class VideoBox {
  constructor() {
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.


    var player;
    function onYouTubeIframeAPIReady(playerID, videoId) {
      player = new YT.Player(playerID, {
        videoId: videoId,
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
    }


    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
      event.target.playVideo();

    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
    function onPlayerStateChange(event) {
      if (event.data == YT.PlayerState.PLAYING && !done) {
        setTimeout(stopVideo, 6000);
        done = true;
      }
    }
    function stopVideo() {
      player.stopVideo();
    }

    $('.video-image', '.video-box').on('click', function(){
      let $this = $(this);
      let videoIframe= $this.siblings('.video-iframe');
      $this.fadeOut();
      $(videoIframe).fadeIn().addClass('visible');
      let playerID = $(videoIframe).children('.youtubeplayer').data('player-id');
      var videoId = $(videoIframe).data('video-id');
      onYouTubeIframeAPIReady(playerID, videoId);
    });
  }
}

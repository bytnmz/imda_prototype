'use strict';

import TeaserCard from '../teaser-card';

describe('TeaserCard View', function() {

  beforeEach(() => {
    this.teaserCard = new TeaserCard();
  });

  it('Should run a few assertions', () => {
    expect(this.teaserCard).toBeDefined();
  });

});

'use strict';

export default class ThumbnailCardPortrait {
  constructor($selector, selfInit = true) {
    this.name = 'thumbnail-card-portrait';
    console.log('thumbnail-card-portrait module');

    if(selfInit) this.init($selector);
  }

  init($selector) {

  }
}

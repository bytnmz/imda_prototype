'use strict';

import PerfectScrollbar from 'perfect-scrollbar';

export default class Rte {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.tsTable = true; 
    this.setTableMinWidth = false;
    this.fontSizeIncrease = $(window).width() > 1023 ? false : true;
    this.fontSizeDecrease = $(window).width() > 1023 ? true : false;
    this.ps = null;

    if(selfInit) this.init();
  }

  init() {
    this.evaluateTables();

    let showHideIndicator;

    $(window).on('resize.tableResponsive', () => {
      if(!this.tsTable) {
        if(!this.$selector.parents('.table-responsive').length) {
          if(this.$selector.width() > this.$selector.parent().width()) {
            this.$selector.wrap('<div class="table-wrapper"><div class="table-responsive" /></div>');
            
            this.$selector.parent().before(`<small><span>Scroll to view</span><span class="icon icon-chevron-right"></span></small>`);
            
            const container = this.$selector.parent()[0];
            this.ps = new PerfectScrollbar(container);
          }
        }

        clearTimeout(showHideIndicator);
        showHideIndicator = setTimeout(() => {
          if(this.$selector.parents('.table-wrapper').width() < parseInt(this.$selector.data('min-width'))) {
            this.$selector.parents('.table-wrapper').removeClass('hide-indicator');
          }
          else {
            this.$selector.parents('.table-wrapper').addClass('hide-indicator');
          }

          this.ps.update();
        }, 300);
      }

      this.setTableMinWidth = (this.$selector.data('min-width') !== undefined) ? true : false;
        
      //reset min-width when font size change on tablet min-width
      if($(window).width() > 1023) {
        if(this.fontSizeIncrease) {
          this.$selector.removeClass('overflow');
          this.$selector.removeData('min-width');
          this.fontSizeIncrease = false;
          this.fontSizeDecrease = true;
          this.setTableMinWidth = false;
        }
      }
      else {
        if(this.fontSizeDecrease) {
          this.$selector.removeClass('overflow');
          this.$selector.removeData('min-width');
          this.fontSizeDecrease = false;
          this.fontSizeIncrease = true;
          this.setTableMinWidth = false;
        }
      }
      
      // Find the min-width for the table if not set yet
      if(!this.setTableMinWidth) {
        if(this.$selector.width() > this.$selector.parent().width()) {
          this.$selector.attr('data-min-width', this.$selector.width());
          this.setTableMinWidth = true;
        }
      }

      // if breakpoint is already set, check to transform table or not
      if(this.setTableMinWidth) {
        if(this.$selector.attr('data-min-width') >= this.$selector.parent().width()) {
          this.$selector.addClass('overflow');
        }
        else {
          this.$selector.removeClass('overflow');
        }
      }
      else {
        // if not already set, recheck is current table width should become breakpoint
        if(this.$selector.width() > this.$selector.parent().width()) {
          this.$selector.attr('data-min-width', this.$selector.width());
          setTableBreakpoint = true;
        }
      }
    }).trigger('resize.tableResponsive');
  }

  evaluateTables() {
    if (this.$selector.children('thead').find('tr').length === 1 && this.$selector.children('tbody').find('tr').length) {
      const headerCells = this.$selector.children('thead').find('tr th');
      const bodyCells = this.$selector.children('tbody').find('tr td');

      if (headerCells.length > 0 && bodyCells.length > 0) {
        headerCells.each((index, element) => {
          if ($(element).attr('colSpan') > 1 || $(element).attr('rowSpan') > 1) {
            this.tsTable = false;
          }
        }); 
        bodyCells.each((index, element) => {
          if ($(element).attr('colSpan') > 1 || $(element).attr('rowSpan') > 1) {
            this.tsTable = false;
          }
        });
      }

      if(this.tsTable) {
        this.$selector.addClass('ts-table tablesaw tablesaw-stack');
        this.$selector.attr('data-tablesaw-mode', 'stack');
      }
    }
    else {
      // does not meet ts table criteria
      this.tsTable = false;
    }
  }
}

'use strict';

export default class Footnote {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    const $toggle = this.$selector.find('.footnote__toggle');
    const $content = this.$selector.find('.footnote__content');

    $toggle.on('click', (e) => {
      e.preventDefault();

      if(!this.$selector.hasClass('active')) {
        $content.slideDown(300);
        this.$selector.addClass('active');
      }
      else {
        $content.slideUp(300);
        this.$selector.removeClass('active');
      }
    });
  }
}

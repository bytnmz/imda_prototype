'use strict';

export default class PurplebgSectionWrapper {
  constructor($selector, selfInit = true) {
    this.name = 'purplebg-section-wrapper';
    console.log('purplebg-section-wrapper module');

    if(selfInit) this.init($selector);
  }

  init($selector) {

  }
}

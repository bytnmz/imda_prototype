'use strict';

import MegaMenu from '../mega-menu';

describe('MegaMenu View', function() {

  beforeEach(() => {
    this.megaMenu = new MegaMenu();
  });

  it('Should run a few assertions', () => {
    expect(this.megaMenu).toBeDefined();
  });

});

'use strict';

import $ from 'jquery';

export default class MegaMenu {
  constructor() {
    let $siteNav = $('.site-nav');
    let $siteNavItems = $('li', $siteNav);

    let $megaMenu = $('.mega-menu');
    let $megaMenuContents = $('.mega-menu__content', $megaMenu);
    let $megaMenuOverlay = $('.mega-menu-overlay');

    let $arrow = $('.arrow', $megaMenu);

    this.$siteNavItems = $siteNavItems;
    this.$megaMenu = $megaMenu;
    this.$megaMenuContents = $megaMenuContents;
    this.$megaMenuOverlay = $megaMenuOverlay;

    let hoverTimeout, megaMenuLeaveTimeout;

    $siteNavItems.map((i, ele) => {
      let $this = $(ele);
      let target = $this.data('target');

      if ($(target).length) {
        $this.on('mouseenter', () => {
          hoverTimeout = setTimeout(() => {
            $siteNavItems.removeClass('active');
            $this.addClass('active');
            this.openMegaMenu(target);

            // adjust arrow
            let position = $this.position().left + ($this.outerWidth() / 2);
            $arrow.css({
              transform: 'translateX(' + position + 'px)'
            });
          }, 200);
        });
      }
    });

    $siteNavItems.on('mouseleave', e => {
      clearTimeout(hoverTimeout);
    });

    $megaMenuContents.map((i, ele) => {
      let $this = $(ele);
      let $nav = $('.mega-menu__nav', $this);
      let $navLinks = $('li', $nav);

      $this.on('mouseenter', e => {
        clearTimeout(megaMenuLeaveTimeout);
      });

      $this.on('mouseleave', e => {
        megaMenuLeaveTimeout = setTimeout(() => {
          this.closeMegaMenu();
        }, 200);
      });

      $navLinks.map((j, link) => {
        let $link = $(link);
        let target = $link.data('target');

        if ($link.hasClass('has-children')) {
          $link.find('a').append('<span class="icon icon-angle-right"></span>');
        }

        let mouseEnterTimeout;

        $link.on('mouseenter', e => {
          clearTimeout(mouseEnterTimeout);
          mouseEnterTimeout = setTimeout(() => {
            $navLinks.removeClass('active');
            $link.addClass('active');
            if ($link.hasClass('has-children')) {
              this.showChildMenu(target);
            } else {
              this.hideChildMenu($link);
            }
          }, 200);
        });

        $link.on('mouseleave', e => {
          clearTimeout(mouseEnterTimeout);
        });
      });
    });

    $(document).on('click.megaMenu', e => {
      let $eTarget = $(e.target);

      if (!$eTarget.hasClass('site-nav') && !$eTarget.parents('.site-nav').length && !$eTarget.hasClass('mega-menu__content') && !$eTarget.parents('.mega-menu__content').length) {
        this.closeMegaMenu();
      }
    });
  }

  openMegaMenu(target) {
    this.$megaMenu.addClass('expanded');
    this.$megaMenuContents.removeClass('active');
    this.$megaMenuOverlay.show(0, () => {
      this.$megaMenuOverlay.addClass('shown');
    });

    $(target).addClass('active');
  }

  closeMegaMenu() {
    this.$siteNavItems.removeClass('active');
    this.$megaMenu.removeClass('expanded');
    this.$megaMenuContents.removeClass('active');
    this.$megaMenuOverlay.removeClass('shown');
    setTimeout(() => {
      this.$megaMenuOverlay.hide(0);
    }, 250);
  }

  showChildMenu(target) {
    let $target = $(target);
    let $parent = $target.parent();
    let $highlights = $parent.find('.mega-menu__highlights');

    $highlights.addClass('hidden');
    $parent.find('.mega-menu__drawer').removeClass('active');
    $target.addClass('active');
  }

  hideChildMenu($link) {
    this.$megaMenuContents.find('.mega-menu__drawer').removeClass('active');
    $link.parents('.mega-menu__right').find('.mega-menu__highlights').removeClass('hidden');
  }
}

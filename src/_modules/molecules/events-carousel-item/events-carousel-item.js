'use strict';

export default class EventsCarouselItem {
  constructor($selector, selfInit = true) {
    this.name = 'events-carousel-item';
    console.log('events-carousel-item module');

    if(selfInit) this.init($selector);
  }

  init($selector) {

  }
}

'use strict';

import $ from 'jquery';
import CustomSelect from '../../custom-select/custom-select';

export default class FiltersGroup {
  constructor($selector) {
    let $selections = $('.custom-select', $selector);
    let $header = $('.filters-group__header', $selector);
    let $footer = $('.filters-group__footer', $selector);
    let $expandBtn = $('.btn-pill', $header);
    let $submitBtn = $('.btn-primary', $footer);
    let $filters = $('.filters-group__filters', $selector);
    let $parent = $selector.parent();
    let $parentSibling = $parent.next();

    this.expanded = false;

    this.$filtersGroup = $selector;
    this.$expandBtn = $expandBtn;
    this.$filters = $filters;

    $expandBtn.on('click', e => {
      e.preventDefault();
      if (this.expanded) {
        this.hideFilters();
        $parent.css({
          height: 'auto'
        })
      } else {
        this.showFilters();
      }
    });

    // fake submit button to collapse the form in mobile
    $submitBtn.on('click', e => {
      $('html, body').animate({
        scrollTop: $selector.parent().offset().top - $header.outerHeight()
      });

      if($('.filters-group__search .btn-submit').length) {
        $('.filters-group__search .btn-submit').trigger('click');
      }
      else {
        this.hideFilters();
      }
    });

    $selections.map((i, ele) => {
      let $this = $(ele);
      let selection = new CustomSelect($this);
    });

    let parentTop = $parent.offset().top;
    let headerHeight = $header.outerHeight();
    /* $(window).on('scroll.stickyFilter', e => {
      if ($parent.height() < $parentSibling.height()) {
        if ($(window).scrollTop() >= parentTop - 10) {
          if ($(window).scrollTop() + $(window).outerHeight() >= $('.site-footer').offset().top) {
            $selector.addClass('stick-to-bottom').removeClass('sticky');
            $parent.removeClass('filter-sticky');
            $parent.css({
              height: 'auto'
            });
            $filters.css({
              paddingBottom: null
            });
          } else {
            $selector.addClass('sticky').removeClass('stick-to-bottom');
            $parent.addClass('filter-sticky');
            $parent.css({
              height: $selector.outerHeight()
            });
            $filters.css({
              paddingBottom: headerHeight
            });
          }
        } else {
          $selector.removeClass('sticky').removeClass('stick-to-bottom');
          $parent.removeClass('filter-sticky');
          $parent.css({
            height: 'auto'
          });
          $filters.css({
            paddingBottom: null
          });
        }
      } else {
        $selector.removeClass('sticky').removeClass('stick-to-bottom');
        $parent.removeClass('filter-sticky');
        $parent.css({
          height: 'auto'
        });
        $filters.css({
          paddingBottom: null
        });
      }
    }).trigger('scroll.stickyFilter'); */
  }

  showFilters() {
    this.$filters.slideDown(() => {
      this.$filtersGroup.addClass('filters-shown');
    });
    this.$expandBtn.text("Done");
    this.expanded = true;
  }

  hideFilters() {
    this.$filtersGroup.removeClass('filters-shown');
    this.$filters.slideUp();
    this.$expandBtn.text("Show");
    this.expanded = false;
  }

  showSubGroup($item, checkAll = false) {
    $item.addClass('active');
    $item.find('.filters-group__subgroup').slideDown();

    if (checkAll) {
      this.checkAllSubGroupItems($item);
    }
  }

  checkAllSubGroupItems($item) {
    $item.find('.filters-group__subgroup').find('input:checkbox').prop('checked', true);
  }

  hideSubGroup($item) {
    $item.find('.filters-group__subgroup').slideUp(() => {
      $item.removeClass('active');
    });

    this.uncheckAllSubGroupItems($item);
  }

  uncheckAllSubGroupItems($item) {
    $item.find('.filters-group__subgroup').find('input:checkbox').prop('checked', false);
  }
}

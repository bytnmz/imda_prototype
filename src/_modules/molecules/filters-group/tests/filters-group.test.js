'use strict';

import FiltersGroup from '../filters-group';

describe('FiltersGroup View', function() {

  beforeEach(() => {
    this.filtersGroup = new FiltersGroup();
  });

  it('Should run a few assertions', () => {
    expect(this.filtersGroup).toBeDefined();
  });

});

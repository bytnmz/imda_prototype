'use strict';

import TagsList from '../tags-list';

describe('TagsList View', function() {

  beforeEach(() => {
    this.tagsList = new TagsList();
  });

  it('Should run a few assertions', () => {
    expect(this.tagsList).toBeDefined();
  });

});

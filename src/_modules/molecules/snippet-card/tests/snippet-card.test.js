'use strict';

import SnippetCard from '../snippet-card';

describe('SnippetCard View', function() {

  beforeEach(() => {
    this.snippetCard = new SnippetCard();
  });

  it('Should run a few assertions', () => {
    expect(this.snippetCard).toBeDefined();
  });

});

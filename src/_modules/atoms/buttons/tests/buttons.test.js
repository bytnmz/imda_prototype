'use strict';

import Buttons from '../buttons';

describe('Buttons View', function() {

  beforeEach(() => {
    this.buttons = new Buttons();
  });

  it('Should run a few assertions', () => {
    expect(this.buttons).toBeDefined();
  });

});

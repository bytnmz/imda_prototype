'use strict';

import LoadingIcon from '../loading-icon';

describe('LoadingIcon View', function() {

  beforeEach(() => {
    this.loadingIcon = new LoadingIcon();
  });

  it('Should run a few assertions', () => {
    expect(this.loadingIcon).toBeDefined();
  });

});

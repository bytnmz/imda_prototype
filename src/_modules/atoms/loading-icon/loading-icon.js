'use strict';
import * as LottiePlayer from "@lottiefiles/lottie-player";

export default class LoadingIcon {
  constructor($selector) {
    this.$loadingIcon = $selector;
    this.$lottiePlayer = $('lottie-player', this.$loadingIcon);
  }

  playAnimation() {
    this.$lottiePlayer[0].play();
  }

  restartFrame() {
    this.$lottiePlayer[0].seek(0);
  }
}

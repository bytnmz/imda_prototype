'use strict';

import SearchField from '../search-field';

describe('SearchField View', function() {

  beforeEach(() => {
    this.searchField = new SearchField();
  });

  it('Should run a few assertions', () => {
    expect(this.searchField).toBeDefined();
  });

});

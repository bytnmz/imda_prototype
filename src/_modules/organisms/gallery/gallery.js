'use strict';

import { initCarousel } from '../../../_scripts/helpers';

export default class Gallery {
  constructor($selector, selfInit = true, config) {
    this.$selector = $selector;
    this.breakpoints = config.breakpoints;
    this.slickOptions = {
      "dots": true,
      "arrows": true,
      "customPaging": (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      }
    };

    if(selfInit) this.init();
  }

  init(config) {
    initCarousel($('[data-carousel]', this.$selector), this.slickOptions);
  }
}

'use strict';

import { initCarousel } from '../../../_scripts/helpers';
export default class AccordionSliders {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.carouselOptions = {
      initialSlide: 0,
      speed: 500,
      dots: false,
      infinite: false,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      customPaging: (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      },
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            infinite: true
          }
        }
      ]
    }

    if(selfInit) this.init();
  }

  init() {
    this.carousel = initCarousel($('[data-carousel]', this.$selector), this.carouselOptions);

    this.handleExpandCollapseOnClick();
  }

  handleExpandCollapseOnClick() {
    $('.horizontal-accordion', this.$selector).map((i, ele) => {
      const index = parseInt($(ele).data('index'));

      $('button', $(ele)).on('click', (e) => {
        e.preventDefault();

        this.carousel.goToSlide(index);
      });
    });
  }
}

'use strict';

// import Parallax from 'parallax-js'
import { initCarousel } from '../../../_scripts/helpers';

export default class StoriesCarousel {
  constructor($selector, selfInit = true, config) {
    this.$selector = $selector;
    this.carouselOptions = {
      "speed": 500,
      "slidesToShow": 1,
      "slidesToScroll": 1,
      "autoplay": false,
      "infinite": true,
      "arrows": true,
      "centerMode": true,
      "centerPadding": '0px'
    };

    if(selfInit) this.init(config);
  }

  init(config) {
    initCarousel($('[data-carousel]', this.$selector), this.carouselOptions, true, (event, slick, currentSlide) => {
      //slick init
      const cur = $(slick.$slides[slick.currentSlide]),
        next = cur.next(),
        next2 = cur.next().next(),
        prev = cur.prev(),
        prev2 = cur.prev().prev();

      prev.addClass('slick-sprev');
      next.addClass('slick-snext');
      prev2.addClass('slick-sprev2');
      next2.addClass('slick-snext2');
      cur.removeClass('slick-snext').removeClass('slick-sprev').removeClass('slick-snext2').removeClass('slick-sprev2');

      slick.$prev = prev;
      slick.$next = next;
    }, (event, slick, currentSlide, nextSlide) => {
      //slick before
      const cur = $(slick.$slides[nextSlide]);
      slick.$prev.removeClass('slick-sprev');
      slick.$next.removeClass('slick-snext');
      slick.$prev.prev().removeClass('slick-sprev2');
      slick.$next.next().removeClass('slick-snext2');

      const next = cur.next(),
        prev = cur.prev();

      prev.addClass('slick-sprev');
      next.addClass('slick-snext');
      prev.prev().addClass('slick-sprev2');
      next.next().addClass('slick-snext2');
      slick.$prev = prev;
      slick.$next = next;
      cur.removeClass('slick-next').removeClass('slick-sprev').removeClass('slick-next2').removeClass('slick-sprev2');
    });
  }

  slickInit() {

  }

  slickBefore() {

  }
}

'use strict';

import 'slick-carousel';
import { initCarousel } from '../../../_scripts/helpers';

export default class EventCardCarousel {
  constructor($selector, selfInit = true, config) {
    this.breakpoints = config.breakpoints;
    this.$selector = $selector;
    this.carouselOptions = {
      dots: true,
      infinite: true,
      adaptiveHeight: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      appendArrows: $('[data-carousel-arrows]', this.$selector),
      appendDots: $('[data-carousel-dots]', this.$selector),
      customPaging: (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      },
      responsive: [
        {
          breakpoint: this.breakpoints.tablet,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false
          }
        }
      ]
    }

    if(selfInit) this.init();
  }

  init() {
    const carousel = initCarousel($('[data-carousel]', this.$selector), this.carouselOptions);

    window.emitter.on('accordionOpened', (data) => {
      carousel.setPosition();
    });
  }
}

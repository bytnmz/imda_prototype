'use strict';

import PageHeader from '../page-header';

describe('PageHeader View', function() {

  beforeEach(() => {
    this.pageHeader = new PageHeader();
  });

  it('Should run a few assertions', () => {
    expect(this.pageHeader).toBeDefined();
  });

});

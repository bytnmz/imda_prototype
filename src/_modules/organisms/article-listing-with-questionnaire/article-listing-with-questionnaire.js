'use strict';

import CustomSelect from '../../custom-select/custom-select';
import dot from 'dot';
import { initCarousel } from '../../../_scripts/helpers';

export default class ArticleListingWithQuestionnaire {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.$template = $('#questionnaireArticlesTemplate', this.$selector).html();
    this.endpoint = this.$selector.data('endpoint');
    this.profile = { "profile" : this.$selector.data('profile') ? this.$selector.data('profile') : "" };
    this.state = {
      initialState: true,
      isShowingDefault: true,
      inTransition: false,
    }
    this.carouselOptions = {
      "rows": 6,
      "speed": 500,
      "slidesToShow": 1,
      "slidesToScroll": 1,
      "adaptiveHeight": true,
      "autoplay": false,
      "infinite": true,
      "dots": false,
      "arrows": true,
      "fade": true,
      "responsive": [
        {
          "breakpoint": 1024,
          "settings": {
            "rows": 3
          }
        }
      ]
    };

    this.resultCarousel = null;

    if(selfInit) this.init();
  }

  init() {
    this.fetchData();

    $('.custom-select', this.$selector).map((i, ele) => {
      new CustomSelect($(ele), true);
    });

    $('.btn-reset', this.$selector).on('click', (e) => {
      e.preventDefault();
      this.resetFilter();
    });

    window.emitter.on('selectChanged', (data) => {
      this.state[data.name] = {
        label: data.element.find(`option[value="${data.value}"]`).text(),
        value: data.value
      };

      if(data.name === 'step2') {
        this.filterData(this.state.step1.value, this.state.step2.value);

        const profileEvent = ($('body').hasClass('imda-profile1')) ? 'business_explore_tab1_clicks_combined' : ($('body').hasClass('imda-profile2')) ? 'students_pros_explore_tab1_clicks_combined' : 'public_seniors_explore_tab1_clicks_combined';

        // if(window.dataLayer && typeof gtag === 'function') {
        //   gtag('event', profileEvent, {
        //     'sub_profile': `${this.state.step1.label}`,
        //     'interest': `${this.state.step2.label}`
        //   });
        // }
        this.bindGtagEvent();
      }

      if(data.name === 'step1') {
        if(!this.state.inTransition) {
          this.state.inTransition = true;

          $('.questionnaire-filters__bottom--disabled', this.$selector).hide();

          if(!$(`.questionnaire-filters__bottom.active`, this.$selector).length) {
            // has no step 2 active
            if($(window).width() >= 1024) {
              $(`#${data.value}`, this.$selector).slideDown(200, () => {
                $(`#${data.value}`, this.$selector).addClass('active');
                this.state.inTransition = false;
              });
            }
            else {
              $(`#${data.value}`, this.$selector).fadeIn(200, () => {
                $(`#${data.value}`, this.$selector).addClass('active');
                this.state.inTransition = false;
              });
            }
          }
          else {
            // has one step 2 active
            if(!$(`#${data.value}`, this.$selector).hasClass('active')) {
              // if selected is not current
              // this if check is ot required because only one can be active but a fallback check
              $(`.questionnaire-filters__bottom.active`, this.$selector).fadeOut(100, () => {
                // reset step 2
                $(`.questionnaire-filters__bottom.active .custom-select`, this.$selector).trigger('resetDropdown');
                $(`.questionnaire-filters__bottom.active`, this.$selector).removeClass('active');
  
                $(`#${data.value}`, this.$selector).fadeIn(100, () => {
                  $(`#${data.value}`, this.$selector).addClass('active');
                  this.state.inTransition = false;
                });
              });
            }
            else {
              this.state.inTransition = false;
            }
          }
        }

        this.state.initialState = false;
      }
    });

    this.bindGtagEvent();

    let timer;

    $(window).on('resize.resultsCarousel', () => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        if(this.resultCarousel) {
          this.resultCarousel.unslick();
          this.resultCarousel.slickInit();
        }
      }, 500);
    });
  }

  fetchData() {
    $.ajax({
      url: this.endpoint,
      method: 'post',
      data: JSON.stringify(this.profile),
      contentType: 'application/json',
      success: (res) => {
        this.state.data = {...res};
      },
      error: (err) => {
        console.log(err);
      }
    });
  }

  filterData(step1, step2) {
    if (this.state.data[step1][step2]) {
      this.renderList(this.state.data[step1][step2], this.state.data[step1][step2]['image']);
    }
  }

  renderList(data, imageSrc) {
    const _renderedHTML = dot.template(this.$template)(data);
    const imageUrl = imageSrc;

    if(this.state.isShowingDefault) {
      this.state.isShowingDefault = false;
      //If currently showing default listing, hide default article listing and show filtered content
      $('.articles-with-questionnaire__listing-default', this.$selector).hide();
      $('.articles-with-questionnaire__listing-content', this.$selector).show();
    }

    if(this.resultCarousel) {
      this.destroyResultsCarousel();
    }

    $('.articles-with-questionnaire__label', this.$selector).addClass('active');
    $('.articles-with-questionnaire__listing-content', this.$selector).addClass('reloaded').html(_renderedHTML);
    
    this.initResultsCarousel();

    $('.articles-with-questionnaire__image img', this.$selector).hide();
    $('.articles-with-questionnaire__image img', this.$selector).attr('src', imageUrl);
    $('.articles-with-questionnaire__image img', this.$selector).fadeIn(300);

    this.fadein($('.articles-with-questionnaire__listing-content article, .articles-with-questionnaire__listing-content a', this.$selector));
  }

  destroyResultsCarousel() {
    this.resultCarousel.unslick();
  }

  initResultsCarousel() {
    this.resultCarousel = initCarousel($('.articles-with-questionnaire__listing-content [data-carousel]', this.$selector), this.carouselOptions);
  }

  fadein($elem) {
    let interval = 50;

    $elem.map((i,ele) => {
      setTimeout(() => {
        $(ele).addClass('fadein');
      }, interval);

      interval +=100;
    });
  }

  resetFilter() {
    //If filter state is not initial state, do reset
    this.state.isShowingDefault = true;
    if (!this.state.initialState) {
      //Reset step 1, step 2 filter
      this.resetFilterDropdown();

      //Listing display animation
      $('.articles-with-questionnaire__listing-default > *', this.$selector).removeClass('fadein');
      $('.articles-with-questionnaire__listing-default', this.$selector).addClass('reloaded');
      $('.articles-with-questionnaire__label', this.$selector).removeClass('active');

      //Hide filtered content listing and show default listing
      $('.articles-with-questionnaire__listing-content', this.$selector).hide();
      $('.articles-with-questionnaire__listing-default', this.$selector).show();

      $('.articles-with-questionnaire__image img', this.$selector).attr('src', $('.articles-with-questionnaire__image img', this.$selector).data('src'));

      this.fadein($('.articles-with-questionnaire__listing-default > *', this.$selector));
    }
  }

  resetFilterDropdown() {
    //Reset filter
    this.state.initialState = true;
    if($(window).width() >= 1024) {
      $('.questionnaire-filters__bottom.active', this.$selector).slideUp(300);
    }
    else {
      $('.questionnaire-filters__bottom.active', this.$selector).hide();
    }

    $(`.questionnaire-filters__bottom.active`, this.$selector).removeClass('active');
    $('.questionnaire-filters__bottom--disabled', this.$selector).show();

    //$('.custom-select li', this.$selector)
    $('.custom-select', this.$selector).map((i, ele) => {
      $(ele).trigger('resetDropdown');
    });
  }

  bindGtagEvent() {
    $('a:not(.btn)', '.articles-with-questionnaire__listing').on('click', (e) => {
      e.preventDefault();
      let $target = $(e.target);
      let step1Value = $(".questionnaire-filters__top .custom-trigger .label", this.$selector).text();
      let step2Value = $(".questionnaire-filters__bottom.active .custom-trigger .label", this.$selector).text();
      let step3Value = $target.parent().find('h3 span.title').text();
      let dataUrl = $target.closest('a').data('url');
      let targetUrl = $target.closest('a').data('target');

      if(step1Value === "Select your profile") {
        step1Value = "";
      }
      if(step2Value === "Select your interest") {
        step2Value = "";
      }
      if(step3Value === "") {
        step3Value = $target.closest('span.title').text();
      }

      window.dataLayer = window.dataLayer || [];
      dataLayer.push({
        'event': 'historyChange-v2',
        'step1': step1Value,
        'step2': step2Value,
        'step3': step3Value,
        'selection': `[${step1Value}|${step2Value}|${step3Value}]`
      });
       
      $target.parent().find('a').attr('href', dataUrl);
      if(targetUrl == '_blank') { 
        window.open(dataUrl, targetUrl);
      } else {
        window.location.replace(dataUrl);
      }
    });
  }
}
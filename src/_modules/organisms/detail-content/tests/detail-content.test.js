'use strict';

import DetailContent from '../detail-content';

describe('DetailContent View', function() {

  beforeEach(() => {
    this.detailContent = new DetailContent();
  });

  it('Should run a few assertions', () => {
    expect(this.detailContent).toBeDefined();
  });

});

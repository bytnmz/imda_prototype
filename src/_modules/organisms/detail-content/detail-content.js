'use strict';

import $ from 'jquery';
import VideoPlayer from '../../molecules/video-player/video-player';

export default class DetailContent {
  constructor() {
    const $videoPlayers = $('.detail-content .video-player');

    if ($videoPlayers.length) {
      // setup YTD Video Player
      const iframeAPI = "https://www.youtube.com/iframe_api";

      if (!$('script[src="' + iframeAPI + '"]').length) {
        const firstScriptTag = document.getElementsByTagName('script')[0];
        const tag = document.createElement('script');
        tag.src = iframeAPI;
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      }

      const players = [];

      const videoPlayers = [];

      $videoPlayers.map((i, video) => {
        const $video = $(video);

        const videoPlayer = new VideoPlayer($video);
        videoPlayers.push(videoPlayer);

        const temp = {
          id: $video.find('.video-player__holder').attr('id'),
          videoId: $video.data('videoId'),
          YTPlayer: null
        };

        players.push(temp);

        const $playButton = $video.find('.btn-play');

        $playButton.on('click', e => {
          videoPlayer.play();
          players[i].YTPlayer.playVideo();
        });
      });

      window.onYouTubeIframeAPIReady = () => {
        players.map((player, i) => {
          player['YTPlayer'] = new YT.Player(player.id, {
            videoId: player.videoId,
            events: {
              'onReady': (e) => {
                videoPlayers[i].onReady($videoPlayers.eq(i));
              }
            }
          });
        });
      };
    }
  }
}

'use strict';

import $ from 'jquery';

export default class DetailAside {
  constructor() {
    let $detailAside = $('.detail-aside');
    let $socialShare = $('.social-share', $detailAside);

    let offsetTop = ($('.anchor-nav').length) ? $('.anchor-nav').outerHeight() + $('.site-header').outerHeight() : $('.site-header').outerHeight();
    
    $(window).on('scroll.detailAside', e => {
      if ($(window).scrollTop() > $detailAside.offset().top - offsetTop) {
        if ($(window).scrollTop() + ($socialShare.outerHeight() + 36 + $('.site-header').outerHeight()) >= $('.detail-wrapper').offset().top + $('.detail-wrapper').outerHeight() - 32) {
          $socialShare.addClass('stick-to-bottom').removeClass('sticky');
          $socialShare.css({
            marginTop: ''
          })
        } else {
          $socialShare.removeClass('stick-to-bottom').addClass('sticky');
          $socialShare.css({
            marginTop: offsetTop + 36 // 36px from padding of detail aside
          })
        }
      } else {
        $socialShare.removeClass('sticky').removeClass('stick-to-bottom');
        $socialShare.css({
          marginTop: ''
        });
      }
    }).trigger('scroll.detailAside');
  }
}

'use strict';

import DetailAside from '../detail-aside';

describe('DetailAside View', function() {

  beforeEach(() => {
    this.detailAside = new DetailAside();
  });

  it('Should run a few assertions', () => {
    expect(this.detailAside).toBeDefined();
  });

});

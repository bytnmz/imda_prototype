"use strict";

import $ from "jquery";
import dot from "dot";
import LoadingIcon from "../../atoms/loading-icon/loading-icon";
import Pagination from "../../molecules/pagination/pagination";
import Cookies from "js-cookie";

export default class SearchResults {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.data = {
      keyword: "",
      support: "",
      page: 1,
      itemsPerPage: $(window).width() >= 1024 ? 9 : 6,
    };
    this.$pagination = $(".pagination", this.$selector);
    this.pagination = new Pagination(this.$pagination);

    this.$content = $(".search-results__content", this.$selector);
    this.$list = $(".search-results__list", this.$selector);
    this.$template = $("#searchResultsTemplate").html();
    this.$suggestionsTemplate = $("#searchSuggestionsTemplate").html();
    this.endpoint = this.$selector.data("endpoint");
    this.suggestionsEndpoint = this.$selector.data("suggestions-endpoint");
    this.$loadingIcon = new LoadingIcon($(".loading-icon", this.$selector));

    if (selfInit) this.init();
  }

  init() {
    this.getUrlQueries();

    /* Define fields */
    this.$search = $("#keyword", this.$selector);
    this.$support = $('input[name="support"]', this.$selector);

    /* get all parameters value */
    if (this.data["keyword"]) {
      this.$search.val(decodeURIComponent(this.data["keyword"]));
    }

    if (this.data.support == "all" || this.data.support == "") {
      $('input[name="support"][value="all"]', this.$selector).prop(
        "checked",
        true
      );
      this.data.support = "all";
    } else {
      const values = this.data.support.split("|");

      this.$support.map((i, ele) => {
        if (values.indexOf($(ele).val()) !== -1) {
          $(ele).prop("checked", true);
        } else {
          $(ele).prop("checked", false);
        }
      });
    }

    /* Populate Data */
    this._updateData = () => {
      this.$content.addClass("content-updating");
      this.$loadingIcon.playAnimation();
      // this._pushDataLayer();

      const scrollPos = $(window).scrollTop();

      if (scrollPos >= this.$content.offset().top) {
        $("html, body").animate({
          scrollTop: this.$content.offset().top - 140,
        });
      }

      const callback = (res) => {
        this._renderTemplate(res);
        if (res.items.length == 0) {
          this.$pagination.hide();
        } else {
          this.$pagination.show();
          this._renderPagination(res.totalpages);
        }
      };

      this.updateData({
        endpoint: this.endpoint,
        data: this.data,
        callback: callback,
      });
    };

    window.onpopstate = (e) => {
      this.getUrlQueries();

      if (this.data["keyword"]) {
        this.$search.val(decodeURIComponent(this.data["keyword"]));
      }
      
      this.$support.prop("checked", false);

      if (this.data.support == "all" || this.data.support == "") {
        $('input[name="support"][value="all"]', this.$selector).prop(
          "checked",
          true
        );
        this.data.support = "all";
      } else {
        const values = this.data.support.split("|");
  
        this.$support.map((i, ele) => {
          if (values.indexOf($(ele).val()) !== -1) {
            $(ele).prop("checked", true);
          } else {
            $(ele).prop("checked", false);
          }
        });
      }

      this._updateData();
    };

    /* Add listener */
    let keyupTimeout;

    const suggestionsCallback = (res) => {
      this.renderSuggestions(res);
    };

    this.$search.on("keyup", (e) => {
      e.preventDefault();
      clearTimeout(keyupTimeout);

      if (e.keyCode == 13 || e.which == 13) {
        this.handleSearchSubmit();
      }
      else {
        if (this.$search.val().length > 0) {
          this.$search.parent().find(".error-msg").remove();
          $(".search-filters__searchbar", this.$selector).removeClass(
            "has-error"
          );

          // fetch suggestions
          setTimeout(() => {
            this.updateData({
              endpoint: this.suggestionsEndpoint,
              data: { keyword: this.$search.val(), profile: Cookies.get('imda_profile') ? Cookies.get('imda_profile') : 'profile0'},
              callback: suggestionsCallback,
            });
          }, 250);
        } else {
          $(".search-filters", this.$selector).removeClass("active");
        }
      }
    });

    this.$selector.find(".btn-search").on("click", (e) => {
      e.preventDefault();
      this.handleSearchSubmit();
    });

    $(document).on('click', (e) => {
      const $eTarget = $(e.target);

      if (!$eTarget.hasClass('search-filters') && !$eTarget.parents('.search-filters').length) {
        $(".search-filters", this.$selector).removeClass("active");
      }
    });

    $(".btn-back, .btn-cancel", this.$selector).on("click", (e) => {
      e.preventDefault();

      this.$search.val("");
      $(".search-filters", this.$selector).removeClass("active");
    });

    $(".search-filters__filters button", this.$selector).on("click", (e) => {
      e.preventDefault();

      if (!$(".search-filters__filters", this.$selector).hasClass("active")) {
        $(".search-filters__filters", this.$selector).addClass("active");
        $(".search-filters__filters__options", this.$selector).slideDown(300);
      } else {
        $(".search-filters__filters", this.$selector).removeClass("active");
        $(".search-filters__filters__options", this.$selector).slideUp(300);
      }
    });

    this.$support.map((i, ele) => {
      $(ele).on("change", (e) => {
        if ($(ele).val() == "all") {
          this.$support.prop("checked", false);
          $(ele).prop("checked", true);
        } else {
          if ($('input[name="support"]:checked', this.$selector).length == 0) {
            $('input[name="support"][value="all"]', this.$selector).prop(
              "checked",
              true
            );
          } else {
            $('input[name="support"][value="all"]', this.$selector).prop(
              "checked",
              false
            );
          }
        }

        const values = [];
        this.$support.toArray().map((ele) => {
          const $this = $(ele);

          if ($this.is(":checked")) {
            values.push($this.val());
          }
        });

        this.data.support = values.join("|");
        this.data.page = 1;

        this._updateData();
        this.updateURL();
      });
    });

    /* Make call upon page load */
    this._updateData();
    this.updateURL(true);
  }

  getUrlQueries() {
    const query = Object.fromEntries(
      new URLSearchParams(window.location.search)
    );

    for (const key in query) {
      this.data[key] = query[key];
    }
  }

  handleSearchSubmit() {
    const $errorMsg = this.$search.parent().find(".error-msg");
    if (this.$search.prop("required") && !this._searchValidated()) {
      if (!$errorMsg.length) {
        this.$search.after(
          '<p class="error-msg">Please enter keyword to search.</p>'
        );
        $(".search-filters__searchbar", this.$selector).addClass("has-error");
      }
    } else {
      this.$search.parent().find(".error-msg").remove();
      $(".search-filters__searchbar", this.$selector).removeClass("has-error");
      this.data.keyword = this.$search.val();
      this.data.page = 1;
      this._updateData();
      this.updateURL();
    }

    $(".search-filters", this.$selector).removeClass("active");
  }

  _searchValidated() {
    /* check if it's empty field */
    if (this.$search.val().trim(" ").length == 0) {
      return false;
    }
    return true;
  }

  _renderTemplate(data) {
    let dotTemplate = dot.template(this.$template)(data.items);

    this.$list.html(dotTemplate);

    this.updateSummary(data.totalItems);

    setTimeout(() => {
      this.$content.addClass("content-loaded");
      this.$content.removeClass("content-updating");
      this.$loadingIcon.restartFrame();
    }, 250);
  }

  _renderPagination(totalpages) {
    let callback = () => {
      this.$pagination.find("a").map((i, ele) => {
        let $this = $(ele);

        $this.on("click", (e) => {
          e.preventDefault();
          this.data.page = $this.data("page");

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }

  renderSuggestions(data) {
    const dotTemplate = dot.template(this.$suggestionsTemplate)(data);

    $('.search-filters__suggestions', this.$selector).html(dotTemplate);
    $(".search-filters", this.$selector).addClass("active");
  }

  updateSummary(totalItems) {
    if(parseInt(totalItems) === 0) {
      $('.search-results__summary .page').text(`0 - 0`);
    }

    if(parseInt(totalItems) !== 0) {
      const start = ((this.data.page - 1) * this.data.itemsPerPage) + 1;
      const end = (this.data.page) * this.data.itemsPerPage > totalItems ? totalItems : (this.data.page) * this.data.itemsPerPage;
      
      $('.search-results__summary .page').text(`${start} - ${end}`);
    }
    
    $('.search-results__summary .total').text(totalItems);
  }

  updateData({ endpoint, data, callback }) {
    $.ajax({
      url: endpoint,
      // data: data,
      data: JSON.stringify(data),
      method: "post",
      contentType: "application/json",
      success: (res) => {
        callback(res);
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  updateURL(replaceState = false) {
    const searchParams = new URLSearchParams(this.data);

    if (replaceState) {
      window.history.replaceState(this.data, "", `?${searchParams.toString()}`);
    } else {
      window.history.pushState(this.data, "", `?${searchParams.toString()}`);
    }
  }

  _pushDataLayer() {
    let filterValues = `[${
      this.parameters.keyword ? this.parameters.keyword + "|" : ""
    }${
      this.parameters.category === "all" || this.parameters.category === "All"
        ? "supportType all|"
        : this.parameters.category
        ? this.parameters.category
        : ""
    }]`;
    let dataLayerModel = {
      event: "historyChange-v2",
      keyword: `[${this.parameters.keyword ? this.parameters.keyword : ""}]`,
      supportType: `[${
        this.parameters.category ? this.parameters.category : ""
      }]`,
      filters: filterValues ? filterValues : "",
    };
    window.dataLayer = window.dataLayer || [];
    dataLayer.push(dataLayerModel);
  }
}

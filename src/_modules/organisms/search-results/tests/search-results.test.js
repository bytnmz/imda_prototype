'use strict';

import SearchResults from '../search-results';

describe('SearchResults View', function() {

  beforeEach(() => {
    this.searchResults = new SearchResults();
  });

  it('Should run a few assertions', () => {
    expect(this.searchResults).toBeDefined();
  });

});

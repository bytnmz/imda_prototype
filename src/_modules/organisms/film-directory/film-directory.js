'use strict';

import $ from 'jquery';
import doT from 'dot';
import Pagination from '../../molecules/pagination/pagination';

export default class FilmDirectory {
  constructor($selector) {
    let pagination = new Pagination();

    let _self = this,
        $filmDirectory = $selector,
        $container = $('.film-listing', $filmDirectory),
        url = $container.data('api'),
        $listingContainer = $container.children('.row'),
        itemperpage = '12',
        page = "1",
        search = '',
        year = '',
        language = '',
        genre = '',
        feature = '';

    if ($(window).width() < 450) {
      itemperpage = '6';
    };

    window.itemperpage = itemperpage;

    let data = {
          itemperpage,
          page,
          search,
          year,
          language,
          genre,
          feature
        },
        query = this.getQueryString(window.location.search.substring(1).split('&'));

    $.each(query, function(key, value){
      data[key] = value;
    });

    $('#search1').on('keypress', function(e){
      // data['search'] = $(this).val();
      if (e.which == 13) {
        e.preventDefault();
        // _self.updateQueryString(data);
        // _self.updateFilmDirectory($listingContainer, url, data);
        $('.keyword-search').trigger('click');
      }
    });


    $('.keyword-search').on('click', function() {
      data['search'] = $('#search1').val();
      data.page = "1";
      _self.updateQueryString(data);

      _self.updateFilmDirectory($listingContainer, url, data);
    });

    $('#filter-btn').on('click', function(e) {
      e.preventDefault();
      $('.filter-select').each(function(i, item){
        let itemName = $(item).attr('name');
        let itemValue = $(item).val();
        data[itemName] = itemValue;
      });
      data.page = "1";
      _self.updateQueryString(data);
      _self.updateFilmDirectory($listingContainer, url, data);
    });


    $('.js-next-page').on('click', function(){
      let activePage = parseInt(data["page"]);
      data["page"] = activePage + 1;
      _self.updateQueryString(data);
      _self.updateFilmDirectory($listingContainer, url, data);
    });

    $('.js-prev-page').on('click', function(){
      let activePage = parseInt(data["page"]);
      data["page"] = activePage - 1;
      _self.updateQueryString(data);
      _self.updateFilmDirectory($listingContainer, url, data);
    });

    let interval;
    $(window).on('resize', function(){
      clearTimeout(interval);

      interval = setTimeout(() => {
        let oldItem = data['itemperpage'];
        if ($(window).width() < 450) {
          itemperpage = 6;
        } else {
          itemperpage = 12;
        }
  
        if (oldItem != itemperpage) {
          data['itemperpage'] = itemperpage;
          window.itemperpage = itemperpage;
          _self.updateQueryString(data);
          _self.updateFilmDirectory($listingContainer, url, data);
        }
      }, 300);
    });

    _self.updateQueryString(data);
    _self.updateFilmDirectory($listingContainer, url, data);
  }

  updatePaginationEvents(data) {
    let _self = this,
      $filmDirectory = $('.film-directory'),
      $container = $('.film-listing', $filmDirectory),
      url = $container.data('api'),
      $listingContainer = $container.children('.row');

    $('.pagination__link', $filmDirectory).on('click', function(e) {
      e.preventDefault();

      console.log('click');

      let activePage = $(this).data('page').toString(),
        totalPage = Math.ceil(window.totalItem / window.itemperpage);

      if (activePage) {
        data["page"] = activePage;
        _self.updateQueryString(data);
        _self.updateFilmDirectory($listingContainer, url, data);
      }

      // if ($(this).parent().hasClass('js-prev-page')) {
      //   let activePage = parseInt(data["page"]);
      //   data["page"] = activePage - 1;
      //   _self.updateQueryString(data);
      //   _self.updateFilmDirectory($listingContainer, url, data);
      // } else if($(this).parent().hasClass('js-next-page') ) {
      //   let activePage = parseInt(data["page"]);
      //   data["page"] = activePage + 1;
      //   _self.updateQueryString(data);
      //   _self.updateFilmDirectory($listingContainer, url, data);
      // } else {
      //   let activePage = $(this).data('page').toString(),
      //   totalPage = Math.ceil(window.totalItem / window.itemperpage);
      //   if (activePage) {
      //     data["page"] = activePage;
      //     _self.updateQueryString(data);
      //     _self.updateFilmDirectory($listingContainer, url, data);
      //   }
      // }
    });
  }

  updateQueryString(data) {
    let query = {};
    query['search'] = data['search'];

    let queryString = $.param(query),
        newUrl = window.location.pathname + '?' + queryString;

    window.history.pushState(data, null, newUrl);
  }

  updateFilmDirectory($listingContainer, url, data) {
    let pagination = new Pagination();
    $.ajax({
      url,
      data,
      dataType: 'json',
      error: (jqXHR, textStatus) => {
        console.log(jqXHR, textStatus);
        console.error('Error: Cannot load directory API content');
      },
      success: (successData) => {

        let actualData = JSON.parse(JSON.stringify(successData)),
          listing = doT.template($('#filmListing').html());

        window.totalItem = successData['totalitem'];

        $listingContainer.empty().append(listing(actualData));

        // window.emitter.emit('film-listing-update');
        $('.has-match-height').matchHeight();
        let totalPage = Math.ceil(successData.totalitem / successData.itemsperpage) ;

        pagination.init(totalPage, successData.page);
        this.updatePaginationEvents(data);
      }
    });
  }

  getQueryString(paramsList) {
    if(paramsList == "") {
      return {};
    }

    const queryObject = {};

    for (let i = 0; i < paramsList.length; i++) {
      const parameter = paramsList[i].split('=', 2);
      if (parameter.length == 1){
        queryObject[parameter[0]] = ""
      }
      else {
        queryObject[parameter[0]] = decodeURIComponent(parameter[1].replace(/\+/g, " "));
      }
    }

    return queryObject;
  }
}

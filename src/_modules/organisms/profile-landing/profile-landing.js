'use strict';

import Cookies from 'js-cookie';

export default class ProfileLanding {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.state = {
      guideModalOpen: false,
      scrollBound: false,
      touchStartX: null,
      touchStartY: null,
      touchEndX: null,
      touchEndY: null
    }

    if(selfInit) this.init();
  }

  init() {
    $('html').addClass('mobile-maxheight');
    $('.profile-selector__info', this.$selector).on('click', (e) => {
      e.preventDefault();

      this.guideModalId = $('.profile-selector__info', this.$selector).attr('href');

      if(!this.state.guideModalOpen) {
        this.openModal(this.guideModalId);
      }
    });

    $('.profile-selector__guide__overlay', this.$selector).on('click', (e) => {
      e.preventDefault();

      if(this.state.guideModalOpen) {
        this.closeModal(this.guideModalId);
      }
    });

    $('.profile-selector__guide', this.$selector).on('click', (e) => {
      e.stopPropagation();
    });

    if($(window).width() >= 1024 && !this.state.scrollBound) {
      this.bindScroll();
    }

    $(window).on('resize', () => {
      if($(window).width() >= 1024) {
        if(this.state.guideModalOpen) {
          this.closeModal(this.guideModalId);
        }

        if(!this.state.scrollBound) {
          this.bindScroll();
        }
      }
      else {
        if(this.state.scrollBound) {
          this.unbindScroll();
        }
      } 
    });

    $('.profile-selector__guide footer button', this.$selector).on('click', (e) => {
      e.preventDefault();
      e.stopPropagation();

      if(this.state.guideModalOpen) {
        this.closeModal(this.guideModalId);
      }
    });

    $(document).on('keyup', (e) => {
      if (e.keyCode == 27 || e.which == 27) {
        if(this.state.guideModalOpen) {
          this.closeModal(this.guideModalId);
        }
      }
    });

    if($('body').hasClass('no-hover')) {
      if($(window).width() >= 1024) {
        this.bindTouchTaps();
      }

      $(window).on('resize orientationchange', () => {
        if($(window).width() >= 1024) {
          this.bindTouchTaps();
        }
        else {
          this.unbindTouchTaps();
        }
      });
    }
    else {
      this.bindClickAndHover();
    }
  }

  bindScroll() {
    $(window).on('mousewheel', (e) => {
      if (e.originalEvent.wheelDelta < 0) {
        $('.profile-item', this.$selector).first().find('a:not(.btn)')[0].click();
      }
    });

    $(window).on('touchstart', (e) => {
      this.state.touchStartY = e.changedTouches[0].screenY;
    });

    $(window).on('touchend', (e) => {
      this.state.touchEndY = e.changedTouches[0].screenY;
      
      if(this.state.touchStartY > this.state.touchEndY + 30) {
        $('.profile-item', this.$selector).first().find('a.btn')[0].click();
      }
    });

    this.state.scrollBound = true;
  }
  
  unbindScroll() {
    $(window).off('mousewheel touchstart touchend');
    this.state.scrollBound = false;
  }

  bindClickAndHover() {
    $('.profile-item', this.$selector).map((i, ele) => {
      const target = $('a:not(.btn)', $(ele)).data('info');
      let interval;

      $(ele).on('mouseenter', () => {
        clearInterval(interval);

        if(!$(`${target}`).hasClass('active')) {
          $(`${target}`).addClass('active');
          $(ele).addClass('active');
        }
      });
      
      $(ele).on('mouseleave', () => {
        clearInterval(interval);

        interval = setTimeout(() => {
          if($(`${target}`).hasClass('active')) {
            $(`${target}`).removeClass('active');
            $(ele).removeClass('active');
          }
        }, 300);
      });
    });
  }

  unbindClickAndHover() {
    $('.profile-item', this.$selector).map((i, ele) => {
      $(ele).off('mouseenter mouseleave');

      $('a', $(ele)).off('click');
    });
  }

  bindTouchTaps() {
    $('.profile-item', this.$selector).map((i, ele) => {
      const target = $('a:not(.btn)', $(ele)).data('info');

      $(ele).on('click', () => {
        if(!$('.profile-selector__intro', this.$selector).hasClass('default-hidden')) {
          $('.profile-selector__intro', this.$selector).addClass('default-hidden');
        }

        if(!$(`${target}`).hasClass('active')) {
          $('.profile-item.active', this.$selector).removeClass('active');
          $('.profile-selector__profiles .active', this.$selector).removeClass('active');
          $(`${target}`).addClass('active');
          $(ele).addClass('active');
        }
      });

      $('a:not(.btn)', $(ele)).on('click', (e) => {
        if (!$(e.target).parents('.profile-item').hasClass('active')) {
          e.preventDefault();
        }
      });
    });
  }

  unbindTouchTaps() {
    $('.profile-item', this.$selector).map((i, ele) => {
      $(ele).off('click');

      $('a', $(ele)).off('click');
    });
  }

  openModal(target) {
    this.state.guideModalOpen = true;
    $(target).attr('aria-hidden', false);
    $(target).fadeIn(300);
    $(`${target} footer button`, this.$selector).focus();
  }

  closeModal(target) {
    this.state.guideModalOpen = false;
    $(target).attr('aria-hidden', true);
    $(target).fadeOut(300);
    $('.profile-selector__info', this.$selector).focus();
  }
}

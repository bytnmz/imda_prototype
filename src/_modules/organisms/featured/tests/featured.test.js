'use strict';

import Featured from '../featured';

describe('Featured View', function() {

  beforeEach(() => {
    this.featured = new Featured();
  });

  it('Should run a few assertions', () => {
    expect(this.featured).toBeDefined();
  });

});

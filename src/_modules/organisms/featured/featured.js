'use strict';

import $ from 'jquery';
import 'jquery-match-height';

export default class Featured {
  constructor() {
    let $featured = $('.featured');
    let $cards = $('.featured-card', $featured);

    $cards.matchHeight();
  }
}

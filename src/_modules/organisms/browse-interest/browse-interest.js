'use strict';

import CustomSelect from '../../custom-select/custom-select';
import Cookies from 'js-cookie';

export default class BrowseInterest {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.url = this.$selector.data('listingurl');
    this.data = {
      type: 'all',
      year: 'all'
    };

    if(selfInit) this.init();
  }

  init() {
    $('.custom-select', this.$selector).map((i, ele) => {
      const selection = new CustomSelect($(ele), true);

      this.setUpData($(ele));
    });

    this.updateHref();

    window.emitter.on('selectChanged', (data) => {
      this.data[data.name] = data.value;

      if(data.element.parents('.browse-interest__step').hasClass('browse-interest__step--step1')) {
        // step 1 handler
        // sets second parameter to the current value in the selected step 2 options on change of step 1
        // also hide and show correct step 2
        const $targetSelect = $(`#${data.profile}`, this.$selector).find('select');
        this.data[$targetSelect.attr('name')] = $targetSelect.val();

        $('.browse-interest__step--step2.active', this.$selector).removeClass('active');
        $(`#${data.profile}`, this.$selector).addClass('active');
      }

      this.updateHref();
    });

    if (Cookies.get('imda_profile')) {
      this.updateProfileFromCookies();
    }
  }

  setUpData($select) {
    if(typeof this.data[$select.find('select').attr('name')] === 'undefined'){
      this.data[$select.find('select').attr('name')] = $select.find('select').val();
    }
  }

  updateProfileFromCookies() {
    // class handler for internal pages
    const profile = Cookies.get('imda_profile');

    const $supportFilter = $('select#support', this.$selector);

    const matchedProfileOption = $('option[data-profile="' + profile + '"]', $supportFilter);
    
    if (matchedProfileOption) {
      $supportFilter.val(matchedProfileOption.val());
      $supportFilter.trigger('change.customSelect');
    }
  }

  updateHref() {
    const searchParams = new URLSearchParams(this.data);

    $('.browse-interest__body .btn', this.$selector).attr('href', `${this.url}?${searchParams.toString()}`);
  }
}

'use strict';

import $ from 'jquery';
import 'slick-carousel';
import 'jquery-match-height';

export default class BannerCarousel {
  constructor(config) {
    let { breakpoints } = config;

    let $bannerCarousel = $('.banner-carousel');

    this.$bannerCarousel = $bannerCarousel;

    $bannerCarousel.on('init', slick => {
      this.updateArrowLabel(1);

      //$bannerCarousel.find('.banner-carousel__item').matchHeight();
    });

    $bannerCarousel.slick({
      dots: true,
      infinite: false,
      prevArrow: '<button class="slick-arrow slick-prev"><span class="icon icon-angle-left"></span><span class="label"></span></button>',
      nextArrow: '<button class="slick-arrow slick-next"><span class="label"></span><span class="icon icon-angle-right"></span></button>',
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: breakpoints.desktop,
          settings: {
            arrows: false,
            adaptiveHeight: true
          }
        }
      ]
    });

    $bannerCarousel.on('beforeChange', (e, slick, currentSlide, nextSlide) => {
      this.updateArrowLabel(nextSlide + 1);
    });
  }

  updateArrowLabel(currentSlide) {
    let $prevButton = $('.slick-prev', this.$bannerCarousel);
    let $nextButton = $('.slick-next', this.$bannerCarousel);

    if (currentSlide - 1 < 10) {
      $prevButton.find('.label').text('0' + (currentSlide - 1));
    } else {
      $prevButton.find('.label').text(currentSlide - 1);
    }

    if (currentSlide + 1 < 10) {
      $nextButton.find('.label').text('0' + (currentSlide + 1));
    } else {
      $nextButton.find('.label').text(currentSlide + 1);
    }

  }
}

'use strict';

import BannerCarousel from '../banner-carousel';

describe('BannerCarousel View', function() {

  beforeEach(() => {
    this.bannerCarousel = new BannerCarousel();
  });

  it('Should run a few assertions', () => {
    expect(this.bannerCarousel).toBeDefined();
  });

});

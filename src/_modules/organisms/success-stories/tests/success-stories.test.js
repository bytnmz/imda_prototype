'use strict';

import SuccessStories from '../success-stories';

describe('SuccessStories View', function() {

  beforeEach(() => {
    this.successStories = new SuccessStories();
  });

  it('Should run a few assertions', () => {
    expect(this.successStories).toBeDefined();
  });

});

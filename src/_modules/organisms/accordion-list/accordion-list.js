'use strict';

import $ from 'jquery';

export default class AccordionList {
	constructor($selector, selfInit = true) {
		this.$selector = $selector;
		this.$items = $('.accordion-item', this.$selector);
		this.state = {
			currentExpanded: 0,
			totalAccordion: $('.accordion-item', this.$selector).length
		}

		// Self init to be true by default unless explicitly set to false through instance argument
		if (selfInit) this.init();
	}

	init(config = {}) {
		const cfg = $.extend({
			autoCollapseSiblings: false,
			scrollOffset: 0,
			scrollToPosition: false
		}, config);
	
		const { scrollOffset, autoCollapseSiblings, scrollToPosition } = cfg;
	
		this.$items.map((i, ele) => {
			// binding accordion operations
			$('.accordion-title', $(ele)).on('click', (e) => {
				e.preventDefault();
		
				if ($(ele).hasClass('expanded')) {
					this._collapseItem($(ele), () => {
            this.trackCount(false);
          });
				} else {
					if (autoCollapseSiblings) {
						/* Auto hide expanded item */
						const $activeItem = this.$selector.find('.accordion-item.expanded');
			
						if ($activeItem.length > 0) {
							this._collapseItem($activeItem);
						}
					}
			
					this._expandItem($(ele), () => {
						if (scrollToPosition) {
							$('html, body').animate({
								scrollTop: $title.offset().top - scrollOffset,
							});
						}

            this.trackCount();
					});
				}
			});
		});
    
    /* automatically expand first item only after window is fully loaded */
    $(window).on('load', () => {
			if(!$('.trip-overlay').css('display') === 'block') {
				this._expandItem(this.$selector.find('.accordion-item').first(), () => {
					this.trackCount();
				});
			}
    });

		// to close the dropdown when other dropdown is being opened
    window.emitter.on('tourEnded', () => {
			setTimeout(() => {
				this._expandItem(this.$selector.find('.accordion-item').first(), () => {
					this.trackCount();
				});
			}, 1000);
    });

    this.initExpandCollapse();
	}
	
	_expandItem($item, callback = () => {}) {
		$item.addClass('expanding');
		$('.accordion-content', $item).slideDown(() => {
			$item.addClass('expanded');
			$item.removeClass('expanding');
			callback();
		});
	}
	
	_collapseItem($item, callback = () => {}) {
		$item.removeClass('expanded');
		$('.accordion-content', $item).stop().slideUp(() => {
			callback();
		});
	}
	
	expandAll() {
		this.$items.map((i, item) => {
			this._expandItem($(item));
		});
	}
	
	collapseAll() {
		this.$items.map((i, item) => {
			this._collapseItem($(item));
		});
	}

  trackCount(increment = true) {
    if (increment) this.state.currentExpanded++;
    else this.state.currentExpanded--;

    if(this.state.currentExpanded === this.state.totalAccordion){
      this.setCollapseText(this.$selector.find('.btn-expand'));
    }
    else {
      this.setExpandText(this.$selector.find('.btn-expand'));
    }
  }

  setExpandText($button) {
    $button.removeClass('active');
    $button.find('.icon').addClass(`icon-plus`).removeClass('icon-minus');
    $button.find('.label').text(`Expand all`);
    $button.attr('title', `Expand all`);
  }
  
  setCollapseText($button) {
    $button.addClass('active');
    $button.find('.icon').addClass(`icon-minus`).removeClass('icon-plus');
    $button.find('.label').text(`Collapse all`);
    $button.attr('title', `Collapse all`);
  }

  initExpandCollapse() {
		$('.accordion-list__header--buttons .btn', this.$selector).map((i,ele) => {
			$(ele).on('click', (e) => {
        e.preventDefault();

				if(!$(ele).hasClass('active')) {
					this.setCollapseText($(ele));
          
          this.expandAll();
          this.state.currentExpanded = this.state.totalAccordion;
				}
				else {
          $(ele).removeClass('active');
          this.setExpandText($(ele));
          
          this.collapseAll();
          this.state.currentExpanded = 0;
				}
			});
		});
	}
}

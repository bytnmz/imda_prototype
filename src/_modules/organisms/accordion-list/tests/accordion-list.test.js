'use strict';

import AccordionList from '../accordion-list';

describe('AccordionList View', function() {

	beforeEach(() => {
		this.accordionList = new AccordionList();
	});

	it('Should run a few assertions', () => {
		expect(this.accordionList).toBeDefined();
	});

});

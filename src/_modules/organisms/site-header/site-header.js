'use strict';

import $ from 'jquery';
import Cookies from 'js-cookie';
import dot from 'dot';
import SiteNav from '../../molecules/site-nav/site-nav';
// import MegaMenu from '../../molecules/mega-menu/mega-menu';
import SiteSearch from '../../molecules/site-search/site-search';
import SgdsMasthead from '../sgds-masthead/sgds-masthead';
// import Notification from '../../molecules/notification/notification';
import { profileClassnames } from '../../../_scripts/globals';
import { setBodyPos, setBodyToFixed } from '../../../_scripts/helpers';

export default class SiteHeader {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.menuOpened = false;
    this.searchOpened = false;
    this.$profile = Cookies.get('imda_profile');

    if(!$('body').hasClass('is-homepage') && !$('.profile-selector').length) {
      this.navTemplate = $('#navTemplate').html();
      this.navData = JSON.parse($('#navData').html());
    }

    // this.siteNav = new SiteNav();
    // this.notification = new Notification();
    // new MegaMenu();

    if(selfInit) this.init();
  }

  init() {
    new SgdsMasthead($('.sgds-masthead', this.$selector));
    new SiteNav($('.site-nav', this.$selector));

    this.siteSearch = new SiteSearch($('.site-search', this.$selector));

    if(!$('body').hasClass('is-homepage')) {
      this.handleProfileChange();
      setTimeout(() => {
        $('.placeholder-angle-down', this.$selector).addClass('hide');
      }, 1500);
    }
    else {
      this.handleProfileRedirect();
      setTimeout(() => {
        $('.placeholder-angle-down', this.$selector).addClass('hide');
      }, 1500);
    }

    $(window).on('resize', () => {
      if($(window).width() >= 1024 && this.menuOpened) {
        this.closeMenu();
      }

      $('.site-nav__lvl1 > li').removeClass('active');
      $('.site-header').removeClass('active');
    });

    $(window).on('load scroll', () => {
      let label = $('.site-header__profile ul li a.active').text();
      
      if(this.$selector.hasClass('site-header--top') && !$('.site-search', this.$selector).hasClass('search-opened') && !$('.site-header__inner', this.$selector).hasClass('active') && $(window).scrollTop() > 50) {
        this.$selector.removeClass('site-header--top');
      }

      if(!this.$selector.hasClass('site-header--top') && !$('.site-search', this.$selector).hasClass('search-opened') && !$('.site-header__inner', this.$selector).hasClass('active') && $(window).scrollTop() <= 50) {
        this.$selector.addClass('site-header--top');
      }

      if($('.profile-indicator', this.$selector).hasClass('active')) {
        $('.profile-indicator', this.$selector).removeClass('active');
        $('.site-header__profile', this.$selector).removeClass('active');
        $('.site-header__inner', this.$selector).css({
          "opacity": ""
        });
        $('.site-header__mobile-ui .icon-angle-down', this.$selector).removeClass('hide');

        if(label === '') {
          $('.site-header__mobile-ui .profile-icon').addClass('hide');
          $('.profile-indicator', this.$selector).text('Select a profile');
          $('.site-header__profile button span:nth-child(2)', this.$selector).text('Select a profile');
        }
        else {
          $('.site-header__mobile-ui .profile-icon').removeClass('hide defaultIcon darkIcon');
        }

        this.headerToTransparent();
      }

      $('.site-nav__lvl1 > li').removeClass('active');
      $('.site-header').removeClass('active')
    });

    $('.btn-toggle-search', this.$selector).on('click', e => {
      if (this.searchOpened) {
        this.closeSearch();
      }
      else {
        this.openSearch();
      }
    });

    $('.btn-toggle-menu', this.$selector).on('click', e => {
      if (this.menuOpened) {
        this.closeMenu();
      }
      else {
        this.openMenu();
      }
    });

    $('.site-header__profile button', this.$selector).on('click', (e) => {
      e.preventDefault();
      const label = $('.site-header__profile ul li a.active').text();

      if(!$('.site-header__profile', this.$selector).hasClass('active')) {
        $('.site-header__profile', this.$selector).addClass('active');
        $('.profile-icon', this.$selector).addClass('hide');
        $('.site-header__profile button span:nth-child(2)', this.$selector).text('Select a profile');
      }
      else {
        $('.site-header__profile', this.$selector).removeClass('active');
        $('.profile-icon', this.$selector).removeClass('hide');

        if(label === '') {
          $('.site-header__profile button span:nth-child(2)', this.$selector).text('Select a profile');
        }
        else {
          $('.site-header__profile button span:nth-child(2)', this.$selector).text(label);
        }
      }   
    });

    $('.site-header__profile nav .close-btn', this.$selector).on('click', (e) => {
      e.preventDefault();
      const label = $('.site-header__profile ul li a.active').text();

      $('.site-header__profile', this.$selector).removeClass('active');
      $('.site-header__mobile-ui .icon-angle-down', this.$selector).removeClass('hide');
      $('.profile-indicator', this.$selector).text('');
      $('.profile-icon', this.$selector).removeClass('hide');
      $('.site-header__profile button span:nth-child(2)', this.$selector).text(label);
      $('.profile-indicator', this.$selector).removeClass('active');

      if (label === '') {
        $('.site-header__mobile-ui .profile-icon').addClass('hide');
        $('.site-header__profile button span:nth-child(2)', this.$selector).text('Select a profile');
        $('.profile-indicator', this.$selector).text('Select a profile');
      }
      else {
        $('.site-header__mobile-ui .profile-icon').removeClass('hide defaultIcon darkIcon');
        $('.profile-indicator', this.$selector).text('');
      }
    });

    $('.profile-indicator', this.$selector).on('click', (e) => {
      e.preventDefault();
      this.handleMobileProfile();
    });

    $(document).on('click.headerCollapse', e => {
      const $eTarget = $(e.target);
      const label = $('.site-header__profile ul li a.active').text();

      if(this.searchOpened) {
        if (!($eTarget.hasClass('site-search') || $eTarget.parents('.site-search').length || $eTarget.hasClass('btn-toggle-search') || $eTarget.parents('.btn-toggle-search').length)) {
          this.closeSearch();
        }
      }

      if (!($eTarget.hasClass('site-nav') || $eTarget.parents('.site-nav').length)) {
        $('.site-nav__lvl1 > li').removeClass('active');
        $('.site-header').removeClass('active')
      }

      // if (!($eTarget.hasClass('site-nav') || $eTarget.parents('.site-nav').length || $eTarget.hasClass('site-nav__lvl2') || $eTarget.parents('.site-nav__lvl2').length || $eTarget.hasClass('btn-toggle-menu') || $eTarget.parents('.btn-toggle-menu').length)) {
      //   this.closeMenu();
      // }

      if (!($eTarget.hasClass('site-header__profile') || $eTarget.parents('.site-header__profile').length || $eTarget.hasClass('profile-indicator'))) {
        $('.site-header__profile', this.$selector).removeClass('active');
        $('.site-header__profile button span:nth-child(2)', this.$selector).text(label);
        $('.profile-indicator', this.$selector).removeClass('active');
        $('.profile-icon', this.$selector).removeClass('hide');
        $('.profile-indicator', this.$selector).text('');
        $('.site-header__mobile-ui .icon-angle-down', this.$selector).removeClass('hide');

        if(label === '') {
          $('.site-header__mobile-ui .profile-icon').addClass('hide');
          $('.profile-indicator', this.$selector).text('Select a profile');
          $('.site-header__profile button span:nth-child(2)', this.$selector).text('Select a profile');
        }
        else {
          $('.site-header__mobile-ui .profile-icon').removeClass('hide defaultIcon darkIcon');
        }
      }
    });
  }

  headerToWhite() {
    if (this.$selector.hasClass('site-header--top')) {
      this.$selector.removeClass('site-header--top');
    }
  }

  headerToTransparent() {
    if (!this.$selector.hasClass('site-header--top') && $(window).scrollTop() <= 50) {
      this.$selector.addClass('site-header--top');
    }
  }

  openMenu() {
    this.headerToWhite();

    setBodyToFixed();

    $('.btn-toggle-menu', this.$selector).addClass('active');
    $('.site-header__inner', this.$selector).addClass('active');
    this.menuOpened = true;
  }

  closeMenu() {
    this.headerToTransparent();

    setBodyPos();

    $('.btn-toggle-menu', this.$selector).removeClass('active');
    $('.site-header__inner', this.$selector).removeClass('active');
    this.menuOpened = false;

    $('.site-nav .active').removeClass('active');

    // if (this.siteNav.lvl2Expanded) {
    //   this.siteNav.backLevelOne();
    //   setTimeout(() => {
    //   }, 250);
    // } else {
    //   $('.btn-toggle-menu', this.$selector).removeClass('active');
    //   this.menuOpened = false;
    // }
  }

  openSearch() {
    this.headerToWhite();

    this.siteSearch.open();
    $('.site-search', this.$selector).addClass('search-opened');
    $('.btn-toggle-search', this.$selector).addClass('active');
    $('.btn-toggle-search', this.$selector).find('.icon').removeClass('icon-search').addClass('icon-close');
    
    setTimeout(() => {
      $('.site-search__input', this.$selector)[0].focus();
    }, 400);

    this.searchOpened = true;
  }

  closeSearch() {
    this.headerToTransparent();

    this.siteSearch.close();
    $('.site-search', this.$selector).removeClass('search-opened');
    $('.btn-toggle-search',this.$selector).removeClass('active');
    $('.btn-toggle-search', this.$selector).find('.icon').removeClass('icon-close').addClass('icon-search');
    this.searchOpened = false;
  }

  handleProfileChange() {
    $('.site-header__profile ul li a', this.$selector).map((i,ele) => {
      $(ele).parent().on('click', (e) => {
        e.preventDefault();
        let label = $('.site-header__profile ul li a.active').text();

        if(!$(ele).hasClass('active')) {
          $('.site-header__profile ul li a.active', this.$selector).parent().removeClass('active');
          $('.site-header__profile ul li a.active', this.$selector).removeClass('active');
          $('.site-header__mobile-ui .icon-angle-down', this.$selector).removeClass('hide');
          $(ele).addClass('active');
          $(ele).parent().addClass('active');
          const profile = $(ele).attr('href').substring(1);
          let profileVal = '';
          
          switch (profile) {
            case 'profile0':
              if(!$('body').hasClass('imda-profile0')) {
                $('body').removeClass(`${profileClassnames[0]} ${profileClassnames[1]} ${profileClassnames[2]}`).addClass(profileClassnames[3]);
                Cookies.set('imda_profile', 'profile0', { expires: 365 });
                this.renderNav(0);
                this.changeBannerShapeColor(profileVal);
                profileVal = 'profile0';
                this.renderProfileButton(profileVal);
              }
              break;
              case 'profile1':
              if(!$('body').hasClass('imda-profile1')) {
                $('body').removeClass(`${profileClassnames[1]} ${profileClassnames[2]} ${profileClassnames[3]}`).addClass(profileClassnames[0]);
                Cookies.set('imda_profile', 'profile1', { expires: 365 });
                this.renderNav(0);
                profileVal = 'profile1';
                this.changeBannerShapeColor(profileVal);
                this.renderProfileButton(profileVal);
              }
              break;
              
              case 'profile2':
              if(!$('body').hasClass('imda-profile2')) {
                $('body').removeClass(`${profileClassnames[0]} ${profileClassnames[2]} ${profileClassnames[3]}`).addClass(profileClassnames[1]);
                Cookies.set('imda_profile', 'profile2', { expires: 365 });
                this.renderNav(1);
                profileVal = 'profile2';
                this.changeBannerShapeColor(profileVal);
                this.renderProfileButton(profileVal);
              }
              break;
              
              case 'profile3':
              if(!$('body').hasClass('imda-profile3')) {
                $('body').removeClass(`${profileClassnames[0]} ${profileClassnames[1]} ${profileClassnames[3]}`).addClass(profileClassnames[2]);
                Cookies.set('imda_profile', 'profile3', { expires: 365 });
                this.renderNav(2);
                profileVal = 'profile3';
                this.changeBannerShapeColor(profileVal);
                this.renderProfileButton(profileVal);
              }
              break;
          
            default:
              break;
          }

          if (profileVal) {
            let $supportFilter = $('select#support');

            let matchedProfileOption = $('option[data-profile="' + profileVal + '"]', $supportFilter);
            if (matchedProfileOption) {
              $supportFilter.val(matchedProfileOption.val());
              $supportFilter.trigger('change.customSelect');
              $supportFilter.trigger('change');
            }
          }

          window.emitter.emit('profileChanged', profileVal);

          $('.site-header__profile', this.$selector).removeClass('active');
          $('.site-header__mobile-ui .profile-indicator', this.$selector).text('');
        } 
        label = $('.site-header__profile ul li a.active').text();
        if(label === '') {
          $('.site-header__profile button span:nth-child(2)', this.$selector).text('label')
        }
        $('.site-header__profile button span:nth-child(2)', this.$selector).text(label);
        this.closePersonaNav();

        if($('.profile-indicator', this.$selector).hasClass('active')) {
          $('.profile-indicator', this.$selector).removeClass('active');
          $('.site-header__mobile-ui .profile-indicator', this.$selector).text('');
          $('.site-header__mobile-ui .profile-icon').removeClass('hide defaultIcon darkIcon');
        }
      });
    });
  }

  handleProfileRedirect() {
    $('.site-header__profile nav ul li', this.$selector).on('click', (e) => {
      e.preventDefault();
      let $target = e.target;
      let $url = $($target).closest('li').find('a').attr('href');
      if($url != "" && $url != undefined) {
        window.location.replace($url)
      } else {
        this.closePersonaNav();
      }
    });
  }

  renderNav(profileNo) {
    const _dotTemplate = dot.template(this.navTemplate)(this.navData[profileNo]);

    $('.site-nav', this.$selector).html(_dotTemplate);

    new SiteNav($('.site-nav', this.$selector));
  }

  changeBannerShapeColor(profileVal) {
    if ($('.page-banner--dark--detail').length) {
      $('.page-banner').addClass('page-banner--white');
      $('.page-banner').removeClass('page-banner--purple page-banner--maroon page-banner--teal')
      // switch (profileVal) {
      //   case 'profile1':
      //     $('.page-banner').removeClass('page-banner--maroon page-banner--teal');
      //     break;
      //   case 'profile2':
      //     if (!$('.page-banner').hasClass('page-banner--maroon')) {
      //      $('.page-banner').addClass('page-banner--maroon');
      //     }
      //     $('.page-banner').removeClass('page-banner--teal');
      //     break;
      //   case 'profile3':
      //     if (!$('.page-banner').hasClass('page-banner--teal')) {
      //       $('.page-banner').addClass('page-banner--teal');
      //     }
      //     $('.page-banner').removeClass('page-banner--maroon');
      //     break;
      //   default:
      //     break;
      // }
    }
  }

  renderProfileButton(profileVal) {
    let purpleIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="20" fill="none" viewBox="0 0 24 20">
    <path stroke="#702F8A" stroke-linecap="round" stroke-miterlimit="10" d="M22.305 9.894v8.601c0 .368-.267.658-.597.658H2.293c-.33 0-.598-.295-.598-.658V9.894"/>
    <path stroke="#702F8A" stroke-linecap="round" stroke-miterlimit="10" d="M10.985 13.028 1.531 9.894C1.217 9.786 1 9.47 1 9.102V4.717c0-.362.268-.657.597-.657H22.4c.333 0 .601.29.601.657v4.385c0 .363-.217.684-.531.792l-9.452 3.134M7.61 4.06l.852-2.407c.14-.393.484-.653.867-.653h5.34c.386 0 .73.26.868.653l.85 2.407"/>
    <path stroke="#702F8A" stroke-linecap="round" stroke-miterlimit="10" d="M11.998 11.439c.527 0 .958.475.958 1.056v1.397c0 .581-.431 1.056-.958 1.056-.527 0-.959-.475-.959-1.056v-1.397c0-.58.431-1.056.959-1.056Z"/>
    </svg>`;

    let maroonIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="28" height="21" fill="none" viewBox="0 0 28 21">
    <path stroke="#9E1B64" stroke-linecap="round" stroke-linejoin="round" d="m27.28 7.247-13.139 6.247L1 7.247 14.139 1 27.28 7.247Z"/>
    <path stroke="#9E1B64" stroke-linecap="round" stroke-linejoin="round" d="M22.798 9.548v6.657l-8.331 3.864-8.334-3.864V9.877M2.281 7.576v10.52"/>
    </svg>`;

    let tealIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="26" height="20" fill="none" viewBox="0 0 26 20">
    <path stroke="#008672" stroke-linejoin="round" d="M1.004 19.343v-2.71c-.023-1.069 0-2.536 1.336-4.612.257-.34.913-1.099 1.475-1.409a9.425 9.425 0 0 1 1.195-.563c-.855-.692-2.425-2.464-2.179-4.787.288-2.719 2.46-4.189 4.638-4.26 2.179-.07 4.252 1.162 4.849 3.239.344 1.197.632 3.098-1.054 4.929l-.773.88c.831.445 2.593 1.689 3.127 3.097.668 1.76.703 2.64.738 3.485.028.676.011 1.913 0 2.71"/>
    <path stroke="#008672" stroke-linejoin="round" d="M14.027 14.226c.035-.104.122-.835 1.424-1.984.868-.766 1.435-.855 1.575-.906-.728-.594-2.065-2.115-1.855-4.108.245-2.333 2.094-3.594 3.95-3.654 1.855-.06 3.62.996 4.129 2.778.293 1.027.538 2.659-.898 4.23l-.652.754c.709.383 2.12 1.516 2.674 2.682.611 1.288.582 2.242.611 2.967.024.58.01 1.501 0 2.186"/>
    </svg>`;

    let defaultIcon = `<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g filter="url(#filter0_d_1774_134866)">
    <path d="M22.0039 32V29.4879C21.9829 28.4983 22.0039 27.1387 23.2021 25.2138C23.4334 24.8984 24.022 24.1959 24.5265 23.9088C25.031 23.6217 25.4514 23.4414 25.5986 23.3871C24.8313 22.7455 23.4229 21.1028 23.6436 18.9499C23.9019 16.4302 25.8508 15.0679 27.8058 15.0027C29.7608 14.9374 31.6212 16.0786 32.1572 18.0038C32.4661 19.113 32.7248 20.8751 31.2113 22.5715L30.5176 23.3871C31.2638 23.8003 32.8446 24.9527 33.3239 26.2577C33.923 27.8889 33.9545 28.7049 33.9861 29.4879C34.0113 30.1143 33.9966 31.2604 33.9861 32" stroke="#262626" stroke-linejoin="round"/>
    <circle cx="28" cy="24" r="15.5" stroke="#262626"/>
    </g>
    <defs>
    <filter id="filter0_d_1774_134866" x="0" y="0" width="56" height="56" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
    <feOffset dy="4"/>
    <feGaussianBlur stdDeviation="6"/>
    <feComposite in2="hardAlpha" operator="out"/>
    <feColorMatrix type="matrix" values="0 0 0 0 0.439216 0 0 0 0 0.184314 0 0 0 0 0.541176 0 0 0 0.25 0"/>
    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1774_134866"/>
    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1774_134866" result="shape"/>
    </filter>
    </defs>
    </svg>
    `;

    switch (profileVal) {
      case 'profile1':
        $('.profile-icon').html(purpleIcon);

        if (!$('.site-header__profile button').hasClass('purpleShadow')) {
          $('.site-header__profile button').addClass('purpleShadow');
          $('.site-header__profile button').removeClass('maroonShadow turquoiseShadow');
        }

        if (!$('.site-header__profile nav').hasClass('purpleShadow')) {
          $('.site-header__profile nav').addClass('purpleShadow');
          $('.site-header__profile nav').removeClass('maroonShadow turquoiseShadow');
        }

        if(!$('.site-header__mobile-ui .profile-indicator').hasClass('purpleShadow')) {
          $('.site-header__mobile-ui .profile-indicator').addClass('purpleShadow');
          $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow turquoiseShadow');
        }

        if($('.newsletter').length && $('.newsletter picture').find('img').length) {
          let url = $('.newsletter picture').find('img').data('profile1');
          $('.newsletter picture').find('img').attr('src', url);
          $('.newsletter picture').find('source').attr('srcset', url);
        }

        if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
          $('.horizontal-accordion__image').each((index, element) => {
            let url = $(element).find('img').data('profile1');
            $(element).find('img').attr('src', url);
          });
        }

        if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
          $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
            const url = $(ele).data('profile1');
            $(ele).attr('src', url);
          });
        }

        if($('ul').length && $('ul > li').length) {
          $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
        }

        break;
      case 'profile2':
        $('.profile-icon').html(maroonIcon);
        if (!$('.site-header__profile button').hasClass('maroonShadow')) {
          $('.site-header__profile button').addClass('maroonShadow');
          $('.site-header__profile button').removeClass('purpleShadow turquoiseShadow');
        }

        if (!$('.site-header__profile nav').hasClass('maroonShadow')) {
          $('.site-header__profile nav').addClass('maroonShadow');
          $('.site-header__profile nav').removeClass('purpleShadow turquoiseShadow');
        }

        if(!$('.site-header__mobile-ui .profile-indicator').hasClass('maroonShadow')) {
          $('.site-header__mobile-ui .profile-indicator').addClass('maroonShadow');
          $('.site-header__mobile-ui .profile-indicator').removeClass('purpleShadow turquoiseShadow');
        }

        if($('.newsletter').length && $('.newsletter picture').find('img').length) {
          let url = $('.newsletter picture').find('img').data('profile2');
          $('.newsletter picture').find('img').attr('src', url);
          $('.newsletter picture').find('source').attr('srcset', url);
        }

        if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
          $('.horizontal-accordion__image').each((index, element) => {
            let url = $(element).find('img').data('profile2');
            $(element).find('img').attr('src', url);
          });
        }

        if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
          $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
            const url = $(ele).data('profile2');
            $(ele).attr('src', url);
          });
        }

        if($('ul').length && $('ul > li').length) {
          $('ul:not(.tags-list__list) > li').addClass('maroon');
          $('ul:not(.tags-list__list) > li').removeClass('turquoise');
        }

        break;
      case 'profile3':
        $('.profile-icon').html(tealIcon);
        if (!$('.site-header__profile button').hasClass('turquoiseShadow')) {
          $('.site-header__profile button').addClass('turquoiseShadow');
          $('.site-header__profile button').removeClass('maroonShadow purpleShadow');
        }
        
        if (!$('.site-header__profile nav').hasClass('turquoiseShadow')) {
          $('.site-header__profile nav').addClass('turquoiseShadow');
          $('.site-header__profile nav').removeClass('maroonShadow purpleShadow');
        }
        
        if(!$('.site-header__mobile-ui .profile-indicator').hasClass('turquoiseShadow')) {
          $('.site-header__mobile-ui .profile-indicator').addClass('turquoiseShadow');
          $('.site-header__mobile-ui .profile-indicator').removeClass('maroonShadow purpleShadow');
        }
        
        if($('.newsletter').length && $('.newsletter picture').find('img').length) {
          let url = $('.newsletter picture').find('img').data('profile3');
          $('.newsletter picture').find('img').attr('src', url);
          $('.newsletter picture').find('source').attr('srcset', url);
        }
        
        if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
          $('.horizontal-accordion__image').each((index, element) => {
            let url = $(element).find('img').data('profile3');
            $(element).find('img').attr('src', url);
          });
        }
        
        if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
          $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
            const url = $(ele).data('profile3');
            $(ele).attr('src', url);
          });
        }

        if($('ul').length && $('ul > li').length) {
          $('ul:not(.tags-list__list) > li').addClass('turquoise');
          $('ul:not(.tags-list__list) > li').removeClass('maroon');
        }
        break;
      default:
        $('.profile-icon').html(defaultIcon);
        $('.site-header__profile .profile-icon').empty();

        if($('.newsletter').length && $('.newsletter picture').find('img').length) {
          let url = $('.newsletter picture').find('img').data('profile1');
          $('.newsletter picture').find('img').attr('src', url);
          $('.newsletter picture').find('source').attr('srcset', url);
        }

        if($('.accordion-sliders').length && $('.accordion-sliders .horizontal-accordion__image').find('img').length) {
          $('.horizontal-accordion__image').each((index, element) => {
            let url = $(element).find('img').data('profile1');
            $(element).find('img').attr('src', url);
          });
        }

        if($('.accordion-item').length && $('.accordion-item .accordion-title__image').find('img').length) {
          $('.accordion-item .accordion-title__image').find('img').map((i,ele) => {
            const url = $(ele).data('profile1');
            $(ele).attr('src', url);
          });
        }

        if($('ul').length && $('ul > li').length) {
          $('ul:not(.tags-list__list) > li').removeClass('turquoise maroon');
        }
        
        break;
    }
    $('.profile-icon', this.$selector).removeClass('hide');
  }

  handleMobileProfile() {
    let label = $('.site-header__profile ul li a.active').text();
    let profile = '';
    $('.profile-indicator', this.$selector).text('');

    if(Cookies.get('imda_profile')) {
      profile = Cookies.get('imda_profile');
    }
    this.renderProfileButton(profile);

    if(!$('.site-header__profile', this.$selector).hasClass('active')) {
      $('.profile-indicator', this.$selector).addClass('active');
      $('.site-header__profile nav li').addClass('darkIcon');
      $('.site-header__profile', this.$selector).addClass('active');
      $('.site-header__mobile-ui .icon-angle-down', this.$selector).addClass('hide');
      
      if(label === ''){
        $('.site-header__mobile-ui .profile-icon').removeClass('hide').addClass('defaultIcon');
      } else {
        $('.site-header__mobile-ui .profile-icon').removeClass('hide').addClass('darkIcon');
      }
      if (this.menuOpened) {
        this.closeMenu();
      } 
      $('.site-header__inner', this.$selector).css({
        "opacity": "1"
      });
      this.headerToWhite();
    }
    else {
      $('.profile-indicator', this.$selector).removeClass('active');
      $('.profile-indicator', this.$selector).text('');
      $('.site-header__profile nav li').removeClass('darkIcon');
      $('.site-header__mobile-ui .profile-icon').removeClass('hide defaultIcon darkIcon');
      $('.site-header__mobile-ui .icon-angle-down', this.$selector).removeClass('hide');
      $('.site-header__profile', this.$selector).removeClass('active');
      $('.site-header__inner', this.$selector).css({
        "opacity": ""
      });
      
      if(label === '') {
        $('.site-header__mobile-ui .profile-icon').addClass('hide');
        $('.profile-indicator', this.$selector).text('Select a profile');
      }
      else {
        $('.site-header__mobile-ui .profile-icon').removeClass('hide defaultIcon darkIcon');
      }
      
      this.headerToTransparent();
    }
  }

  closePersonaNav(label) {
    $('.site-header__profile', this.$selector).removeClass('active');
    $('.profile-indicator', this.$selector).removeClass('active');
    $('.profile-icon', this.$selector).removeClass('hide');
    $('.profile-indicator', this.$selector).text('');
    $('.site-header__mobile-ui .icon-angle-down', this.$selector).removeClass('hide');
    $('.site-header__profile nav li').removeClass('darkIcon');
    
    if(label === '') {
      $('.site-header__mobile-ui .profile-icon').addClass('hide');
      $('.profile-indicator', this.$selector).text('Select a profile');
      $('.site-header__profile button span:nth-child(2)', this.$selector).text('Select a profile');
      $('.site-header__profile .profile-icon').empty();
    }
    else {
      $('.site-header__mobile-ui .profile-icon').removeClass('hide defaultIcon darkIcon');
    }
  }
}

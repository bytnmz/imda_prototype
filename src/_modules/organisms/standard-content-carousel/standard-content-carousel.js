'use strict';

import $ from 'jquery';
import 'slick-carousel';
import { initCarousel } from '../../../_scripts/helpers';

export default class StandardContentCarousel {
  constructor($selector, selfInit = true, config) {
    const { breakpoints } = config;
    this.$selector = $selector;

    if(selfInit) this.init(breakpoints);
  }

  init(breakpoints) {
    let carouselOptions = {
      dots: true,
      infinite: true,
      prevArrow: '<button class="btn slick-prev" type="button"><span class="icon icon-arrow-left"></span><span class="vh">Previous slide</span></button>',
      nextArrow: '<button class="btn slick-next" type="button"><span class="icon icon-arrow-right"></span><span class="vh">Next slide</span></button>',
      adaptiveHeight: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      appendArrows: $('[data-carousel-arrows]', this.$selector),
      appendDots: $('[data-carousel-dots]', this.$selector),
      customPaging: (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      },
      responsive: [
        {
          breakpoint: breakpoints.desktop,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 1023,
          settings: {
            slidesToShow: 2,
            dots: false
          }
        },
        {
          breakpoint: breakpoints.tablet,
          settings: {
            slidesToShow: 1,
            dots: false
          }
        }
      ]
    }
    initCarousel($('[data-carousel]', this.$selector), carouselOptions);
  }
}

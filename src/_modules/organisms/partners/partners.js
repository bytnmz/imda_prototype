'use strict';

import 'slick-carousel';
import { initCarousel } from '../../../_scripts/helpers';

export default class Partners {
  constructor($selector, selfInit = true, config) {
    this.$selector = $selector;
    this.breakpoints = config.breakpoints;
    this.subsidiaryOptions = {
      "rows": 1,
      "speed": 500,
      "slidesToShow": 1,
      "slidesToScroll": 1,
      "autoplay": false,
      "infinite": true,
      "arrows": true
    };

    this.relatedSiteOptions = {
      "rows": 1,
      "speed": 500,
      "slidesToShow": 6,
      "slidesToScroll": 1,
      "autoplay": false,
      "infinite": true,
      "arrows": true,
      "responsive": [
        {
          "breakpoint": 1440,
          "settings": {
            "slidesToShow": 5
          }
        },
        {
          "breakpoint": 1280,
          "settings": {
            "slidesToShow": 4
          }
        },
        {
          "breakpoint": 1024,
          "settings": {
            "slidesToShow": 1
          }
        }
      ]
    };

    if(selfInit) this.init();
  }

  init() {
    const subsidiaries = initCarousel($('.partners__group--subsidiary [data-carousel]', this.$selector), this.subsidiaryOptions, false);
    
    if($('.partners__group--subsidiary .partner-link', this.$selector).length > 1) {
      subsidiaries.slickInit();
    }

    const relatedSites = initCarousel($('.partners__group--related-sites [data-carousel]', this.$selector), this.relatedSiteOptions, false);

    if($('.partners__group--related-sites .partner-link', this.$selector).length <= 6) {
      this.checkScreenSize(relatedSites);
      
      let interval = null;
      $(window).on('resize', () => {
        clearInterval(interval);

        interval = setTimeout(() => {
          // unslick first
          if($('.partners__group--related-sites [data-carousel]', this.$selector).hasClass('slick-initialized')) {
            relatedSites.unslick();
          }

          // then decide is carousel is required
          if(!$('.partners__group--related-sites [data-carousel]', this.$selector).hasClass('slick-initialized')) {
            this.checkScreenSize(relatedSites);
          }
        }, 300);
      });
    }
    else {
      relatedSites.slickInit();
    }
  }

  checkScreenSize(carouselInstance) {
    if($(window).width() >= 1440) {
      if($('.partners__group--related-sites .partner-link', this.$selector).length > 6) {
        carouselInstance.slickInit();
      }
    }
    else {
      if($(window).width() >= 1280) {
        if($('.partners__group--related-sites .partner-link', this.$selector).length > 5) {
          carouselInstance.slickInit();
        }
      }
      else {
        if($(window).width() >= 1024) {
          if($('.partners__group--related-sites .partner-link', this.$selector).length > 4) {
            carouselInstance.slickInit();
          }
        }
        else {
          if($(window).width() > 840) {
            if($('.partners__group--related-sites .partner-link', this.$selector).length > 3) {
              carouselInstance.slickInit();
            }
          }
          else {
            if($(window).width() > 767) {
              if($('.partners__group--related-sites .partner-link', this.$selector).length > 2) {
                carouselInstance.slickInit();
              }
            }
            else {
              if($('.partners__group--related-sites .partner-link', this.$selector).length > 1) {
                carouselInstance.slickInit();
              }
            }
          }
        }
      }
    }
  }
}

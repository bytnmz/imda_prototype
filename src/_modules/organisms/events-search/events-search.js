'use strict';

import ListingFilter from '../../molecules/listing-filter/listing-filter';
import Cookies from 'js-cookie';

export default class EventsSearch {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.$filters = $('.events-listing__filters', this.$selector);
    this.url = this.$selector.data('listingurl');
    this.data = {
      type: 'all',
      page: 1,
      itemsPerPage: $(window).width() >= 1024 ? 9 : 6
    };

    if(selfInit) this.init();
  }

  init() {
    new ListingFilter(this.$filters);
    
    if (Cookies.get('imda_profile')) {
      this.updateProfileFromCookies();
    }

    $('.custom-select', this.$selector).map((i, ele) => {
      this.setUpData($(ele));
    });

    this.updateHref();
    
    window.emitter.on('selectChanged', (data) => {
      this.data[data.name] = data.value;
      this.updateHref();
    });

    let keypressTimeout;

    $('.search-field__input', this.$selector).on('keypress', e => {
      clearTimeout(keypressTimeout);

      keypressTimeout = setTimeout(() => {
        this.data[$('.search-field__input', this.$selector).attr('name')] = $('.search-field__input', this.$selector).val();
        this.updateHref();
      }, 250);
    });
  }

  setUpData($select) {
    if(typeof this.data[$select.find('select').attr('name')] === 'undefined'){
      this.data[$select.find('select').attr('name')] = $select.find('select').val();
    }
  }

  updateProfileFromCookies() {
    // class handler for internal pages
    const profile = Cookies.get('imda_profile');

    const $supportFilter = $('select#support', this.$selector);

    const matchedProfileOption = $('option[data-profile="' + profile + '"]', $supportFilter);

    if (matchedProfileOption) {
      $supportFilter.val(matchedProfileOption.val());
      $supportFilter.trigger('change.customSelect');
    }
  }

  updateHref() {
    const searchParams = new URLSearchParams(this.data);

    $('.listing-filter__footer .btn', this.$selector).attr('href', `${this.url}?${searchParams.toString()}`);
  }
}

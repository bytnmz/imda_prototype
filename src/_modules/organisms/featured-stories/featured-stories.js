'use strict';

import { initCarousel } from '../../../_scripts/helpers';

export default class FeaturedStories {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.carouselOptions = {
      dots: false,
      arrows: true,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            arrows: true
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            arrows: false
          }
        },
        {
          breakpoint: 860,
          settings: {
            slidesToShow: 2,
            arrows: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            arrows: false
          }
        }
      ]
    }

    if(selfInit) this.init();
  }

  init() {
    const carousel = initCarousel($('[data-carousel]', this.$selector), this.carouselOptions);
  }
}

'use strict';

export default class FullBleedAccordions {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    $('.accordion-item', this.$selector).map((i,ele) => {
      const $toggle = $(ele).find('.accordion-title');
      const $content = $(ele).find('.accordion-content');
      
      $toggle.on('click', (e) => {
        e.preventDefault();
        
				if (!$(ele).hasClass('expanded')) {
          $(ele).addClass('expanding');
          $content.slideDown(300, () => {
            $(ele).removeClass('expanding');
            $(ele).addClass('expanded');

            window.emitter.emit('accordionOpened');
          });
        }
        else {
          $content.slideUp(300);
          $(ele).removeClass('expanded');
        }
      });
    });
  }
}

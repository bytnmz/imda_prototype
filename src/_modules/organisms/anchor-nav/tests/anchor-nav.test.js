'use strict';

import AnchorNav from '../anchor-nav';

describe('AnchorNav View', function() {

  beforeEach(() => {
    this.anchorNav = new AnchorNav();
  });

  it('Should run a few assertions', () => {
    expect(this.anchorNav).toBeDefined();
  });

});

'use strict';

import $ from 'jquery';

export default class AnchorNav {
  constructor() {
    const $anchorNav = $('.anchor-nav');
    const $links = $('li', $anchorNav);

    const anchorNavHeight = $anchorNav.outerHeight();

    $('.detail-wrapper').css({
      paddingTop: anchorNavHeight
    });

    $links.map((i, ele) => {
      const $this = $(ele);
      const $link = $this.find('a');
      const target = $link.attr('href');

      $link.on('click', e => {
        e.preventDefault();

        $this.siblings('.active').removeClass('active');
        $this.addClass('active');

        if ($(target).length) {
          $('html, body').animate({
            scrollTop: $(target).offset().top - anchorNavHeight - $('.site-header').outerHeight() - 30
          });
        }
      });
      
      if (i === 0) {
        $this.addClass('active');
      }
      
    });
  
    const hash = window.location.hash;
    if ($(hash).length) {
      $('html, body').animate({
        scrollTop: $(hash).offset().top - anchorNavHeight - $('.site-header').outerHeight() - 30
      });
    }

    $(window).on('load', e => {
      const pageBannerPos = $('.page-banner').offset().top + $('.page-banner').outerHeight();

      // store position of each sections into array
      let sectionPos = [];
      sectionPos = $('.detail-section').map((i, ele) => {
        let $this = $(ele);

        return Math.floor($this.offset().top);
      });

      let scrollTimeout;

      $(window).on('scroll.anchorNav', e => {
        // set anchor nav to be sticky
        if ($(window).scrollTop() >= pageBannerPos - $('.site-header').outerHeight()) {
          $anchorNav.addClass('is-sticky');
        } else {
          $anchorNav.removeClass('is-sticky');
        }

        // check scroll position against section position
        clearTimeout(scrollTimeout);
        scrollTimeout = setTimeout(() => {
          let activeSet = false;
          sectionPos.map((i, position) => {
            // add 1px here to remove uncertainty
            if ($(window).scrollTop() + 1 >= position - anchorNavHeight - $('.site-header').outerHeight() - 30) {
              $('li.active', $anchorNav).removeClass('active');
              $links.eq(i).addClass('active');
              activeSet = true;
              
              clearTimeout(scrollTimeout);
            }else{
              // check if scroll to top and highlight back the first item
              if(i==0){
                $('li.active', $anchorNav).removeClass('active');
                $links.eq(i).addClass('active');
                activeSet = true;
              }
            }
          });

          // if (!activeSet) {
          //  // $('li.active', $anchorNav).removeClass('active');
          // }
        }, 100);
      }).trigger('scroll.anchorNav');
    });
  }
}

'use strict';

import SelectedPosts from '../selected-posts';

describe('SelectedPosts View', function() {

  beforeEach(() => {
    this.selectedPosts = new SelectedPosts();
  });

  it('Should run a few assertions', () => {
    expect(this.selectedPosts).toBeDefined();
  });

});

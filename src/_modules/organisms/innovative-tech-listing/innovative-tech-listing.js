'use strict';
import dot from 'dot';
import ListingBase from '../../../_scripts/listingBase.js';
import FiltersGroup from '../../molecules/filters-group/filters-group';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';

export default class InnovativeTechListing extends ListingBase {
  constructor() {
    let dataModel = {
      keyword: '',
      industries: '',
      programmes: '',
      page: 1
    };
    
    let parametersModel = {
      keyword: '',
      industries: '',
      programmes: '',
      page: 1
    };

    super($('.pagination', $('.innovative-tech-listing')), dataModel, parametersModel);
    this.filters = new FiltersGroup($('.filters-group', $('.innovative-tech-listing')));

    this.$template = $('#innovativeTechListingTemplate').html();
    this.$content = $('.innovative-tech-listing__content', $('.innovative-tech-listing'));
    this.endpoint = $('.innovative-tech-listing').data('endpoint');
    this.$loadingIcon = new LoadingIcon($('.loading-icon', $('.innovative-tech-listing')));

    this.init();
  }

  init() {
    const $search = $('#keyword', $('.filters-group', $('.innovative-tech-listing')));
    const $programmes = $('input[name="programmes"]', $('.filters-group', $('.innovative-tech-listing')));
    const $industries = $('input[name="industries"]', $('.filters-group', $('.innovative-tech-listing')));

    let programmeString = this.setupCheckboxes('programmes', this.queries, this.filters);
    let industryString = this.setupCheckboxes('industries', this.queries, this.filters);

    let page = 1;
    if (this.queries['page']) {
      page = decodeURIComponent(this.queries['page']);
    }

    if(this.queries['keyword']) {
      $search.val(decodeURIComponent(this.queries['keyword']));
    }

    this.data.keyword = $search.val();
    this.data.industries = industryString;
    this.data.programmes = programmeString;
    this.data.page = page;

    this.parameters.keyword = this.data.keyword;
    this.parameters.industries = this.data.industries;
    this.parameters.programmes = this.data.programmes;
    this.parameters.page = this.data.page;

    this._updateData = () => {
      this.$content.addClass('content-updating');
      this.$loadingIcon.playAnimation();
      this._pushDataLayer();

      const scrollPos = $(window).scrollTop();
      if (scrollPos >= this.$content.offset().top) {
        $('html, body').animate({
          scrollTop: this.$content.offset().top - 140
        });
      }

      const callback = (res) => {
        this._renderTemplate(res);
        if (res.items.length == 0) {
          this.$pagination.hide();
        } else {
          this.$pagination.show();
          this._renderPagination(res.totalpages);
        }
      };

      this.updateData({
        endpoint: this.endpoint,
        data: this.data,
        callback: callback
      });
    }

    window.onpopstate = (e) => {
      this.data.keyword = e.state.keyword;
      this.data.industries = e.state.industries;
      this.data.programmes = e.state.programmes;
      this.data.page = e.state.page;

      this.parameters.keyword = this.data.keyword;
      this.parameters.industries = this.data.industries;
      this.parameters.programmes = this.data.programmes;
      this.parameters.page = this.data.page;

      this.queries = this.getUrlQueries();

      programmeString = this.setupCheckboxes('programmes', this.queries, this.filters);
      industryString = this.setupCheckboxes('industries', this.queries, this.filters);

      $search.val(this.data.keyword);

      this._updateData();
    };

    /* Add listener */
    this.addCheckboxListener('industries', 'industries', this.filters, this._updateData);
    this.addCheckboxListener('programmes', 'programmes', this.filters, this._updateData);

    $search.on('keypress', e => {
      if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        this.submitHandler($search);
      }
    });
    
    $search.parent().find('.btn-submit').on('click', e => {
      e.preventDefault();
      this.submitHandler($search);
    });

    $programmes.map((i, ele) => {
      const $parent = $(ele).parent();
      if ($parent.find('.filters-group__subgroup').length) {
        const subgroupName = $parent.find('.filters-group__subgroup').find('input[type="checkbox"]').attr('name');
        this.addCheckboxListener(subgroupName, 'programmes', this.filters, this._updateData, $(ele).attr('name'));
      }
    });

    $industries.map((i, ele) => {
      const $parent = $(ele).parent();
      if ($parent.find('.filters-group__subgroup').length) {
        const subgroupName = $parent.find('.filters-group__subgroup').find('input[type="checkbox"]').attr('name');
        this.addCheckboxListener(subgroupName, 'industries', this.filters, this._updateData, $(ele).attr('name'));
      }
    });

    this.data.industries = this.updateCheckboxValues($industries, this.filters);
    this.parameters.industries = this.data.industries;
    this.data.programmes = this.updateCheckboxValues($programmes, this.filters);
    this.parameters.programmes = this.data.programmes;

    /* Make call upon page load */
    this._updateData();
    this.updateURL(true);
  }

  submitHandler($search) {
    this.data.keyword = $search.val();
    this.parameters.keyword = this.data.keyword;
    this.data.page = 1;
    this.parameters.page = 1;
    this._updateData();
    this.updateURL();
    $('.btn-pill', '.filters-group__header').trigger('click');
  }

  _renderTemplate(data) {
    const dotTemplate = dot.template(this.$template)(data.items);

    $('.innovative-tech-listing__list', $('.innovative-tech-listing')).html(dotTemplate);

    setTimeout(() => {
      this.$content.addClass('content-loaded');
      this.$content.removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);
  }

  _renderPagination(totalpages) {
    const callback = () => {
      this.$pagination.find('a').map((i, ele) => {

        $(ele).on('click', e => {
          e.preventDefault();
          this.data.page = $(ele).data('page');
          this.parameters.page = this.data.page;

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }

  _pushDataLayer() {
    let filterValues = `[${this.parameters.keyword ? this.parameters.keyword + '|' : ''}${this.parameters.industries === 'all' || this.parameters.industries === 'All' ? 'businessType all|' :this.parameters.industries ? this.parameters.industries + '|' : ''}${this.parameters.programmes === 'all' || this.parameters.programmes === 'All' ? 'supportType all' :this.parameters.programmes ? this.parameters.programmes : ''}]`;
    let dataLayerModel = {
      'event': 'historyChange-v2',
      'businessType': `[${this.parameters.industries ? this.parameters.industries : ''}]`,
      'keyword': `[${this.parameters.keyword ? this.parameters.keyword : ''}]`,
      'supportType': `[${this.parameters.programmes ? this.parameters.programmes : ''}]`,
      'filters': filterValues ? filterValues : ''
    }
    window.dataLayer = window.dataLayer || [];
    dataLayer.push(
      dataLayerModel
    );
  }
}
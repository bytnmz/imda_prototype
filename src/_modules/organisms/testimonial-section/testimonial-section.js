'use strict';


import $ from 'jquery';
import 'slick-carousel';

export default class TestimonialSection {
  constructor() {
    $('.testimonial-carousel').slick({
      speed: 700,
      autoplaySpeed: 5000,
      slidesToShow: 1,
      infinite: true,
      dots: true,
      arrows: true,
      cssEase: 'ease-in-out',
      nextArrow: $('.carousel-arrow--next'),
      prevArrow: $('.carousel-arrow--prev'),
      centerPadding: '10px',
      autoplay: true,
      dotsClass: "testimonial-slider-dots",
      responsive: [
        {
          breakpoint: 760,
          settings: {
            arrows: true,
            slidesToShow: 1
          }
        }
      ]
    });
  }
}

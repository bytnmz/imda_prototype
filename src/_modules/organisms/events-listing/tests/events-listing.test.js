'use strict';

import EventsListing from '../events-listing';

describe('EventsListing View', function() {

  beforeEach(() => {
    this.eventsListing = new EventsListing();
  });

  it('Should run a few assertions', () => {
    expect(this.eventsListing).toBeDefined();
  });

});

'use strict';

import $ from 'jquery';
import dot from 'dot';
import Masonry from 'masonry-layout';
import Cookies from 'js-cookie';
import ListingBase from '../../../_scripts/listingBase.js';
import ListingFilter from '../../molecules/listing-filter/listing-filter';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';

export default class EventsListing extends ListingBase {
  constructor() {
    let $eventsListing = $('.events-listing');
    let $filters = $('.events-listing__filters', $eventsListing);
    let $content = $('.events-listing__content', $eventsListing);
    let $pagination = $('.events-listing__pagination', $eventsListing);
    let $list = $('.events-listing__list', $eventsListing);

    let $searchIconTop = $('#icon-search-top');
    let $searchInput = $('.listing-filter__search');

    let dataModel = {
      keyword: '',
      support: '',
      activity: '',
      format: '',
      period: '',
      type: '',
      page: 1,
      itemsPerPage: $(window).width() >= 1024 ? 9 : 6
    };

    let parametersModel = {
      keyword: '',
      support: '',
      activity: '',
      format: '',
      period: '',
      type: '',
      page: 1,
      itemsPerPage: $(window).width() >= 1024 ? 9 : 6
    };

    super($pagination, dataModel, parametersModel);

    this.msnry = new Masonry('.events-listing__list', {
      percentPosition: true,
      horizontalOrder: true
    });

    new ListingFilter($filters);

    // Setup basic
    this.$template = $('#eventsListingTemplate').html();
    this.$content = $content;
    this.$list = $list;
    this.endpoint = $eventsListing.data('endpoint');
    this.$loadingIcon = new LoadingIcon($('.loading-icon', $eventsListing));

    // Define fields
    let $search = $('#keyword', $filters);
    let $support = $('#support', $filters);
    let $activity = $('#activity', $filters);
    let $format = $('#format', $filters);
    let $period = $('#period', $filters);
    let $type = $('#type', $eventsListing);

    // get all parameters value
    let queries = this.getUrlQueries();

    let page = 1;
    if (queries['page']) {
      page = decodeURIComponent(queries['page']);
    }

    if(queries['keyword']) {
      $search.val(decodeURIComponent(queries['keyword']));
    }

    if (queries['activity']) {
      $activity.val(queries['activity']);
      $activity.trigger('change.customSelect');
    }

    if (queries['support']) {
      $support.val(queries['support']);
      $support.trigger('change.customSelect');

      const profileLabel = $(`#support`).next().find(`[data-value="${queries['support']}"]`).find('button').text();
      $('.profile-label', this.$selector).text(profileLabel);
    } else {
      //Check if there's cookie value for IMDA profile, if profile is present, pre-select the profile filter
      if (Cookies.get('imda_profile')) {
        const profile = Cookies.get('imda_profile');
        const matchedProfileOption = $('option[data-profile="' + profile + '"]');
        
        if (matchedProfileOption) {
          $support.val(matchedProfileOption.val());
          $support.trigger('change.customSelect');
        }

        const profileLabel = $(`#support`).next().find(`[data-profile=${profile}]`).find('button').text();
        $('.profile-label', this.$selector).text(profileLabel);
      }
    }

    if (queries['format']) {
      $format.val(queries['format']);
      $format.trigger('change.customSelect');
    }

    if (queries['period']) {
      $period.val(queries['period']);
      $period.trigger('change.customSelect');
    }
    
    if (queries['type']) {
      if(queries['type'] == 'signature') {
        $type.prop('checked', true);
      }
      else {
        $type.prop('checked', false);
      }
    }


    // Setup Data Structure
    this.data.keyword = $search.val();
    this.data.support = $support.val() ? $support.val() : 'all';
    this.data.activity = $activity.val() ? $activity.val() : 'all';
    this.data.format = $format.val() ? $format.val() : 'all';
    this.data.period = $period.val() ? $period.val() : 'all';
    this.data.type = $type.is(':checked') ? $type.val() : 'all';
    this.data.page = page;

    this.parameters.keyword = this.data.keyword;
    this.parameters.activity = this.data.activity;
    this.parameters.support = this.data.support;
    this.parameters.format = this.data.format;
    this.parameters.period = this.data.period;
    this.parameters.type = this.data.type;
    this.parameters.page = this.data.page;

    this.currentProfile = $('#support option:selected').data('profile');

    if(this.currentProfile === 'profile0') {
      //update UI to show "all" if no profile set 
      $activity.val(this.data['activity']);
      $activity.trigger('change.customSelect');

      $format.val(this.data['format']);
      $format.trigger('change.customSelect');
      
      $period.val(this.data['period']);
      $period.trigger('change.customSelect');
      
      $type.prop('checked', false);
    }

    this._updateData = () => {
      this.$content.addClass('content-updating');
      this.$loadingIcon.playAnimation();
      // this._pushDataLayer();

      let scrollPos = $(window).scrollTop();
      if (scrollPos >= this.$content.offset().top) {
        $('html, body').animate({
          scrollTop: this.$content.offset().top - 230
        });
      }

      let callback = (res) => {
        this._renderTemplate(res);
        if (res.items.length == 0) {
          this.$pagination.hide();
        } else {
          this.$pagination.show();
          this._renderPagination(res.totalpages);
        }
      };

      this.updateData({
        endpoint: this.endpoint,
        data: this.data,
        callback: callback
      });
    };

    window.onpopstate = (e) => {
      this.data.keyword = e.state.keyword;
      this.data.activity = e.state.activity;
      this.data.support = e.state.support;
      this.data.format = e.state.format;
      this.data.period = e.state.period;
      this.data.type = e.state.type;
      this.data.page = e.state.page;

      this.parameters.keyword = this.data.keyword;
      this.parameters.activity = this.data.activity;
      this.parameters.support = this.data.support;
      this.parameters.format = this.data.format;
      this.parameters.period = this.data.period;
      this.parameters.type = this.data.type;
      this.parameters.page = this.data.page;

      $search.val(this.data.keyword);

      $activity.val(this.data.activity);
      $activity.trigger('change.customSelect');

      $support.val(this.data.support);
      $support.trigger('change.customSelect');
      const profileLabel = $(`#support`).next().find(`[data-value="${this.data.support}"]`).find('button').text();
      $('.profile-label', this.$selector).text(profileLabel);

      $format.val(this.data.format);
      $format.trigger('change.customSelect');

      $period.val(this.data.period);
      $period.trigger('change.customSelect');

      if(this.data.type == 'signature') {
        $type.prop('checked', true);
      }
      else {
        $type.prop('checked', false);
      }

      this._updateData();
    };

    // Add listener
    let checkingTimeout;
    $search.on('keypress', e => {
      if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        this.data.keyword = $search.val();
        this.parameters.keyword = this.data.keyword;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
      }
    });

    $search.parent().find('.btn-submit').on('click', e => {
      e.preventDefault();
      this.data.keyword = $search.val();
      this.parameters.keyword = this.data.keyword;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });

    $activity.on('change', e => {
      this.data.activity = $activity.val();
      this.parameters.activity = this.data.activity;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });
    
    $support.on('change', e => {
      this.data.support = $support.val();
      this.parameters.support = this.data.support;
      this.data.page = 1;
      this.parameters.page = 1;
      this.currentProfile = $(`#support option[value="${this.parameters.support}"]`).data('profile');

      const profileLabel = $(`#support`).next().find(`[data-value="${this.data.support}"]`).find('button').text();
      $('.profile-label', this.$selector).text(profileLabel);

      // this.processFilterChanges($activity, $type, $format, $period);
      this._updateData();
      this.updateURL();
    });
    
    $format.on('change', e => {
      this.data.format = $format.val();
      this.parameters.format = this.data.format;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });
    
    $period.on('change', e => {
      this.data.period = $period.val();
      this.parameters.period = this.data.period;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });
    
    $type.on('change', e => {
      if ($type.prop('checked') == true) {
        this.data.type = $type.val();
      }
      else {
        this.data.type = 'all';
      }

      this.parameters.type = this.data.type;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });
    
    $searchIconTop.on('click', e => {
      $searchIconTop.toggleClass('icon-close');
      $searchIconTop.toggleClass('icon-search');
      $searchInput.toggleClass('show');
    });

    // Make call upon page load
    this._updateData();
    this.updateURL(true);
  }

  updateUI($activity) {
    if(this.currentProfile !== "profile1") {
      $activity.val("all");
      this.data.activity = $activity.val();
      this.parameters.activity = this.data.activity;
      $activity.trigger('change.customSelect');
      
      $('[data-filtername="activity"]').addClass('hide');
    } else {
      $('[data-filtername="activity"]').removeClass('hide');
    }
  
    $(`.events-listing__filters [data-profilelist]`).addClass('hide');
    $(`.events-listing__filters [data-profilelist="${this.currentProfile}"]`).removeClass('hide');
  }

  processFilterChanges($activity, $type, $format, $period) {
    this.updateUI($activity);

    // substitute for custom select change event for $type
    this.substituteEvent($type, 'type');
    
    // substitute for custom select change event for $format
    this.substituteEvent($format, 'format');

    // substitute for custom select change event for $period
    this.substituteEventValueAll($period, 'period');
  }

  substituteEvent($target, targetKey) {
    //set target value
    $target.val($target.next().find(`[data-profilelist="${this.currentProfile}"]`).data('value'));
    this.data[targetKey] = $target.val();
    this.parameters[targetKey] = this.data[targetKey];

    // substitute for custom select change event
    $target.next().find('.active').removeClass('active');
    $target.next().find(`[data-value="${ this.data[targetKey] }"]`).addClass('active');

    $target.next().find('.label').text($target.next().find(`[data-profilelist="${this.currentProfile}"]`).first().find('button').text().length ? $target.next().find(`[data-profilelist="${this.currentProfile}"]`).first().find('button').text() : $target.next().find(`li[data-value="all"]`).first().find('button').text());
  }

  substituteEventValueAll($target, targetKey) {
    //set target value
    $target.val("all");
    this.data[targetKey] = $target.val();
    this.parameters[targetKey] = this.data[targetKey];

    // substitute for custom select change event
    $target.next().find('.active').removeClass('active');
    $target.next().find(`[data-value="all"]`).addClass('active');

    $target.next().find('.label').text($target.next().find(`[data-value="all"]`).find('button').text());
  }

  _renderTemplate(data) {
    this.msnry.destroy();
    let dotTemplate = dot.template(this.$template)(data.items);

    this.$list.html(dotTemplate);

    this.msnry = new Masonry('.events-listing__list', {
      percentPosition: true,
      horizontalOrder: true
    });

    this.updateSummary(data.totalItems);

    setTimeout(() => {
      this.$content.addClass('content-loaded');
      this.$content.removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);
  }

  updateSummary(totalItems) {
    if(parseInt(totalItems) === 0) {
      $('.events-listing__summary .page').text(`0 - 0`);
    }

    if(parseInt(totalItems) !== 0) {
      const start = ((this.data.page - 1) * this.data.itemsPerPage) + 1;
      const end = (this.data.page) * this.data.itemsPerPage > totalItems ? totalItems : (this.data.page) * this.data.itemsPerPage;
      
      $('.events-listing__summary .page').text(`${start} - ${end}`);
    }
    
    $('.events-listing__summary .total').text(totalItems);
  }

  _renderPagination(totalpages) {
    let callback = () => {
      this.$pagination.find('a').map((i, ele) => {
        let $this = $(ele);

        $this.on('click', e => {
          e.preventDefault();
          this.data.page = $this.data('page');
          this.parameters.page = this.data.page;

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }

  _pushDataLayer() {
    let dataLayerModel = {};
    let filterValues = '';
    let dataProfile = this.parameters.support;

    switch (dataProfile) {
      case 'All': 
      case 'Businesses':
      case 'all':
      case 'businesses':
        filterValues = `[${this.parameters.keyword ? this.parameters.keyword + '|' : ''}${this.parameters.activity ? this.parameters.activity + '|' : ''}${this.parameters.support ? this.parameters.support + '|' : ''}${this.parameters.format ? this.parameters.format + '|' : ''}${this.parameters.period ? this.parameters.period + '|' : ''}${this.parameters.type ? this.parameters.type : ''}]`;
        dataLayerModel = {
          'event': 'historyChange-v2',
          'keyword': `[${this.parameters.keyword ? this.parameters.keyword : ''}]`,
          'activityType': `[${this.parameters.activity ? this.parameters.activity : ''}]`,
          'profileType': `[${this.parameters.support ? this.parameters.support : ''}]`,
          'contentDate': `[${this.parameters.format ? this.parameters.format : ''}]`,
          'contentPeriod': `[${this.parameters.period ? this.parameters.period : ''}]`,
          'eventType': `[${this.parameters.type ? this.parameters.type : ''}]`,
          'filters': filterValues ? filterValues : ''
        }
        break;

      case 'Students-&-Professionals':
      case 'Public-&-Seniors':
      case 'students and professionals':
      case 'public and seniors':
        filterValues = `[${this.parameters.keyword ? this.parameters.keyword + '|' : ''}${this.parameters.support === 'all' || this.parameters.support === 'All' ? 'profileType all|' : this.parameters.support ? this.parameters.support + '|' : ''}${this.parameters.format === 'all' || this.parameters.format === 'All' ? 'contentDate all|' : this.parameters.format ? this.parameters.format + '|' : ''}${this.parameters.period === 'all' || this.parameters.period === 'All' ? 'contentperiod all|' : this.parameters.period ? this.parameters.period + '|' : ''}${this.parameters.type === 'all' || this.parameters.type === 'All' ? 'supportType all' : this.parameters.type ? this.parameters.type : ''}]`;
        dataLayerModel = {
          'event': 'historyChange-v2',
          'keyword': `[${this.parameters.keyword ? this.parameters.keyword : ''}]`,
          'profileType': `[${this.parameters.support ? this.parameters.support : ''}]`,
          'contentDate': `[${this.parameters.format ? this.parameters.format : ''}]`,
          'contentperiod': `[${this.parameters.period ? this.parameters.period : ''}]`,
          'eventType': `[${this.parameters.type ? this.parameters.type : ''}]`,
          'filters': filterValues ? filterValues : ''
        }
        break;
    
      default:
        break;
    }

    window.dataLayer = window.dataLayer || [];
    dataLayer.push(
      dataLayerModel
    );
  }
}

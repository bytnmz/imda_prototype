'use strict';

import Cookies from 'js-cookie';
import dot from 'dot';
import { initCarousel } from '../../../_scripts/helpers';

export default class ThoughtLeadership {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.$template = $('#thoughtLeadershipTemplate', this.$selector).html();
    this.endpoint = this.$selector.data('endpoint');
    this.data = {};
    this.data.profile = Cookies.get('imda_profile');
    this.data.type = this.$selector.data('type');
    this.state = {};

    this.carouselOptions = {
      dots: false,
      arrows: false,
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            arrows: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            arrows: false
          }
        }
      ]
    };
    
    this.resultCarousel = null;
    if(selfInit) this.init();
  }
  
  init() {
    if (this.data.profile === 'profile0') {
      this.data.profile = 'profile1';
    }

    this.fetchData();

    let timer;

    $(window).on('resize.resultsCarousel', () => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        if(this.resultCarousel) {
          this.resultCarousel.unslick();
          this.resultCarousel.slickInit();
        }
      }, 500);
    });
    
    $('.site-header__profile ul li').on('click', (e) => {
      let currentProfile = String(this.data.profile);
      this.checkProfileSelected(currentProfile);
    }); 
  }

  checkProfileSelected(currentProfile) {
    setTimeout(() => {
      let selectedProfile = Cookies.get('imda_profile');
      if (selectedProfile != currentProfile) {
        this.data.profile = selectedProfile;
        this.renderList(this.state.data, String(this.data.profile));  
      }
    }, 700);
  }

  fetchData() {
    let dataObject = {
      profile: this.data.profile,
      type: this.data.type
    }

    $.ajax({
      url: this.endpoint,
      async: true,
      method: 'post',
      data: JSON.stringify(dataObject),
      contentType: 'application/json',
      success: (res) => {
        this.state.data = {...res};
        this.renderList(this.state.data, String(this.data.profile));
      },
      error: (err) => {
        console.log(err);
      }
    });
  }

  renderList(data, profile) {
    let thoughtArray = [];
    if (profile === 'profile0') {
      profile = 'profile1';
    }
    
    for(let i = 0; i < data.items.length; i++) {
      if(data.items[i].profile === profile) {
        thoughtArray.push(data.items[i]);
      }
    }

    const _renderedHTML = dot.template(this.$template)(thoughtArray);


    if(this.resultCarousel) {
      this.destroyResultsCarousel();
    }

    $('.thought-leadership__body__label', this.$selector).addClass('active');
    $('.thought-leadership__body', this.$selector).addClass('reloaded').html(_renderedHTML);
    
    this.initResultsCarousel();

    this.fadein($('.thought-leadership__body article', this.$selector));
  }

  destroyResultsCarousel() {
    this.resultCarousel.unslick();
  }

  initResultsCarousel() {
    this.resultCarousel = initCarousel($('.thought-leadership__body [data-carousel]', this.$selector), this.carouselOptions);
  }

  fadein($elem) {
    let interval = 50;

    $elem.map((i,ele) => {
      setTimeout(() => {
        $(ele).addClass('fadein');
      }, interval);

      interval +=100;
    });
  }
}

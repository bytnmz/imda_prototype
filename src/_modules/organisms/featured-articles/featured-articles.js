'use strict';

import $ from 'jquery';
import 'slick-carousel';
import 'jquery-match-height';

export default class FeaturedArticles {
  constructor(config) {
    let { breakpoints } = config;

    let $featuredArticles = $('.featured-articles');
    let $posts = $('.featured-articles__posts', $featuredArticles);

    $posts.slick({
      centerPadding: '30px',
      infinite: false,
      prevArrow: '<button class="btn btn-circle slick-prev" type="button"><span class="icon icon-arrow-left"></span><span class="vh">Previous slide</span></button>',
      nextArrow: '<button class="btn btn-circle slick-next" type="button"><span class="icon icon-arrow-right"></span><span class="vh">Next slide</span></button>',
      slidesToShow: 4,
      responsive: [
        {
          breakpoint: breakpoints.desktop,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: breakpoints.tablet,
          settings: {
            dots: true,
            slidesToShow: 1,
            arrows: false
          }
        }
      ]
    });

    $posts.on('breakpoint', e => {
      $posts.find('.image-card__content').matchHeight();
    });

    $posts.find('.image-card__content').matchHeight();

  }
}

'use strict';

import FeaturedArticles from '../featured-articles';

describe('FeaturedArticles View', function() {

  beforeEach(() => {
    this.featuredArticles = new FeaturedArticles();
  });

  it('Should run a few assertions', () => {
    expect(this.featuredArticles).toBeDefined();
  });

});

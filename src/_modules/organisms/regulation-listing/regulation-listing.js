'use strict';

import $ from 'jquery';
import dot from 'dot';
import ListingBase from '../../../_scripts/listingBase.js';
import FiltersGroup from '../../molecules/filters-group/filters-group';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';

export default class RegulationListing extends ListingBase {
  constructor() {
    let $regulationListing = $('.regulation-listing');
    let $content = $('.regulation-listing__content', $regulationListing);
    let $filtersGroup = $('.filters-group', $regulationListing);
    let $pagination = $('.pagination', $regulationListing);
    let $list = $('.regulation-listing__list', $regulationListing);

    /* define AJAX data model */
    let dataModel = {
      keyword: '',
      categories: '',
      industries: '',
      page: 1
    };

    /* Define query parameters model */
    let parametersModel = {
        keyword: '',
        category: '',
        industry: '',
        page: 1
    };

    super($pagination, dataModel, parametersModel);

    let filters = new FiltersGroup($filtersGroup);
    this.filters = filters;

    /* Setup basic */
    this.$template = $('#regulationListingTemplate').html();
    this.$content = $content;
    this.$list = $list;
    this.endpoint = $regulationListing.data('endpoint');
    this.$loadingIcon = new LoadingIcon($('.loading-icon', $regulationListing));

    /* Define fields */
    let $search = $('#keyword', $filtersGroup);
    let $categories = $('input[name="category"]', $filtersGroup);
    let $industries = $('input[name="industry"]', $filtersGroup);

    this.$categories = $categories;
    this.$industries = $industries;

    /* get all parameters value */
    let queries = this.getUrlQueries();

    let categoryString = this.setupCheckboxes('category', queries, this.filters);
    let industryString = this.setupCheckboxes('industry', queries, this.filters);

    let page = 1;
    if (queries['page']) {
      page = decodeURIComponent(queries['page']);
    }

    if(queries['keyword']) {
      $search.val(decodeURIComponent(queries['keyword']));
    }

    /* Populate Data */
    this.data.keyword = $search.val();
    this.data.categories = categoryString;
    this.data.industries = industryString;
    this.data.page = page;

    this.parameters.keyword = this.data.keyword;
    this.parameters.category = this.data.categories;
    this.parameters.industry = this.data.industries;
    this.parameters.page = this.data.page;

    this._updateData = (replaceState) => {
      this.$content.addClass('content-updating');
      this.$loadingIcon.playAnimation();
      this._pushDataLayer();

      let scrollPos = $(window).scrollTop();
      if (scrollPos >= this.$content.offset().top) {
        $('html, body').animate({
          scrollTop: this.$content.offset().top - 140
        });
      }

      let callback = (res) => {
        this._renderTemplate(res);
        if (res.items.length == 0) {
          this.$pagination.hide();
        } else {
          this.$pagination.show();
          this._renderPagination(res.totalpages);
        }
      };

      this.updateData({
        endpoint: this.endpoint,
        data: this.data,
        callback: callback
      });
    };

    window.onpopstate = (e) => {
      this.data.categories = e.state.categories;
      this.data.industries = e.state.industries;
      this.data.keyword = e.state.keyword;
      this.data.page = e.state.page;

      this.parameters.keyword = this.data.keyword;
      this.parameters.category = this.data.categories;
      this.parameters.industry = this.data.industries;
      this.parameters.page = this.data.page;

      queries = this.getUrlQueries();

      categoryString = this.setupCheckboxes('category', queries, this.filters);
      industryString = this.setupCheckboxes('industry', queries, this.filters);

      $search.val(this.data.keyword);

      this._updateData();
    };

    /* Add listener */
    this.addCheckboxListener('category', 'categories', this.filters, this._updateData);
    this.addCheckboxListener('industry', 'industries', this.filters, this._updateData);

    $search.on('keypress', e => {
      if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        this.data.keyword = $search.val();
        this.parameters.keyword = this.data.keyword;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
        $('.btn-pill', '.filters-group__header').trigger('click');
      }
    });

    $search.parent().find('.btn-submit').on('click', e => {
      e.preventDefault();
      this.data.keyword = $search.val();
      this.parameters.keyword = this.data.keyword;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
      $('.btn-pill', '.filters-group__header').trigger('click');
    });

    /* get all checked values */
    /*
      - loop through all checkboxes and find out if there is any subgroup
      - store the subgroup field name into array
      - add checkbox listener to the subgroup checkboxes
      - concatenate all subgroup field values with main field value
    */
    $categories.map((i, ele) => {
      let $this = $(ele);
      let $parent = $this.parent();
      if ($parent.find('.filters-group__subgroup').length) {
        let subgroupName = $parent.find('.filters-group__subgroup').find('input[type="checkbox"]').attr('name');
        this.addCheckboxListener(subgroupName, 'categories', this.filters, this._updateData, $this.attr('name'));
      }
    });
    this.data.categories = this.updateCheckboxValues($categories, this.filters);
    this.parameters.category = this.data.categories;

    $industries.map((i, ele) => {
      let $this = $(ele);
      let $parent = $this.parent();
      if ($parent.find('.filters-group__subgroup').length) {
        let subgroupName = $parent.find('.filters-group__subgroup').find('input[type="checkbox"]').attr('name');
        this.addCheckboxListener(subgroupName, 'industries', this.filters, this._updateData, $this.attr('name'));
      }
    });

    this.data.industries = this.updateCheckboxValues($industries, this.filters);
    this.parameters.industry = this.data.industries;

    /* Make call upon page load */
    this._updateData();
    this.updateURL(true);
  }

  _renderTemplate(data) {
    let dotTemplate = dot.template(this.$template)(data.items);

    this.$list.html(dotTemplate);

    setTimeout(() => {
      this.$content.addClass('content-loaded');
      this.$content.removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);
  }

  _renderPagination(totalpages) {
    let callback = () => {
      this.$pagination.find('a').map((i, ele) => {
        let $this = $(ele);

        $this.on('click', e => {
          e.preventDefault();
          this.data.page = $this.data('page');
          this.parameters.page = this.data.page;

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }

  _pushDataLayer() {
    let filterValues = `[${this.parameters.keyword ? this.parameters.keyword + '|' : ''}${this.parameters.category === 'all' || this.parameters.category === 'All' ? 'regulationType all|' : this.parameters.industry ? this.parameters.industry + '|' : ''}${this.parameters.industry === 'all' || this.parameters.category === 'All' ? 'industryType all' : this.parameters.industry ? this.parameters.industry : ''}]`;
    let dataLayerModel = {
      'event': 'historyChange-v2',
      'keyword': `[${this.parameters.keyword ? this.parameters.keyword : ''}]`,
      'regulationType': `[${this.parameters.category ? this.parameters.category : ''}]`,
      'industryType': `[${this.parameters.industry ? this.parameters.industry : ''}]`,
      'filters': filterValues ? filterValues : ''
    }
    window.dataLayer = window.dataLayer || [];
    dataLayer.push(
      dataLayerModel
    );
  }
}

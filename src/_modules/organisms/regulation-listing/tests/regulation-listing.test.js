'use strict';

import RegulationListing from '../regulation-listing';

describe('RegulationListing View', function() {

  beforeEach(() => {
    this.regulationListing = new RegulationListing();
  });

  it('Should run a few assertions', () => {
    expect(this.regulationListing).toBeDefined();
  });

});

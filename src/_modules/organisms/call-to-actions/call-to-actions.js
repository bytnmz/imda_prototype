'use strict';
import * as LottiePlayer from "@lottiefiles/lottie-player";

export default class CallToActions {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
		$(window).on('load scroll', () => {
      const offsetTop = this.$selector.offset().top,
        scrollTrigger = $(window).height() * 0.8;

      if (($(window).scrollTop() + scrollTrigger) > offsetTop) {
        this.startTransitions();
      }
    });
  }

  startTransitions() {
    $('.call-to-actions__lottie', this.$selector).addClass('fade-in');
    // $('lottie-player', this.$selector)[0].play();
    $('.call-to-actions__text', this.$selector).addClass('fade-in');
  }
}

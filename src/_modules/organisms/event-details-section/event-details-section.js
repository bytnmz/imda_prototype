'use strict';

export default class EventDetailsSection {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    const $toggle = this.$selector.find('header');
    const $content = this.$selector.find('> div');

    $toggle.on('click', (e) => {
      e.preventDefault();

      if(!this.$selector.hasClass('active')) {
        $content.slideDown(300, () => {
          window.emitter.emit('accordionOpened');
        });
        this.$selector.addClass('active');
      }
      else {
        $content.slideUp(300);
        this.$selector.removeClass('active');
      }
    });
  }
}

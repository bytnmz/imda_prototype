'use strict';

import Pikaday from 'pikaday';
import fecha from 'fecha';

export default class EventForm {
  constructor() {
    let _self = this,
        $event = $('.event-form'),
        $eventContainer = $('.event-form__container', $event),
        $nextBtn = $('.js-next-step', $event),
        $prevBtn = $('.js-prev-step', $event),
        $submitBtn =  $('.scfSubmitButtonBorder', $event);

    $event.closest('form').attr('novalidate', 'novalidate');



    window.resize = function(animate) {
      animate = animate || true;

      var height = $('.event-form-iframe').contents().find('body').outerHeight() + 50;

      $('.event-form-iframe').height(height);

      if (animate) {
        $('html, body').animate({
          scrollTop: $('.event-form-iframe').offset().top - $('#primary-nav').outerHeight()
        }, 500);
      }
    }

    $submitBtn.detach().insertAfter($nextBtn).find('input[type="submit"]').addClass('cbtn');

    $nextBtn.on('click', function(e){
      e.preventDefault();

      let id = $eventContainer.children('.is-active').next().data('content');

      let $form = $event.closest('form'),
          isValidate = $form[0].checkValidity();

      _self.clickEvent(id);
    });

    $prevBtn.on('click', function(e){
      e.preventDefault();

      let id = $eventContainer.children('.is-active').prev().data('content');

      let $form = $event.closest('form'),
          isValidate = $form[0].checkValidity();

      _self.clickEvent(id);
    });

    $('.js-change-step', $event).on('click', function(e){
      e.preventDefault();

      let id = $(this).data('content');


      let $form = $event.closest('form'),
          isValidate = $form[0].checkValidity();

      _self.clickEvent(id);
    });

    $('.scfChecklistBorder').each(function(){
      let $this = $(this),
          $errorMsg = $this.find('.scfValidator').first();

      if ($this.data('max')) {
        let max = $this.data('max'),
            $checkbox = $this.find('input[type=checkbox]');

        $checkbox.on('change', function (e) {
          if ($this.find('input[type=checkbox]:checked').length > max) {
            $(this).prop('checked', false);
            $errorMsg.show();
          } else {
            $errorMsg.hide();
          }
        });
      }

    });

    // convert input date into datepicker
    $('input[type="date"]', $event).each(function(){
      let $this = $(this);

      $this.attr('type', 'text').addClass('is-date-picker');

      let picker = new Pikaday({
        field: $this[0],
        minDate: new Date(),
        format: 'DD/MM/YYYY'
      });

      $this.on('keypress', function(event){
        let key = window.event ? event.keyCode : event.which;

        if (event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 191 || event.keyCode == 47) {
          return true;
        } else {
          return false;
        }
      });

      $this.on('blur', function(){
        if ($this.val() !== '') {
          _self.validateDate($this);
        }
      });
    });

  }

  clickEvent(id) {
    let _self = this,
        $event = $('.event-form'),
        $eventContainer = $('.event-form__container', $event),
        $activeContent = $eventContainer.children('.is-active'),
        isPassed = _self.checkRequiredField($activeContent),
        isPatternPassed = _self.checkPatternField($activeContent);

    if (isPassed && isPatternPassed) {
      $activeContent.hide(400, function(){
        let $this = $(this);

        $this.removeClass('is-active');

      });
      _self.showContent(id);
      $('.js-current-progress', $event).text(id);
    } else {
      let $errorField = $('.is-error', $event).first(),
          offset = $('header').outerHeight(),
          totalScrollTop = $errorField.offset().top - offset;

      $('html, body').animate({
        scrollTop: totalScrollTop
      }, 500);
    }
  }

  prevNextClicFunction(contentNumber) {
    $eventContainer.children('.is-active').hide(400, function(){
      let $this = $(this),
          prevID = $this.prev().data('content');

      $this.removeClass('is-active');
    });

    _self.showContent(prevID);
  }

  showContent(contentNumber) {
    let _self = this,
        $event = $('.event-form'),
        totalContent = $('.event-form__content', $event).length,
        $selectedContent = $('.event-form__content[data-content="' + contentNumber + '"]', $event) || $('.event-form__content[data-content="1"]', $event),
        offset = $('header').outerHeight(),
        totalScrollTop = $event.offset().top - offset - 20;

    $('html, body').animate({
      scrollTop: totalScrollTop
    }, 500);

    let width = contentNumber/totalContent;

    _self.setProgressWidth(width);
    _self.setActiveTab(contentNumber);

    $selectedContent.show(400, function(){
      $(this).addClass('is-active');
    });

    if (contentNumber == totalContent) {
      $('.js-next-step', $event).attr('disabled', 'disabled');
      $('input[type="submit"]', $event).show();
    } else {
      $('.js-next-step', $event).attr('disabled', false);
      $('input[type="submit"]', $event).hide();
    }

    if (contentNumber == '1') {
      $('.js-prev-step', $event).attr('disabled', 'disabled');
    } else {
      $('.js-prev-step', $event).attr('disabled', false);
    }
  }

  setProgressWidth(width) {
    let $event = $('.event-form'),
        $progress = $('.progress__value', $progress);

    $progress.css({"transform": "scaleX(" + width + ")"})
  }

  setActiveTab(contentNumber) {
    let _self = this,
        $event = $('.event-form'),
        $activeTab = $('.js-change-step[data-content="' + contentNumber + '"]', $event) || $('.js-change-step[data-content="1"]', $event);

    $activeTab.addClass('is-active').siblings().removeClass('is-active');
  }

  checkRequiredField($activeContent) {
    let _self = this,
        isPassed = true;

    $activeContent.find('.scfSectionContent > div').each(function(){
      let $this = $(this),
          hasAnswer = 0;

      if ($this.find('[required]').length) {
        if ($this.hasClass('is-date-picker')) {
          _self.validateDate($this);
        } else {

          if ($this.find('input[type="radio"], input[type="checkbox"]').length) {
            hasAnswer = $this.find('input:checked').length;
          } else if ($this.find('select').length) {
            if (!($this.find('select').val() === '')){
              hasAnswer = $this.find('select').val().length;
            }
          } else {
            hasAnswer = $this.find('input').val().length;
          }

          if (hasAnswer) {
            _self.removeErrorState($this, true);
          } else {
            _self.addErrorState($this, true);
            isPassed = false;
          }
        }
      }
    });

    return isPassed;
  }

  checkPatternField($activeContent) {

    let _self = this,
        isPassed = true;

    $activeContent.find('.scfSectionContent > div').each(function(){
      let $this = $(this);


      if ($this.find('[pattern]').length) {
        if ($this.find('input').val()) {
          let re = $this.find('[pattern]').attr('pattern');
          
          // if (re[0] = '/') {
          //   re = re[1];
          // } else {
          //   re = re[0];
          // }


          let regex = new RegExp(re),
              passRegex = regex.test($this.find('input').val());
          if (passRegex) {
            _self.removeErrorState($this);
          } else {
            _self.addErrorState($this);
            isPassed = false;
          }
        }
      }
    });

    return isPassed;
  }

  addErrorState($this, $required) {
    let required = $required || false;
    $this.addClass('is-error');
    if (required) {
      $this.find('.scfValidator.error-required').show();
      $this.find('.scfValidator.error-pattern').hide();
    } else {
      $this.find('.scfValidator.error-pattern').show();
      $this.find('.scfValidator.error-required').hide();
    }
  }

  removeErrorState($this, $required) {
    let required = $required || false;
    $this.removeClass('is-error');
    if (required) {
      $this.find('.scfValidator').hide();
    } else {
      $this.find('.scfValidator').hide();
    }
  }

  validateDate($this) {
    let _self = this,
        dateValue = fecha.parse($this.val(), 'DD/MM/YYYY');

    if (dateValue) {
      _self.removeErrorState($this);
    } else {
      _self.addErrorState($this);
    }
  }
}


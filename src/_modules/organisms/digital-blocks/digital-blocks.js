'use strict';

import { initCarousel } from '../../../_scripts/helpers';

export default class DigitalBlocks {
  constructor($selector, selfInit = true, config) {
    this.$selector = $selector;
    this.$mobile = $('.no-desktop', this.$selector);
    this.$desktop = $('.desktop-only', this.$selector);
    this.breakpoints = config.breakpoints;
    this.mobileOptions = {
      "rows": 3,
      "speed": 500,
      "slidesToShow": 1,
      "slidesToScroll": 1,
      "adaptiveHeight": true,
      "autoplay": false,
      "infinite": true,
      "dots": true,
      "arrows": true,
      "fade": true,
      "customPaging": (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      }
    };
    this.desktopOptions = {
      "rows": 1,
      "speed": 500,
      "slidesToShow": 1,
      "slidesToScroll": 1,
      "autoplay": false,
      "infinite": true,
      "dots": true,
      "arrows": true,
      "fade": true,
      "customPaging": (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      }
    };

    if(selfInit) this.init();
  }

  init() {
    initCarousel($('[data-carousel]', this.$mobile), this.mobileOptions);
    initCarousel($('[data-carousel]', this.$desktop), this.desktopOptions);
  }
}

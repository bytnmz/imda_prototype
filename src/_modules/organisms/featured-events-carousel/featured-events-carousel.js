'use strict';

import dot from 'dot';
import Cookies from 'js-cookie';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';
import CustomSelect from '../../custom-select/custom-select';
import { initCarousel, stackedCarousel } from '../../../_scripts/helpers';

export default class FeaturedEventsCarousel {
  constructor($selector, selfInit = true, config) {
    this.$selector = $selector;
    this.breakpoints = config.breakpoints;
    this.contentData = JSON.parse($('[type="application/json"]', this.$selector).html());
    this.$template = $(`#${this.$selector.data('templateid')}`, this.$selector).html();
    this.maxItems = this.$selector.data('maxitems') ? parseInt(this.$selector.data('maxitems')) : $('[data-carousel] article', this.$selector).length;
    this.carousel = null;
    this.carouselOptions = {
      rows: 1,
      speed: 500,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: true,
      dots: true,
      arrows: true,
      customPaging: (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      }
    };

    if($('.featured-events-carousel__background', this.$selector).length) {
      this.$imageTemplate =  $(`#${this.$selector.data('imagetemplateid')}`).html();
    }

    this.filterValue = this.$selector.hasClass('featured-events-carousel--top-picks') ? $('select', this.$selector).val() : null;

    this.$loadingIcon = new LoadingIcon($('.loading-icon', this.$selector));

    this.currentProfile = Cookies.get('imda_profile') ? Cookies.get('imda_profile') : 'profile0';

    if(selfInit) this.init();
  }

  init() {
    $('.custom-select', this.$selector).map((i, ele) => {
      const selection = new CustomSelect($(ele), true);

      $(ele).find('select').on('change', (e) => {
        if(this.$selector.hasClass('featured-events-carousel--top-picks')) this.filterValue = $(ele).find('select').val();
        if(this.$selector.hasClass('featured-events-carousel--profile-picks')) this.currentProfile = $(ele).find('select option:selected').data('profile');
        this.renderCarousel();
      })
    });

    if(this.$selector.hasClass('featured-events-carousel--profile-picks')) {
      this.setProfileDropdown();
    }

    $('.featured-events-carousel__content', this.$selector).addClass('content-updating');
    this.$loadingIcon.playAnimation();

    this.renderCarousel();

    window.emitter.on('profileChanged', (data) => {
      $('.featured-events-carousel__content', this.$selector).addClass('content-updating');
      this.currentProfile = data;
      this.setProfileDropdown();
      this.renderCarousel();
    });
  }

  setProfileDropdown() {
    const matchedProfileOption = $(`option[data-profile="${this.currentProfile}"]`, this.$selector);

    if (matchedProfileOption.length) {
      $('select', this.$selector).val(matchedProfileOption.val());
      $('select', this.$selector).trigger('change.customSelect');
      $('select', this.$selector).trigger('change');
    }
  }

  renderCarousel() {
    if(this.carousel) {
      this.carousel.unslick();
      this.carousel = null; 
      $(`[data-carousel-pager]`, this.$selector).text('');
    }

    let data = this.contentData[this.currentProfile];

    if(this.$selector.hasClass('featured-events-carousel--top-picks')) {
      if(this.filterValue.length && this.filterValue !== 'all'){
        data = data.filter((item) => {
          return item.type.toLowerCase() === this.filterValue;
        });
      }
    }

    this.maxItems = data.length;
    
    const dotTemplateContent = `<div data-carousel>${dot.template(this.$template)(data)}</div>`;

    $('.featured-events-carousel__content [data-carousel]', this.$selector).remove();
    $('.featured-events-carousel__content .loading-icon', this.$selector).after(dotTemplateContent);

    if(this.$selector.hasClass('featured-events-carousel--profile-picks')) {
      if($('.featured-events-carousel__content [data-carousel] > *', this.$selector).length > 1) {
        this.carousel = stackedCarousel($('.featured-events-carousel__content', this.$selector), this.carouselOptions, this.maxItems, true);
      }
    }
    else {
      if($('.featured-events-carousel__content [data-carousel] > *', this.$selector).length > 1) {
        this.carousel = initCarousel($('.featured-events-carousel__content [data-carousel]', this.$selector), this.carouselOptions);
      }
    }

    if($('.featured-events-carousel__background', this.$selector).length) {
      if(data.length == 0) {
        const dotTemplateImages = dot.template(this.$imageTemplate)(data);
        $('.featured-events-carousel__background > div', this.$selector).html(dotTemplateImages);
      }
    }

    setTimeout(() => {
      $('.featured-events-carousel__content', this.$selector).addClass('content-loaded');
      $('.featured-events-carousel__content', this.$selector).removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);
  }
}

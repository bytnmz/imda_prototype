'use strict';

export default class HomeTabs {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.state = {
      currentNav: 0,
      tabCount: $('.home-tabs__nav a').length,
      inTransition: false
    }

    if(selfInit) this.init();
  }

  init() {
    $('.home-tabs__nav', this.$selector).addClass('nav-init');
    $('.home-tabs__nav a', this.$selector).first().addClass('active');
    this.state.currentNav = 0;

    $('.home-tabs__nav a').map((i,ele) => {
      $(ele).on('click', (e) => {
        e.preventDefault();
        if(!$(ele).hasClass('active')) {
          const target = $(ele).attr('href');
          const shapesRef = $(ele).data('shapesref');

          this.state.currentNav = i;

          this.changeTab(i, $(ele), target, shapesRef);
        }
      });
    });
  }

  changeTab(index, $navItem, target, shapesRef) {
    this.slideHandle(index);

    if ($(window).width() < 1024) {
      $('.home-tabs__nav .active', this.$selector).fadeOut(300, () => {
        $navItem.fadeIn(300);
      });
    }
    
    $('.home-tabs__nav .active', this.$selector).removeClass('active');
    $navItem.addClass('active');

    $('.home-tabs__content .active', this.$selector).addClass('transition');
    $('.home-tabs__content .active', this.$selector).removeClass('fadein');
    $('.home-tabs__content .active', this.$selector).attr('aria-hidden', true);
    $('.home-tabs__shapes .active', this.$selector).addClass('transition');
    $('.home-tabs__shapes .active', this.$selector).removeClass('fadein');

    setTimeout(() => {
      $('.home-tabs__content .active', this.$selector).removeClass('transition active');
      $('.home-tabs__shapes .active', this.$selector).removeClass('transition active');
      $(`${target}`, this.$selector).addClass('active');
      $(`${target}`, this.$selector).addClass('transition fadein');
      $(`${target}`, this.$selector).attr('aria-hidden', false);
      $(`${shapesRef}`, this.$selector).addClass('active');
      $(`${shapesRef}`, this.$selector).addClass('transition fadein');
    }, 300);
    
    setTimeout(() => {
      this.state.inTransition = false;
    }, 600)
  }

  slideHandle(index) {
    const unit = 100 / $('.home-tabs__nav a', this.$selector).length;

    $('.home-tabs__nav .ui-handle', this.$selector).css('left', `${index * unit}%`);

    if(index == 1) {
      $('.home-tabs__nav .ui-handle', this.$selector).css('scale', `0.9`);
    } else {
      $('.home-tabs__nav .ui-handle', this.$selector).css('scale', `1.0`);
    }
  }
}

'use strict';

import $ from 'jquery';
import 'slick-carousel';

export default class AnnouncementSection {
  constructor() {
    $('.announcement-carousel').slick({
      autoplay: true,
      autoplaySpeed: 4500,
      speed: 600,
      accessibility: true,
      mobileFirst: true,
      infinite: true,
      dots: true,
      prevArrow: $('.announcement-prev-arrow'),
      nextArrow: $('.announcement-next-arrow'),
      dotsClass: "announcement-slider-dots",
      cssEase: 'linear',
    })
  }
}

'use strict';

import $ from 'jquery';

export default class HomeStats {
  constructor() {
    let $homeStats = $('.home-stats');
    let $figure = $('.home-stats__figure', $homeStats);

    let number = parseInt($figure.data('number'));

    this.$figure = $figure;

    this.$figure.find('.number').text(0);

    $(window).on('scroll.animateNumber', e => {
      // when the screen reveal more than 1/3 of the home stats
      if ($(window).scrollTop() + $(window).height() >= $homeStats.offset().top + $homeStats.outerHeight() / 3) {
        this.animateNumber(number);
        $(window).off('scroll.animateNumber');
      }
    }).trigger('scroll.animateNumber');
  }

  animateNumber(number) {
    let duration = 1500;
    let stepDuration = 50;
    
    let steps = duration / stepDuration;
    let stepSize = parseInt(number / steps);

    let x = 0;
    let xText = x;
    let animateInterval = setInterval(() => {
      xText = this._convertString(x);
      this.$figure.find('.number').text(xText);
      x += stepSize;

      if (x >= number) {
        x = number;
        xText = this._convertString(x);

        this.$figure.find('.number').text(xText);
        clearInterval(animateInterval);
        return;
      }
    }, stepDuration);
  }

  _convertString(number) {
    // let x = number;
    // let numberString = x;

    // if (x >= 1000) {
    //   let x1 = parseInt(x / 1000);
    //   let x2 = (x % 1000).toString();
    //   if (x2.length < 3) {
    //     switch (x2.length) {
    //       case 1:
    //         x2 = '00' + x2;
    //         break;
    //       case 2:
    //         x2 = '0' + x2;
    //         break;
    //       default:
    //         x2 = x2;
    //     }
    //   }
    //   numberString = x1 + ',' + x2;
    // }

    return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }
}

'use strict';

import HomeStats from '../home-stats';

describe('HomeStats View', function() {

  beforeEach(() => {
    this.homeStats = new HomeStats();
  });

  it('Should run a few assertions', () => {
    expect(this.homeStats).toBeDefined();
  });

});

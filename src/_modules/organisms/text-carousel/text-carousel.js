'use strict';

import 'slick-carousel';
import { initCarousel } from '../../../_scripts/helpers';

export default class TextCarousel {
  constructor($selector, selfInit = true, config) {
    this.$selector = $selector;
    this.breakpoints = config.breakpoints;
    this.carouselOptions = {
      rows: 1,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: true,
      dots: true,
      arrows: true,
      customPaging: (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      }
    };

    if(selfInit) this.init();
  }

  init() {
    const carousel = initCarousel($('[data-carousel]', this.$selector), this.carouselOptions);

    window.emitter.on('accordionOpened', (data) => {
      carousel.setPosition();
    });
  }
}

'use strict';

import $ from 'jquery';
import dot from 'dot';
import ListingBase from '../../../_scripts/listingBase.js';
import FiltersGroup from '../../molecules/filters-group/filters-group';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';
import Cookies from 'js-cookie';

export default class Newsroom extends ListingBase {
  constructor() {
    let $newsroom = $('.newsroom');
    let $content = $('.newsroom__content', $newsroom);
    let $filtersGroup = $('.filters-group', $newsroom);
    let $pagination = $('.pagination', $newsroom);
    let $list = $('.newsroom__list', $newsroom);

    let dataModel = {
      support: '',
      type: '',
      year: '',
      page: 1
    };

    let parametersModel = {
      support: '',
      type: '',
      year: '',
      page: 1
    };

    super($pagination, dataModel, parametersModel);

    let filters = new FiltersGroup($filtersGroup);
    // Setup basic
    this.$template = $('#newsroomTemplate').html();
    this.$content = $content;
    this.$list = $list;
    this.endpoint = $newsroom.data('endpoint');
    this.$loadingIcon = new LoadingIcon($('.loading-icon', $newsroom));

    // Define fields
    let $support = $('#support', $filtersGroup);
    let $type = $('#type', $filtersGroup);
    let $year = $('#year', $filtersGroup);

    // get all parameters value
    let queries = this.getUrlQueries();

    let page = 1;
    if (queries['page']) {
      page = encodeURIComponent(queries['page']);
    }

    if (queries['year']) {
      $year.val(queries['year']);
      $year.trigger('change.customSelect');
    }

    if (queries['type']) {
      $type.val(queries['type']);
      $type.trigger('change.customSelect');
    }

    if ($support) {
      if (queries['support']) {
        $support.val(queries['support']);
        $support.trigger('change.customSelect');
      } else {
        //Check if there's cookie value for IMDA profile, if profile is present, pre-select the profile filter
        if (Cookies.get('imda_profile')) {
          let profile = Cookies.get('imda_profile');
          if (profile === 'profile0') {
            profile = 'profile1';
          }
          let matchedProfileOption = $('option[data-profile="' + profile + '"]');
          
          if (matchedProfileOption) {
            $support.val(matchedProfileOption.val());
            $support.trigger('change.customSelect');
          }
        }
      }
    }

    // Setup Data Structure
    this.data.support = $support ? $support.val() : '';
    this.data.type = $type.val();
    this.data.year = $year.val();
    this.data.page = page;

    this.parameters.support = this.data.support;
    this.parameters.type = this.data.type;
    this.parameters.year = this.data.year;
    this.parameters.page = this.data.page;

    this._updateYearFilter();

    window.onpopstate = (e) => {
      this.data.support = e.state.support;
      this.data.type = e.state.type;
      this.data.year = e.state.year;
      this.data.page = e.state.page;

      this.parameters.support = this.data.support;
      this.parameters.type = this.data.type;
      this.parameters.year = this.data.year;
      this.parameters.page = this.data.page;

      if ($support) {
        $support.val(this.data.support);
        $support.trigger('change.customSelect');
      }

      $type.val(this.data.type);
      $type.trigger('change.customSelect');

      $year.val(this.data.year);
      $year.trigger('change.customSelect');

      this._updateData();
    };

    // Add listener
    if ($support) {
      $support.on('change', e => {
        this.data.support = $support.val();
        this.parameters.support = this.data.support;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
      });
    }

    $type.on('change', e => {
      this.data.type = $type.val();
      this.parameters.type = this.data.type;
      this.data.page = 1;
      this.parameters.page = 1;

      this._updateYearFilter();
      
      this._updateData();
      this.updateURL();
    });

    $year.on('change', e => {
      let $value = $year.val();
      if ($value.startsWith('/')) {
        window.location = $value;
      } else {
        this.data.year = $year.val();
        this.parameters.year = this.data.year;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL()
      }      
    });

    // Make call upon page load
    this._updateData();
    this.updateURL(true);
  }

  _updateData() {
    this.$content.addClass('content-updating');
    this.$loadingIcon.playAnimation();
    this._pushDataLayer();

    let scrollPos = $(window).scrollTop();
    if (scrollPos >= this.$content.offset().top) {
      $('html, body').animate({
        scrollTop: this.$content.offset().top - 140
      });
    }

    let callback = (res) => {
      this._renderTemplate(res);
      if (res.items.length == 0) {
        this.$pagination.hide();
      } else {
        this.$pagination.show();
        this._renderPagination(res.totalpages);
      }
    };

    this.updateData({
      endpoint: this.endpoint,
      data: this.data,
      callback: callback
    });
  }

  _renderTemplate(data) {
    let dotTemplate = dot.template(this.$template)(data.items);

    this.$list.html(dotTemplate);

    setTimeout(() => {
      this.$content.addClass('content-loaded');
      this.$content.removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);
  }

  _renderPagination(totalpages) {
    let callback = () => {
      this.$pagination.find('a').map((i, ele) => {
        let $this = $(ele);

        $this.on('click', e => {
          e.preventDefault();
          this.data.page = $this.data('page');
          this.parameters.page = this.data.page;

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }

  _updateYearFilter() {
    let $filtersGroup = $('.newsroom .filters-group');
    let $year = $('#year', $filtersGroup);
    let $type = $('#type', $filtersGroup);
    
    if($type.val() != 'all') {
      let $yearParent = $year.parent();
      /* Show / Hide respective Year filter based on selected Type filter */
      if($('[data-categories]', $yearParent)) {
        $('[data-categories]', $yearParent).hide();
        $('[data-categories]', $yearParent).map((i, ele) => {
          let relatedYears = $(ele).data('categories').split('|');
          for (let j = 0; j < relatedYears.length; j++) {
            if(relatedYears[j] == this.data.type) {
              $(ele).show();
              break;
            }
          }  
        });
      }
    } else {
      $('[data-categories]').show();
    }

    if(!$('option[value="'+ this.data.year +'"]', $year).filter(function(){ return $(this).css('display') != 'none';}).length) {
      $year.val($year.find('option').filter(function(){ return $(this).css('display') != 'none';}).first().val());
      $year.trigger('change.customSelect');
      this.data.year = $year.val();
      this.parameters.year = this.data.year;
    }
  }

  _pushDataLayer() {
    let filterValues = `[${this.parameters.support === 'all' || this.parameters.support === 'All' ? 'profileType all|' : this.parameters.support ? this.parameters.support + '|' : ''}${this.parameters.type === 'all' || this.parameters.type === 'All' ? 'pressreleaseType all|' :this.parameters.type ? this.parameters.type + '|' : ''}${this.parameters.year === 'all' || this.parameters.year === 'All' ? 'contentDate all|' : this.parameters.year ? this.parameters.year : ''}]`;
    let dataLayerModel = {
      'event': 'historyChange-v2',
      'profileType': `[${this.parameters.support ? this.parameters.support : ''}]`,
      'pressreleaseType': `[${this.parameters.type ? this.parameters.type : ''}]`,
      'contentDate': `[${this.parameters.year ? this.parameters.year : ''}]`,
      'filters': filterValues ? filterValues : ''
    }
    window.dataLayer = window.dataLayer || [];
    dataLayer.push(
      dataLayerModel
    );
  }
}
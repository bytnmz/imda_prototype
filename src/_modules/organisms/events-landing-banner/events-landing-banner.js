'use strict';

import dot from 'dot';
import Cookies from 'js-cookie';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';
import { stackedCarousel } from '../../../_scripts/helpers';

export default class EventsLandingBanner {
  constructor($selector, selfInit = true, config) {
    this.breakpoints = config.breakpoints;
    this.$selector = $selector;
    this.contentData = JSON.parse($('#eventBannerData', this.$selector).html());
    this.$template = $('#eventsBannerItemTemplate', this.$selector).html();
    this.maxItems = this.$selector.data('maxitems') ? parseInt(this.$selector.data('maxitems')) : $('[data-carousel] article').length;

    this.carousel = null;
    this.carouselOptions = {
      rows: 1,
      fade: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: true,
      dots: true,
      arrows: true,
      customPaging: (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      }
    };

    this.$loadingIcon = new LoadingIcon($('.loading-icon', this.$selector));

    this.currentProfile = Cookies.get('imda_profile') ? Cookies.get('imda_profile') : 'profile0';

    if(selfInit) this.init();
  }

  init() {
    $('.events-landing-banner__content', this.$selector).addClass('content-updating');
    this.$loadingIcon.playAnimation();

    this.renderCarousel(this.currentProfile);

    window.emitter.on('profileChanged', (data) => {
      $('.events-landing-banner__content', this.$selector).addClass('content-updating');
      this.renderCarousel(data);
    });
  }

  renderCarousel(profile) {
    if(this.carousel) {
      this.carousel.unslick();
      this.carousel = null; 
      $(`[data-carousel-pager]`, this.$selector).text('');
    }
    
    this.maxItems = this.contentData[profile].length;
    const dotTemplateContent = `<div id="bannerContent" data-carousel>${dot.template(this.$template)(this.contentData[profile])}</div>`;

    $('.events-landing-banner__content [data-carousel]', this.$selector).remove();
    $('.events-landing-banner__content .loading-icon', this.$selector).after(dotTemplateContent);

    this.carousel = stackedCarousel($('.events-landing-banner__content', this.$selector), this.carouselOptions, this.maxItems, true);

    setTimeout(() => {
      $('.events-landing-banner__content', this.$selector).addClass('content-loaded');
      $('.events-landing-banner__content', this.$selector).removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);
  }
}

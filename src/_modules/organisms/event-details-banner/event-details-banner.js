'use strict';

export default class EventDetailsBanner {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.startTime = $('.event-details-banner__starttime', this.$selector).data('starttime');

    if(selfInit) this.init();
  }

  init() {
    const targetDate = new Date(this.startTime).getTime();

    let interval = setInterval(function() {

      // Get today's date and time
      const now = new Date().getTime();
    
      // Find the distance between now and the count down date
      const distance = targetDate - now;
    
      // Time calculations for days, hours, minutes and seconds
      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
      // Display the result in the element with id="demo"
      (days > 9) ? $('.day .counter', this.$selector).text(days) : $('.day .counter', this.$selector).text(`0${days}`);
      (hours > 9) ? $('.hour .counter', this.$selector).text(hours) : $('.hour .counter', this.$selector).text(`0${hours}`);
      (minutes > 9) ? $('.min .counter', this.$selector).text(minutes) : $('.min .counter', this.$selector).text(`0${minutes}`);
      (seconds > 9) ? $('.sec .counter', this.$selector).text(seconds) : $('.sec .counter', this.$selector).text(`0${seconds}`);
    
      // If the count down is finished, write some text
      if (distance < 0) {
        $('.event-details-banner__starttime', this.$selector).hide();
        
        clearInterval(interval);
        $('.day .counter', this.$selector).text('00');
        $('.hour .counter', this.$selector).text('00');
        $('.min .counter', this.$selector).text('00');
        $('.sec .counter', this.$selector).text('00');
      }
    }, 1000);
  }
}

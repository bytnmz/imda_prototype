'use strict';

import $ from 'jquery';
import Masonry from 'masonry-layout';
import imagesLoaded from 'imagesloaded';

export default class SocialFeed {
  constructor() {
    let $socialFeed = $('.social-feed');
    let $content = $('.social-feed__content', $socialFeed);

    let imgLoad = new imagesLoaded('.social-feed__content', () => {

      let msnry = new Masonry('.social-feed__content', {
        percentPosition: true,
        horizontalOrder: true
      });
    });
  }
}

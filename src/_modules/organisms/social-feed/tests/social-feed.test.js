'use strict';

import SocialFeed from '../social-feed';

describe('SocialFeed View', function() {

  beforeEach(() => {
    this.socialFeed = new SocialFeed();
  });

  it('Should run a few assertions', () => {
    expect(this.socialFeed).toBeDefined();
  });

});

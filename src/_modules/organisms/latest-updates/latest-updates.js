'use strict';

import $ from 'jquery';
import enquire from 'enquire.js';
import dot from 'dot';
import Cookies from 'js-cookie';
import imagesLoaded from 'imagesloaded';
import CustomSelect from '../../custom-select/custom-select';
import { filterCookieItems, sortCookieItems } from '../../../_scripts/cookieHelpers';

export default class LatestUpdates {
  constructor(config) {
    this.config = config;

    let $latestUpdates = $('.latest-updates');
    let $selection = $('.latest-updates__selection', $latestUpdates);
    let $content = $('.latest-updates__content', $latestUpdates);
    let $topic = $('#latestUpdateTopics', $latestUpdates);

    this.selection = new CustomSelect($selection);

    this.$latestUpdates = $latestUpdates;
    this.$content = $content;
    this.$template = $('#latestUpdateTemplate').html();

    this.endpoint = $latestUpdates.data('endpoint');
    this.data = {
      topic: $topic.val()
    };

    this.topicString = $('option[value="' + $topic.val() + '"]', $topic).data('topic');

    this.updateData();

    $topic.on('change', e => {
      this.data.topic = $topic.val();
      this.topicString = $('option[value="' + this.data.topic + '"]', $topic).data('topic');
      this.updateCookies();
      this.updateData();
    });
  }

  updateCookies() {
    if (this.data.topic == 'all') return;

    let { pPoints } = this.config;

    let index = -1;
    window.cookieTopics.map((topic, i) => {
      if (topic.value == this.topicString) {
        index = i;

        return false;
      }
    });

    if (index > -1) {
      window.cookieTopics[index].point += pPoints.homeLatestUpdate;
    } else {
      window.cookieTopics.push({
        value: this.topicString,
        point: pPoints.homeLatestUpdate
      });
    }

    const sortedCookie = sortCookieItems(window.cookieTopics);
    const filteredCookie = filterCookieItems(sortedCookie);

    Cookies.set('IMDA_topics_unfiltered', JSON.stringify(window.cookieTopics));
    Cookies.set("IMDA_topics", JSON.stringify(filteredCookie));
  }

  initiateLayout() {
    this.$items = $('.latest-updates__item', this.$latestUpdates);

    let { breakpoints } = this.config;

    let resizeTimeout;
    // enquire.unregister(`screen and (min-width:${ breakpoints.desktop }px)`);
    enquire.register(`screen and (min-width:${ breakpoints.desktop }px)`, {
      match: () => {
        /* store original height of box */
        $(window).on('resize.latestUpdate', e => {
          clearTimeout(resizeTimeout);

          resizeTimeout = setTimeout(() => {
            this.adjustCardHeight();
          }, 100);
        });

        imagesLoaded('.latest-updates', () => {
          $(window).trigger('resize.latestUpdate');
        });
      },
      unmatch: () => {
        clearTimeout(resizeTimeout);
        $(window).off('resize.latestUpdate');
        this.removeCardHeight();
      }
    });

    // enquire.unregister(`screen and (min-width:${ breakpoints.tablet }px) and (max-width: ${ breakpoints.desktop -1}px)`);
    enquire.register(`screen and (min-width:${ breakpoints.tablet }px) and (max-width: ${ breakpoints.desktop -1}px)`, {
      match: () => {
        this.$items.matchHeight();
      },
      unmatch: () => {
        this.$items.matchHeight({ remove: true});
      }
    });
  }

  adjustCardHeight() {

    this.originalHeight = this.$items.map((i, item) => {
      let $item = $(item);
      $item.css({
        height: 'auto'
      });

      return $item.outerHeight();
    });

    if (this.originalHeight[1] > this.originalHeight[2]) {
      // 2nd box > 3rd box
      this.$items.eq(2).css({
        height: this.originalHeight[1]
      });
      this.originalHeight[2] = this.originalHeight[1];
    } else {
      this.$items.eq(1).css({
        height: this.originalHeight[2]
      });
      this.originalHeight[1] = this.originalHeight[2];
    }

    if (this.originalHeight[0] > (this.originalHeight[1] + this.originalHeight[2] + 30)) {
      // 1st box > 2nd + 3rd box height + padding
      this.$items.eq(1).css({
        height: (this.originalHeight[0] - 30) / 2
      });
      this.$items.eq(2).css({
        height: (this.originalHeight[0] - 30) / 2
      });
    } else {
      this.$items.eq(0).css({
        height: this.originalHeight[1] + this.originalHeight[2] + 30
      });
    }

    $('.latest-updates__item:eq(3), .latest-updates__item:eq(4)').matchHeight();

  }

  removeCardHeight() {
    $('.latest-updates__item').css({
      height: 'auto'
    });
  }

  updateData() {
    $.ajax({
      url: this.endpoint,
      data: JSON.stringify(this.data),
      method: 'post',
      contentType: 'application/json',
      success: (res) => {
        this.renderTemplate(res);
      },
      error: (err) => {
        console.log(err);
      }
    });
  }

  renderTemplate(data) {
    let dotTemplate = dot.template(this.$template)(data);

    this.$content.html(dotTemplate);

    this.initiateLayout();
  }
}

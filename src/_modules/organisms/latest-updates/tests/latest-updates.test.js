'use strict';

import LatestUpdates from '../latest-updates';

describe('LatestUpdates View', function() {

  beforeEach(() => {
    this.latestUpdates = new LatestUpdates();
  });

  it('Should run a few assertions', () => {
    expect(this.latestUpdates).toBeDefined();
  });

});

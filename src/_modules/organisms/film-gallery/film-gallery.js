'use strict';

import '@fancyapps/fancybox';

export default class FilmGallery {
  constructor() {

    let $gallery = $('.film-gallery__list'),
        $galleryItem = $('.film-gallery__item', $gallery);

    if ($galleryItem.length > 2) {
      $('.film-gallery__list').slick({
        rows: 2,
        slidesPerRow: 2,
        mobileFirst:true,
        prevArrow: '<button type="button" class="slick-prev slick-arrow"><span class="sr-only">Previous</span><span class="icon icon-chevron-left"></span></button>',
        nextArrow: '<button type="button" class="slick-next slick-arrow"><span class="sr-only">Next</span><span class="icon icon-chevron-right"></span></button>',
        responsive: [
          {
            breakpoint: 767,
            settings: {
              rows: 2,
              slidesPerRow: 1
            }
          }
        ]
      }).on('init', function(){
        window.emiter.emit('gallery-init');
      });
    } else if ($galleryItem.length < 2) {
      $gallery.addClass('is-single');
    }

    let fancyBoxSetting = {
      toolbar: false
    };

    $('.fancybox').fancybox(fancyBoxSetting);

    window.emitter.on('gallery-init', function () {
      $('.film-gallery__item.fancybox').fancybox(fancyBoxSetting);
    });
  }
}

'use strict';

import RelatedPosts from '../related-posts';

describe('RelatedPosts View', function() {

  beforeEach(() => {
    this.relatedPosts = new RelatedPosts();
  });

  it('Should run a few assertions', () => {
    expect(this.relatedPosts).toBeDefined();
  });

});

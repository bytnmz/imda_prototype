'use strict';

import { initCarousel } from "../../../_scripts/helpers";

export default class ArticleGalleryCarousel {
  constructor($selector, selfInit = true, config) {
    this.$selector = $selector;
    this.breakpoints = config.breakpoints;
    // this.imageOptions = {
    //   "rows": 1,
    //   "speed": 1000,
    //   "slidesToShow": 5,
    //   "slidesToScroll": 1,
    //   "autoplay": false,
    //   "infinite": true,
    //   "dots": false,
    //   "arrows": false,
    //   "fade": false,
    //   "responsive": [
    //     {
    //       "breakpoint": 1024,
    //       "settings": {
    //         "slidesToShow": 4
    //       }
    //     },
    //     {
    //       "breakpoint": 768,
    //       "settings": {
    //         "slidesToShow": 3
    //       }
    //     },
    //     {
    //       "breakpoint": 540,
    //       "settings": {
    //         "slidesToShow": 2
    //       }
    //     }
    //   ]
    // };
    this.textOptions = {
      "rows": 1,
      "speed": 500,
      "slidesToShow": 1,
      "slidesToScroll": 1,
      "autoplay": false,
      "infinite": true,
      "dots": true,
      "arrows": true,
      "fade": true,
      "customPaging": (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      }
    };

    if(selfInit) this.init();
  }

  init() {
    // const navInit = () => {
    //   $('.article-gallery-carousel__images [data-carousel] .slick-slide', this.$selector).map((i,ele) => {
    //     $(ele).off('click');
    //   });

    //   $('.article-gallery-carousel__images [data-carousel] .slick-slide', this.$selector).map((i,ele) => {
    //     $(ele).on('click', (e) => {
    //       e.preventDefault();

    //       if(!$(ele).hasClass('slick-current')) {
    //         $('.article-gallery-carousel__images [data-carousel]', this.$selector).slick('slickGoTo', $(ele).data('slick-index'));
    //       }
    //     });
    //   });
    // }

    // initCarousel($('.article-gallery-carousel__images [data-carousel]', this.$selector), this.imageOptions, true, null, null, null, navInit);

    initCarousel($('.article-gallery-carousel__texts [data-carousel]', this.$selector), this.textOptions);
  }
}

'use strict';

import PageBanner from '../page-banner';

describe('PageBanner View', function() {

  beforeEach(() => {
    this.pageBanner = new PageBanner();
  });

  it('Should run a few assertions', () => {
    expect(this.pageBanner).toBeDefined();
  });

});

'use strict';

import readTimeEstimate from 'read-time-estimate';

export default class PageBanner {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    
    if(selfInit) this.init();
  }

  init() {
    if($('.publication-detail .read-time').length) {
      this.initReadTime();
    }

    if($(window).width() >= 1024) {
      if(document.readyState === 'ready' || document.readyState === 'complete') {
        setTimeout(() => {
          this.startAnimation();
        }, 300);
      }
      else {
        document.onreadystatechange = () => {
          if (document.readyState == 'complete') {
            setTimeout(() => {
              this.startAnimation();
            }, 300);
          }
        }
      }
    } else {
      this.startAnimation();
    }
  }

  startAnimation() {
    if($(window).width() >= 1024) {
      this.$selector.find('.page-banner__shapes').addClass('animation-building-start');
      
      setTimeout(() => {
        this.$selector.find('.page-banner__shapes').addClass('animation-building-end');
        this.$selector.find('.page-banner__line').addClass('animation-line-start');
      }, 300);
      
      
      setTimeout(() => {
        this.$selector.find('.page-banner__line').addClass('animation-line-end');
        this.$selector.find('.page-banner__notch').addClass('animation-step-1-start');
      }, 400);
      
      setTimeout(() => {
        this.$selector.find('.page-banner__notch').addClass('animation-step-1-end animation-step-2-start');
      }, 420);
      
      setTimeout(() => {
        this.$selector.find('.page-banner__notch').addClass('animation-step-2-end');
        this.$selector.find('.page-banner__shapes').addClass('animation-logo-start');
      }, 470);
      
      setTimeout(() => {
        this.$selector.find('.page-banner__shapes').addClass('animation-logo-end');
      }, 770);
    }
    else {
      this.$selector.find('.page-banner__line').addClass('animation-line-end');
      this.$selector.find('.page-banner__notch').addClass('animation-step-1-end animation-step-2-end');
      this.$selector.find('.page-banner__shapes').addClass('animation-logo-end');
    }
  }

  initReadTime() {
    const string = $('.detail-wrapper .detail-content').html();

    const {
      humanizedDuration, // 'less than a minute'
      duration, // 0.23272727272727273
      totalWords, // 9
      wordTime, // 0.03272727272727273
      totalImages, // 1
      imageTime, //  0.2
      otherLanguageTimeCharacters, // 6
      otherLanguageTime, // 0.012
    } = readTimeEstimate(string, 250, 10, 500, ['img', 'Image']);

    const readTime = Math.ceil(duration);

    $('.publication-detail .read-time').text(readTime);
  }
}

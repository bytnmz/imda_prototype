'use strict';

import FeaturedCampaign from '../featured-campaign';

describe('FeaturedCampaign View', function() {

  beforeEach(() => {
    this.featuredCampaign = new FeaturedCampaign();
  });

  it('Should run a few assertions', () => {
    expect(this.featuredCampaign).toBeDefined();
  });

});

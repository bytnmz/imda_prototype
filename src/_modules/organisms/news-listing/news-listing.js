'use strict';

import $ from 'jquery';
import dot from 'dot';
import Cookies from 'js-cookie';
import ListingBase from '../../../_scripts/listingBase.js';
import ListingFilter from '../../molecules/listing-filter/listing-filter';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';

export default class NewsListing extends ListingBase {
  constructor() {
    let $newsListing = $('.news-listing');
    let $filters = $('.news-listing__filters', $newsListing);
    let $content = $('.news-listing__content', $newsListing);
    let $pagination = $('.news-listing__pagination', $newsListing);
    let $list = $('.news-listing__list', $newsListing);

    let $searchIconTop = $('#icon-search-top');
    let $searchInput = $('.listing-filter__search');

    let dataModel = {
      keyword: '',
      industry: '',
      support: '',
      topic: '',
      type: '',
      year: '',
      page: 1
    };

    let parametersModel = {
      keyword: '',
      industry: '',
      support: '',
      topic: '',
      type: '',
      year: '',
      page: 1
    };

    super($pagination, dataModel, parametersModel);

    new ListingFilter($filters);

    // Setup basic
    this.$template = $('#newsListingTemplate').html();
    this.$content = $content;
    this.$list = $list;
    this.endpoint = $newsListing.data('endpoint');
    this.$loadingIcon = new LoadingIcon($('.loading-icon', $newsListing));

    // Define fields
    let $search = $('#keyword', $filters);
    let $industry = $('#industry', $filters);
    let $support = $('#support', $filters);
    let $year = $('#year', $filters);
    let $topic = $('#topic', $filters);
    let $type = $('#type', $filters);

    // get all parameters value
    let queries = this.getUrlQueries();

    let page = 1;
    if (queries['page']) {
      page = decodeURIComponent(queries['page']);
    }

    if(queries['keyword']) {
      $search.val(decodeURIComponent(queries['keyword']));
    }

    if (queries['industry']) {
      $industry.val(queries['industry']);
      $industry.trigger('change.customSelect');
    }

    if (queries['support']) {
      $support.val(queries['support']);
      $support.trigger('change.customSelect');

      const profileLabel = $(`#support`).next().find(`[data-value="${queries['support']}"]`).find('button').text();
      $('.profile-label', this.$selector).text(profileLabel);
    } else {
      //Check if there's cookie value for IMDA profile, if profile is present, pre-select the profile filter
      if (Cookies.get('imda_profile')) {
        const profile = Cookies.get('imda_profile');
        const matchedProfileOption = $('option[data-profile="' + profile + '"]');
        
        if (matchedProfileOption) {
          $support.val(matchedProfileOption.val());
          $support.trigger('change.customSelect');
        }

        const profileLabel = $(`#support`).next().find(`[data-profile=${profile}]`).find('button').text();
        $('.profile-label', this.$selector).text(profileLabel);
      }
    }

    if (queries['year']) {
      $year.val(queries['year']);
      $year.trigger('change.customSelect');
    }

    if (queries['topic']) {
      $topic.val(queries['topic']);
      $topic.trigger('change.customSelect');
    }
    
    if (queries['type']) {
      $type.val(queries['type']);
      $type.trigger('change.customSelect');
    }

    // Setup Data Structure
    this.data.keyword = $search.val();
    this.data.industry = $industry.val() ? $industry.val() : 'all';
    this.data.support = $support.val() ? $support.val() : 'all';
    this.data.year = $year.val() ? $year.val() : 'all';
    this.data.topic = $topic.val() ? $topic.val() : 'all';
    this.data.type = $type.val() ? $type.val() : 'all';
    this.data.page = page;

    this.parameters.keyword = this.data.keyword;
    this.parameters.industry = this.data.industry;
    this.parameters.support = this.data.support;
    this.parameters.year = this.data.year;
    this.parameters.topic = this.data.topic;
    this.parameters.type = this.data.type;
    this.parameters.page = this.data.page;

    this.currentProfile = $('#support option:selected').data('profile');

    if(this.currentProfile === 'profile0') {
      //update UI to show "all" if no profile set 
      $industry.val(this.data['industry']);
      $industry.trigger('change.customSelect');

      $year.val(this.data['year']);
      $year.trigger('change.customSelect');
      
      $topic.val(this.data['topic']);
      $topic.trigger('change.customSelect');
      
      $type.val(this.data['type']);
      $type.trigger('change.customSelect');
    }

    this._updateData = () => {
      this.$content.addClass('content-updating');
      this.$loadingIcon.playAnimation();
      this._pushDataLayer();

      let scrollPos = $(window).scrollTop();
      if (scrollPos >= this.$content.offset().top) {
        $('html, body').animate({
          scrollTop: this.$content.offset().top - 230
        });
      }

      let callback = (res) => {
        this._renderTemplate(res);
        if (res.items.length == 0) {
          this.$pagination.hide();
        } else {
          this.$pagination.show();
          this._renderPagination(res.totalpages);
        }
      };

      this.updateData({
        endpoint: this.endpoint,
        data: this.data,
        callback: callback
      });

      this.updateUI($industry);
    };

    window.onpopstate = (e) => {
      this.data.keyword = e.state.keyword;
      this.data.industry = e.state.industry;
      this.data.support = e.state.support;
      this.data.year = e.state.year;
      this.data.topic = e.state.topic;
      this.data.type = e.state.type;
      this.data.page = e.state.page;

      this.parameters.keyword = this.data.keyword;
      this.parameters.industry = this.data.industry;
      this.parameters.support = this.data.support;
      this.parameters.year = this.data.year;
      this.parameters.topic = this.data.topic;
      this.parameters.type = this.data.type;
      this.parameters.page = this.data.page;

      $search.val(this.data.keyword);

      $industry.val(this.data.industry);
      $industry.trigger('change.customSelect');

      $support.val(this.data.support);
      $support.trigger('change.customSelect');
      const profileLabel = $(`#support`).next().find(`[data-value="${this.data.support}"]`).find('button').text();
      $('.profile-label', this.$selector).text(profileLabel);

      $year.val(this.data.year);
      $year.trigger('change.customSelect');

      $topic.val(this.data.topic);
      $topic.trigger('change.customSelect');

      $type.val(this.data.type);
      $type.trigger('change.customSelect')

      this._updateData();
    };

    // Add listener
    let checkingTimeout;
    $search.on('keypress', e => {
      if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        this.data.keyword = $search.val();
        this.parameters.keyword = this.data.keyword;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
      }
    });

    $search.parent().find('.btn-submit').on('click', e => {
      e.preventDefault();
      this.data.keyword = $search.val();
      this.parameters.keyword = this.data.keyword;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });

    $industry.on('change', e => {
      this.data.industry = $industry.val();
      this.parameters.industry = this.data.industry;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });
    
    $support.on('change', e => {
      this.data.support = $support.val();
      this.parameters.support = this.data.support;
      this.data.page = 1;
      this.parameters.page = 1;
      this.currentProfile = $(`#support option[value="${this.parameters.support}"]`).data('profile');

      const profileLabel = $(`#support`).next().find(`[data-value="${this.data.support}"]`).find('button').text();
      $('.profile-label', this.$selector).text(profileLabel);

      this.processFilterChanges($industry, $type, $topic, $year);
      this._updateData();
      this.updateURL();
    });
    
    $year.on('change', e => {
      let $value = $year.val();
      
      if ($value.startsWith('/')) {
        window.location = $value;
      } else {
        this.data.year = $year.val();
        this.parameters.year = this.data.year;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
      }
    });
    
    $topic.on('change', e => {
      this.data.topic = $topic.val();
      this.parameters.topic = this.data.topic;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();

      // substitute for custom select change event
      $topic.next().find('.active').removeClass('active');
      $topic.next().find(`[data-value="${ this.data.topic }"]`).addClass('active');
      $topic.next().find('.label').text($topic.next().find(`[data-profilelist="${this.currentProfile}"][data-value="${this.data.topic}"]`).find('button').text());
    });
    
    $type.on('change', e => {
      this.data.type = $type.val();
      this.parameters.type = this.data.type;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();

      // substitute for custom select change event
      $type.next().find('.active').removeClass('active');
      $type.next().find(`[data-value="${ this.data.type }"]`).addClass('active');
      $type.next().find('.label').text($type.next().find(`[data-profilelist="${this.currentProfile}"][data-value="${this.data.type}"]`).find('button').text());
    });
    
    $searchIconTop.on('click', e => {
      $searchIconTop.toggleClass('icon-close');
      $searchIconTop.toggleClass('icon-search');
      $searchInput.toggleClass('show');
    });

    // Make call upon page load
    this._updateData();
    this.updateURL(true);
  }

  updateUI($industry) {
    if(this.currentProfile !== "profile1") {
      $industry.val("all");
      this.data.industry = $industry.val();
      this.parameters.industry = this.data.industry;
      $industry.trigger('change.customSelect');
      
      $('[data-filtername="industry"]').addClass('hide');
    } else {
      $('[data-filtername="industry"]').removeClass('hide');
    }
  
    $(`.news-listing__filters [data-profilelist]`).addClass('hide');
    $(`.news-listing__filters [data-profilelist="${this.currentProfile}"]`).removeClass('hide');
  }

  processFilterChanges($industry, $type, $topic, $year) {
    this.updateUI($industry);

    // substitute for custom select change event for $type
    this.substituteEvent($type, 'type');
    
    // substitute for custom select change event for $topic
    this.substituteEvent($topic, 'topic');

    // substitute for custom select change event for $year
    this.substituteEventValueAll($year, 'year');
  }

  substituteEvent($target, targetKey) {
    //set target value
    $target.val($target.next().find(`[data-profilelist="${this.currentProfile}"]`).data('value'));
    this.data[targetKey] = $target.val();
    this.parameters[targetKey] = this.data[targetKey];

    // substitute for custom select change event
    $target.next().find('.active').removeClass('active');
    $target.next().find(`[data-value="${ this.data[targetKey] }"]`).addClass('active');

    $target.next().find('.label').text($target.next().find(`[data-profilelist="${this.currentProfile}"]`).first().find('button').text().length ? $target.next().find(`[data-profilelist="${this.currentProfile}"]`).first().find('button').text() : $target.next().find(`li[data-value="all"]`).first().find('button').text());
  }

  substituteEventValueAll($target, targetKey) {
    //set target value
    $target.val("all");
    this.data[targetKey] = $target.val();
    this.parameters[targetKey] = this.data[targetKey];

    // substitute for custom select change event
    $target.next().find('.active').removeClass('active');
    $target.next().find(`[data-value="all"]`).addClass('active');

    $target.next().find('.label').text($target.next().find(`[data-value="all"]`).find('button').text());
  }

  _renderTemplate(data) {
    let dotTemplate = dot.template(this.$template)(data.items);

    this.$list.html(dotTemplate);

    setTimeout(() => {
      this.$content.addClass('content-loaded');
      this.$content.removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);
  }

  _renderPagination(totalpages) {
    let callback = () => {
      this.$pagination.find('a').map((i, ele) => {
        let $this = $(ele);

        $this.on('click', e => {
          e.preventDefault();
          this.data.page = $this.data('page');
          this.parameters.page = this.data.page;

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }

  _pushDataLayer() {
    let dataLayerModel = {};
    let filterValues = '';
    let dataProfile = this.parameters.support;

    switch (dataProfile) {
      case 'All': 
      case 'Businesses':
      case 'all':
      case 'businesses':
        filterValues = `[${this.parameters.keyword ? this.parameters.keyword + '|' : ''}${this.parameters.industry ? this.parameters.industry + '|' : ''}${this.parameters.support ? this.parameters.support + '|' : ''}${this.parameters.year ? this.parameters.year + '|' : ''}${this.parameters.topic ? this.parameters.topic + '|' : ''}${this.parameters.type ? this.parameters.type : ''}]`;
        dataLayerModel = {
          'event': 'historyChange-v2',
          'keyword': `[${this.parameters.keyword ? this.parameters.keyword : ''}]`,
          'industryType': `[${this.parameters.industry ? this.parameters.industry : ''}]`,
          'profileType': `[${this.parameters.support ? this.parameters.support : ''}]`,
          'contentDate': `[${this.parameters.year ? this.parameters.year : ''}]`,
          'contentTopic': `[${this.parameters.topic ? this.parameters.topic : ''}]`,
          'supportType': `[${this.parameters.type ? this.parameters.type : ''}]`,
          'filters': filterValues ? filterValues : ''
        }
        break;

      case 'Students-&-Professionals':
      case 'Public-&-Seniors':
      case 'students and professionals':
      case 'public and seniors':
        filterValues = `[${this.parameters.keyword ? this.parameters.keyword + '|' : ''}${this.parameters.support === 'all' || this.parameters.support === 'All' ? 'profileType all|' : this.parameters.support ? this.parameters.support + '|' : ''}${this.parameters.year === 'all' || this.parameters.year === 'All' ? 'contentDate all|' : this.parameters.year ? this.parameters.year + '|' : ''}${this.parameters.topic === 'all' || this.parameters.topic === 'All' ? 'contentTopic all|' : this.parameters.topic ? this.parameters.topic + '|' : ''}${this.parameters.type === 'all' || this.parameters.type === 'All' ? 'supportType all' : this.parameters.type ? this.parameters.type : ''}]`;
        dataLayerModel = {
          'event': 'historyChange-v2',
          'keyword': `[${this.parameters.keyword ? this.parameters.keyword : ''}]`,
          'profileType': `[${this.parameters.support ? this.parameters.support : ''}]`,
          'contentDate': `[${this.parameters.year ? this.parameters.year : ''}]`,
          'contentTopic': `[${this.parameters.topic ? this.parameters.topic : ''}]`,
          'supportType': `[${this.parameters.type ? this.parameters.type : ''}]`,
          'filters': filterValues ? filterValues : ''
        }
        break;
    
      default:
        break;
    }

    window.dataLayer = window.dataLayer || [];
    dataLayer.push(
      dataLayerModel
    );
  }
}

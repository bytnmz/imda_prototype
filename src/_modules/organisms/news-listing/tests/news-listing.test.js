'use strict';

import NewsListing from '../news-listing';

describe('NewsListing View', function() {

  beforeEach(() => {
    this.newsListing = new NewsListing();
  });

  it('Should run a few assertions', () => {
    expect(this.newsListing).toBeDefined();
  });

});

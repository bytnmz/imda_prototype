'use strict';

import LoadingIcon from "../../atoms/loading-icon/loading-icon";

export default class LoadingScreen {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    const loader = new LoadingIcon($('.loading-icon', this.$selector));

    loader.playAnimation();
  }
}

'use strict';

import DetailSection from '../detail-section';

describe('DetailSection View', function() {

  beforeEach(() => {
    this.detailSection = new DetailSection();
  });

  it('Should run a few assertions', () => {
    expect(this.detailSection).toBeDefined();
  });

});

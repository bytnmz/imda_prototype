'use strict';

import $ from 'jquery';
import dot from 'dot';
import ImagesLoaded from 'imagesloaded';
import enquire from 'enquire.js';
import ListingBase from '../../../_scripts/listingBase.js';
import ListingFilter from '../../molecules/listing-filter/listing-filter';
import Pagination from '../../molecules/pagination/pagination';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';

export default class StoryListing extends ListingBase {
  constructor(config) {
    let { breakpoints } = config;

    let dataModel = {
      keyword: '',
      industry: '',
      type: '',
      page: 1
    };

    let parametersModel = {
      keyword: '',
      industry: '',
      page: 1
    };

    super($pagination, dataModel, parametersModel);

    let $storyListing = $('.story-listing');
    let $filters = $('.story-listing__filters', $storyListing);
    let $content = $('.story-listing__content', $storyListing);
    let $pagination = $('.story-listing__pagination', $storyListing);
    let $list = $('.story-listing__list', $storyListing);

    this.$storyListing = $storyListing;
    this.$content = $content;
    this.$list = $list;
    this.originalHeight = [];
    this.$loadingIcon = new LoadingIcon($('.loading-icon', $storyListing));

    this.breakpoints = breakpoints;

    let filters = new ListingFilter($filters);
    this.pagination = new Pagination($pagination);
    this.$pagination = $pagination;

    // Setup basic
    this.$template = $('#storyListingTemplate').html();
    this.$content = $content;
    this.endpoint = $storyListing.data('endpoint');

    // Define fields
    let $search = $('#keyword', $filters);
    let $industry = $('#industry', $filters);
    let $type = $('#type', $filters);

    // get all parameters value
    let queries = this.getUrlQueries();

    let page = 1;
    if (queries['page']) {
      page = decodeURIComponent(queries['page']);
    }

    if(queries['keyword']) {
      $search.val(decodeURIComponent(queries['keyword']));
    }

    if (queries['industry']) {
      $industry.val(queries['industry']);
      $industry.trigger('change.customSelect');
    }

    // Setup Data Structure
    this.data.keyword = $search.val();
    this.data.industry = $industry.val();
    this.data.type = $type.val();
    this.data.page = page;

    this.parameters.keyword = this.data.keyword;
    this.parameters.industry = this.data.industry;
    this.parameters.page = this.data.page;

    this._updateData = () => {
      this.$content.addClass('content-updating');
      this.$loadingIcon.playAnimation();

      let scrollPos = $(window).scrollTop();
      if (scrollPos >= this.$content.offset().top) {
        $('html, body').animate({
          scrollTop: this.$content.offset().top - 230
        });
      }

      let callback = (res) => {
        this._renderTemplate(res);
        this._renderPagination(res.totalpages);
      };

      this.updateData({
        endpoint: this.endpoint,
        data: this.data,
        callback: callback
      });
    };

    window.onpopstate = (e) => {
      this.data.keyword = e.state.keyword;
      this.data.industry = e.state.industry;
      this.data.type = e.state.type;
      this.data.page = e.state.page;

      this.parameters.keyword = this.data.keyword;
      this.parameters.industry = this.data.industry;
      this.parameters.page = this.data.page;

      $search.val(this.data.keyword);

      $industry.val(this.data.industry);
      $industry.trigger('change.customSelect');

      this._updateData();
    }

    // Add listener
    $search.on('keypress', e => {
      if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        this.data.keyword = $search.val();
        this.parameters.keyword = this.data.keyword;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
      }
    });

    $search.parent().find('.btn-submit').on('click', e => {
      e.preventDefault();
      this.data.keyword = $search.val();
      this.parameters.keyword = this.data.keyword;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });

    $industry.on('change', e => {
      this.data.industry = $industry.val();
      this.parameters.industry = this.data.industry;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });

    // Make call upon page load
    this._updateData();
    this.updateURL(true);
  }

  _adjustLayout() {
    let padding = 30;

    this.originalHeight = this.$content.find('.story-listing__item').map((i, item) => {
      let $item = $(item);
      $item.css({
        height: 'auto'
      });

      return parseFloat($item.outerHeight().toFixed(2));
    });

    this.$content.find('.story-listing__item').eq(2).css({
      height: this.originalHeight[0] + this.originalHeight[1] + padding + 0.1,
      marginTop: -(this.originalHeight[0] + padding)
    });

    this.$content.find('.story-listing__item').eq(3).css({
      height: this.originalHeight[4] + this.originalHeight[5] + padding + 0.1
    });



  }

  _resetLayout() {
    $('.story-listing__item').css({
      height: 'auto',
      marginTop: 'auto'
    });
  }

  // _updateData() {
  //   this.$content.addClass('content-updating');

  //   let scrollPos = $(window).scrollTop();
  //   if (scrollPos >= this.$content.offset().top) {
  //     $('html, body').animate({
  //       scrollTop: this.$content.offset().top
  //     });
  //   }

  //   let callback = (res) => {
  //     this._renderTemplate(res);
  //     this._renderPagination(res.totalpages);
  //   };

  //   this.updateData({
  //     endpoint: this.endpoint,
  //     data: this.data,
  //     callback: callback
  //   });
  // }

  _renderTemplate(data) {
    let dotTemplate = dot.template(this.$template)(data.items);

    this.$list.find('.row').html(dotTemplate);

    enquire.unregister(`screen and (min-width:${ this.breakpoints.desktop}px)`);

    let resizeTimeout;
    enquire.register(`screen and (min-width:${ this.breakpoints.desktop }px)`, {
      match: () => {
        $(window).on('resize.storyListing', e => {
          clearTimeout(resizeTimeout);

          resizeTimeout = setTimeout(() => {
            this._adjustLayout();
          }, 150);
        });

        let imgLoad = new ImagesLoaded('.story-listing__list', () => {
          $(window).trigger('resize.storyListing');
        });
      },
      unmatch: () => {
        clearTimeout(resizeTimeout);
        $(window).off('resize.storyListing');
        this._resetLayout();
      }
    });

    setTimeout(() => {
      this.$content.addClass('content-loaded');
      this.$content.removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);

    $('.story-card').each(function(index, item){
      let companyHeight= $(item).find('.story-card__company').height();
      $(item).find('.story-card__content').css('padding-bottom', companyHeight + 60);
    });

  }

  _renderPagination(totalpages) {
    let callback = () => {
      this.$pagination.find('a').map((i, ele) => {
        let $this = $(ele);

        $this.on('click', e => {
          e.preventDefault();
          this.data.page = $this.data('page');
          this.parameters.page = this.data.page;

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }
}

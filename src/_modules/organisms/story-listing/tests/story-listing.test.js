'use strict';

import StoryListing from '../story-listing';

describe('StoryListing View', function() {

  beforeEach(() => {
    this.storyListing = new StoryListing();
  });

  it('Should run a few assertions', () => {
    expect(this.storyListing).toBeDefined();
  });

});

'use strict';

import InterestFilter from '../interest-filter';

describe('InterestFilter View', function() {

  beforeEach(() => {
    this.interestFilter = new InterestFilter();
  });

  it('Should run a few assertions', () => {
    expect(this.interestFilter).toBeDefined();
  });

});

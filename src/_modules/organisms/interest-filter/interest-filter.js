'use strict';

import $ from 'jquery';
import CustomSelect from '../../custom-select/custom-select';
import Cookies from 'js-cookie';
import { filterCookieItems, sortCookieItems } from '../../../_scripts/cookieHelpers';

export default class InterestFilter {
  constructor(config) {
    this.config = config;
    let $interestFilter = $('.interest-filter');
    let $support = $('#interestSupport', $interestFilter);
    // let $persona = $('#interestPersona', $interestFilter);
    // let $industry = $('#interestIndustry', $interestFilter);
    let $organisationCategory = $('#organisationCategory', $interestFilter);
    let $defaultCategory = $('#defaultCategory', $interestFilter);
    let $individualCategory = $('#individualCategory', $interestFilter);
    let $submitBtn = $('.btn-primary', $interestFilter);

    this.$interestFilter = $interestFilter;
    this.$organisationCategory = $organisationCategory;
    this.$individualCategory = $individualCategory;
    this.$defaultCategory = $defaultCategory;

    let $customSelects = $('.custom-select', $interestFilter);
    $customSelects.map((i, ele) => {
      let $this = $(ele);
      new CustomSelect($this);
    });

    $(window).on('scroll.interestFilter', e => {
      if ($(window).scrollTop() + $(window).outerHeight() >= $interestFilter.offset().top) {
        this.showFilters();
      }
    }).trigger('scroll.interestFilter');

    this.data = {
      support: $support.val(),
      // persona: $persona.val(),
      // industry: $industry.val(),
      category: '',
      categoryString: $('option[value="' + $organisationCategory.val() + '"]', $organisationCategory).data('category')
    };

    $support.on('change', e => {
      this.data.support = $support.val();
      let $this = $(e.currentTarget);
      if ($support.val() == '') {
        this.data.category = '';
        this.showOption($defaultCategory);
        $this.parent().siblings('.btn').addClass('inactive');
      } else if ($support.val() == 'individuals'){
        this.data.category = $individualCategory.val();
        this.showOption($individualCategory);
        $this.parent().siblings('.btn').removeClass('inactive');
      }else {
        this.data.category = $organisationCategory.val();
        this.showOption($organisationCategory);
        $this.parent().siblings('.btn').removeClass('inactive');
      }
    });

    // $industry.on('change', e => {
    //   this.data.industry = $industry.val();
    // });

    $organisationCategory.on('change', e => {
      this.data.category = $organisationCategory.val();

      let $selectedOption = $('option[value="' + this.data.category + '"]', $organisationCategory);
      this.data.categoryString = $selectedOption.data('category');

      $individualCategory.val(this.data.category).trigger('change.customSelect');
    });

    $individualCategory.on('change', e => {
      this.data.category = $individualCategory.val();

      let $selectedOption = $('option[value="' + this.data.category + '"]', $individualCategory);
      this.data.categoryString = $selectedOption.data('category');

      $organisationCategory.val(this.data.category).trigger('change.customSelect');
    });

    $submitBtn.on('click', e => {
      this.updateCookies();
      this.submitForm();
    });
  }

  showFilters() {
    this.$interestFilter.find('.form-group').addClass('visible');
  }

  showOption($option) {
    $('.interest-filter__option').removeClass('visible').removeClass('shown');
    $option.parent().addClass('shown');
    setTimeout(() => {
      $option.parent().addClass('visible');
    }, 0);
  }

  updateCookies() {
    let { pPoints } = this.config;

    let index = -1;
    window.cookieSupports.map((support, i) => {
      if (support.value == this.data.support) {
        index = i;

        return false;
      }
    });

    if (index > -1) {
      window.cookieSupports[index].point += pPoints.homeFilter;
    } else {
      window.cookieSupports.push({
        value: this.data.support,
        point: pPoints.homeFilter
      });
    }

    const sortedSupportCookie = sortCookieItems(window.cookieSupports);
    const filteredSupportCookie = filterCookieItems(sortedSupportCookie);

    Cookies.set('IMDA_supports_unfiltered', JSON.stringify(window.cookieSupports));
    Cookies.set("IMDA_supports", JSON.stringify(filteredSupportCookie));


    index = -1;
    window.cookieCategories.map((category, i) => {
      if (category.value == this.data.categoryString) {
        index = i;

        return false;
      }
    });

    if (index > -1) {
      window.cookieCategories[index].point += pPoints.homeFilter;
    } else {
      window.cookieCategories.push({
        value: this.data.categoryString,
        point: pPoints.homeFilter
      });
    }

    const sortedCategoriesCookie = sortCookieItems(window.cookieCategories);
    const filteredCategoriesCookie = filterCookieItems(sortedCategoriesCookie);

    Cookies.set('IMDA_categories_unfiltered', JSON.stringify(window.cookieCategories));
    Cookies.set("IMDA_categories", JSON.stringify(filteredCategoriesCookie));
  }

  submitForm() {

    let url = '';
    if (this.data.category !== '') {
      // if (this.data.category.indexOf('?') !== -1) {
      //   url = `${this.data.category}&support=${this.data.support}`;
      // } else {
      //   url = `${this.data.category}?&support=${this.data.support}`;
      // }
      url = `${this.data.category}`

      window.location.href = url;
    }

    // if (this.data.support === 'individuals') {
    //   url = `${this.data.category}&persona=${this.data.persona}&support=${this.data.support}`
    // } else {
    //   url = `${this.data.category}&industry=${this.data.industry}&support=${this.data.support}`;
    // }


  }
}

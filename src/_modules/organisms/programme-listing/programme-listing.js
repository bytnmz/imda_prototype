'use strict';

import $ from 'jquery';
import dot from 'dot';
import ListingBase from '../../../_scripts/listingBase.js';
import FiltersGroup from '../../molecules/filters-group/filters-group';
import Cookies from 'js-cookie';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';

export default class ProgrammeListing extends ListingBase {
  constructor() {
    /* define AJAX data model */
    let dataModel = {
      keyword: '',
      categories: '',
      support: '',
      orgTypes: '',
      industries: '',
      personas: '',
      page: 1
    };

    /* Define query parameters model */
    let parametersModel = {
      keyword: '',
      category: '',
      support: '',
      orgType: '',
      industry: '',
      persona: '',
      page: 1
    };

    /* Setup basic */
    let $programmeListing = $('.programme-listing');
    let $pagination = $('.pagination', $programmeListing);

    super($pagination, dataModel, parametersModel);

    this.$programmeListing = $programmeListing;

    let $filtersGroup = $('.filters-group', this.$programmeListing);
    this.filters = new FiltersGroup($filtersGroup);

    
    this.$template = $('#programmeListingTemplate').html();
    this.$content = $('.programme-listing__content', this.$programmeListing);
    this.$list = $('.programme-listing__list', this.$programmeListing);
    this.endpoint = this.$programmeListing.data('endpoint');
    this.$loadingIcon = new LoadingIcon($('.loading-icon', this.$programmeListing));

    /* Define fields */
    let $search = $('#keyword', $filtersGroup);
    let $categories = $('input[name="category"]', $filtersGroup);
    let $support = $('#support', $filtersGroup);
    let $orgTypes = $('input[name="orgType"]', $filtersGroup);
    let $industries = $('input[name="industry"]', $filtersGroup);
    let $personas = $('input[name="persona"]', $filtersGroup);

    this.$categories = $categories;
    this.$orgTypes = $orgTypes;
    this.$industries = $industries;
    this.$personas = $personas;

    /* get all parameters value */
    let queries = this.getUrlQueries();

    let categoryString = this.setupCheckboxes('category', queries, this.filters);
    let industryString = this.setupCheckboxes('industry', queries, this.filters);
    let orgTypeString = this.setupCheckboxes('orgType', queries, this.filters);
    let personaString = this.setupCheckboxes('persona', queries, this.filters);

    let page = 1;
    if (queries['page']) {
      page = decodeURIComponent(queries['page']);
    }

    if(queries['keyword']) {
      $search.val(decodeURIComponent(queries['keyword']));
    }

    if (queries['support']) {
      $support.val(queries['support']);
      $support.trigger('change.customSelect');
    } else {
      //Check if there's cookie value for IMDA profile, if profile is present, pre-select the profile filter
      if (Cookies.get('imda_profile')) {
        let profile = Cookies.get('imda_profile');
        if(profile === 'profile0') {
          profile = 'profile1';
        }
        let matchedProfileOption = $('option[data-profile="' + profile + '"]');
        
        if (matchedProfileOption) {
          $support.val(matchedProfileOption.val());
          $support.trigger('change.customSelect');
        }
      }
    }

    /* Populate Data */
    this.data.keyword = $search.val();
    this.data.categories = categoryString;
    this.data.support = $support.val();
    this.data.orgTypes = orgTypeString;
    this.data.industries = industryString;
    this.data.personas = personaString;
    this.data.page = page;

    this.parameters.keyword = this.data.keyword;
    this.parameters.category = this.data.categories;
    this.parameters.support = this.data.support;
    this.parameters.orgType = this.data.orgTypes;
    this.parameters.industry = this.data.industries;
    this.parameters.persona = this.data.personas;
    this.parameters.page = this.data.page;

    this._updateData = () => {
      this.$content.addClass('content-updating');
      this.$loadingIcon.playAnimation();
      this._pushDataLayer();

      let scrollPos = $(window).scrollTop();
      if (scrollPos >= this.$content.offset().top) {
        $('html, body').animate({
          scrollTop: this.$content.offset().top - 140
        });
      }

      let callback = (res) => {
        this._renderTemplate(res);
        if (res.items.length == 0) {
          this.$pagination.hide();
        } else {
          this.$pagination.show();
          this._renderPagination(res.totalpages);
        }
      };

      this.updateData({
        endpoint: this.endpoint,
        data: this.data,
        callback: callback
      });
    };

    window.onpopstate = (e) => {
      this.data.keyword = e.state.keyword;
      this.data.categories = e.state.keyword;
      this.data.support = e.state.support;
      this.data.orgTypes = e.state.orgTypes;
      this.data.industries = e.state.industries;
      this.data.personas = e.state.personas;
      this.data.page = e.state.page;

      this.parameters.keyword = this.data.keyword;
      this.parameters.category = this.data.categories;
      this.parameters.support = this.data.support;
      this.parameters.orgType = this.data.orgTypes;
      this.parameters.industry = this.data.industries;
      this.parameters.persona = this.data.personas;
      this.parameters.page = this.data.page;

      queries = this.getUrlQueries();

      categoryString = this.setupCheckboxes('category', queries, this.filters);
      industryString = this.setupCheckboxes('industry', queries, this.filters);
      orgTypeString = this.setupCheckboxes('orgType', queries, this.filters);
      personaString = this.setupCheckboxes('persona', queries, this.filters);

      $search.val(this.data.keyword);

      $support.val(this.data.support);
      $support.trigger('change.customSelect');

      this._updateFilters();
      this._updateData();
    };

    /* Add listener */
    this.addCheckboxListener('category', 'categories', this.filters, this._updateData);
    this.addCheckboxListener('industry', 'industries', this.filters, this._updateData);
    this.addCheckboxListener('orgType', 'orgTypes', this.filters, this._updateData);
    this.addCheckboxListener('persona', 'personas', this.filters, this._updateData);

    $search.on('keypress', e => {
      if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        this.data.keyword = $search.val();
        this.parameters.keyword = this.data.keyword;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
        $('.btn-pill', '.filters-group__header').trigger('click');
      }
    });

    $search.parent().find('.btn-submit').on('click', e => {
      e.preventDefault();
      this.data.keyword = $search.val();
      this.parameters.keyword = this.data.keyword;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
      $('.btn-pill', '.filters-group__header').trigger('click');
    });

    $support.on('change', e => {
      this.data.support = $support.val();
      this.parameters.support = this.data.support;
      this.data.page = 1;
      this.parameters.page = 1;
      /* to reset the data to ALL when switching from one support to another support */
      this.data.personas = 'all';
      this.data.industries = 'all';
      this.data.categories = 'all';
      this.data.orgTypes = 'all';
      this.parameters.persona = this.data.personas;
      this.parameters.industry = this.data.industries;
      this.parameters.category = this.data.categories;
      this.parameters.orgType = this.data.orgTypes;

      this._updateFilters(true);
      this._updateData();
      this.updateURL();
    });

    // -- NO SUBGROUP OPTION FOR PROGRAMME LISTING AT THE MOMENT--
    /* get all checked values */
    /*
      - loop through all checkboxes and find out if there is any subgroup
      - store the subgroup field name into array
      - add checkbox listener to the subgroup checkboxes
      - concatenate all subgroup field values with main field value
    */
    /* $categories.map((i, ele) => {
      let $this = $(ele);
      let $parent = $this.parent();
      if ($parent.find('.filters-group__subgroup').length) {
        let subgroupName = $parent.find('.filters-group__subgroup').find('input[type="checkbox"]').attr('name');
        this.addCheckboxListener(subgroupName, 'categories', this.filters, this._updateData, $this.attr('name'));
      }
    }); */
    //this.data.categories = this.updateCheckboxValues($categories, this.filters);
    //this.parameters.category = this.data.categories;

    /* $industries.map((i, ele) => {
      let $this = $(ele);
      let $parent = $this.parent();
      if ($parent.find('.filters-group__subgroup').length) {
        let subgroupName = $parent.find('.filters-group__subgroup').find('input[type="checkbox"]').attr('name');
        this.addCheckboxListener(subgroupName, 'industries', this.filters, this._updateData, $this.attr('name'));
      }
    }); */
    this.data.industries = this.updateCheckboxValues($industries, this.filters);
    this.parameters.industry = this.data.industries;
    this.data.orgTypes = this.updateCheckboxValues($orgTypes, this.filters);
    this.parameters.orgType = this.data.orgTypes;
    this.data.personas = this.updateCheckboxValues($personas, this.filters);
    this.parameters.persona = this.data.personas;
    this.data.categories = this.updateCheckboxValues($categories, this.filters);
    this.parameters.category = this.data.categories;
    
    this._updateFilters();

    /* Make call upon page load */
    this._updateData();
    this.updateURL(true);
  }

  _renderTemplate(data) {
    let dotTemplate = dot.template(this.$template)(data.items);

    this.$list.html(dotTemplate);

    setTimeout(() => {
      this.$content.addClass('content-loaded');
      this.$content.removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);
  }

  _renderPagination(totalpages) {
    let callback = () => {
      this.$pagination.find('a').map((i, ele) => {
        let $this = $(ele);

        $this.on('click', e => {
          e.preventDefault();
          this.data.page = $this.data('page');
          this.parameters.page = this.data.page;

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }

  _updateFilters(isResetFilters = false) {
    /*
      - type: VIEW
      - to be used when the Support data (I am a...) is updated
      - will show/hide the Persona and Industry filters based on the selection of Support (I am a...)
    */

    if (isResetFilters) {
      $('input[name="persona"]').prop('checked', false);
      $('input[name="persona"][value="all"]').prop('checked', true);
      $('input[name="industry"]').prop('checked', false);
      $('input[name="industry"][value="all"]').prop('checked', true);
      $('input[name="category"]').prop('checked', false);
      $('input[name="category"][value="all"]').prop('checked', true);
      $('input[name="orgType"]').prop('checked', false);
      $('input[name="orgType"][value="all"]').prop('checked', true);
    }

    /* Show / Hide respective 'Type of Support' and persona options based on selected support option */
    $('input[data-profile]').parent('li').hide();
    $('input[data-profile]').map((i, ele)=> {
      let relatedProfiles = $(ele).data('profile').split('|');
      for (let j = 0; j < relatedProfiles.length; j++) {
        if (relatedProfiles[j] == this.data.support) {
          $(ele).parent('li').show();
          break;
        }
      }
    });

    /* Show / Hide child filters based on selected support option*/
    let filterToShow = (this.data.support == 'individuals' || this.data.support == 'students') ? 'persona' : 'industry';
    let filterToHide = (this.data.support == 'individuals' || this.data.support == 'students') ? 'industry' : 'persona';
    let $filterToShow = $('input[name="' + filterToShow + '"]').closest('.filters-group__field');
    let $filterToHide = $('input[name="' + filterToHide + '"]').closest('.filters-group__field');

    $filterToHide.addClass('hidden');
    setTimeout(() => {
      $filterToHide.hide();
      $filterToShow.show(0, () => {
        $filterToShow.removeClass('hidden');
      });
    }, 250);

    /* Show / Hide Organisation Type field */
    let $orgTypeField = this.$orgTypes.closest('.filters-group__field');
    if (this.data.support == 'individuals' || this.data.support == 'students') {
      $orgTypeField.addClass('hidden');
      setTimeout(() => {
        $orgTypeField.hide();
      }, 250);
    } else {
      $orgTypeField.show(0, () => {
        $orgTypeField.removeClass('hidden');
      });
    }  
  }

  _pushDataLayer() {
    let dataLayerModel = {};
    let filterValues = '';
    let dataProfile = this.parameters.support;

    switch (dataProfile) {
      case 'All': 
      case 'organisations':
      case 'all':
        filterValues = `[${this.parameters.keyword ? this.parameters.keyword + '|' : ''}${this.parameters.orgType === 'all' || this.parameters.orgType === 'All' ? 'businessType all|' : this.parameters.orgType ? this.parameters.orgType + '|' : ''}${this.parameters.industry === 'all' || this.parameters.orgType === 'All' ? 'industryType all|' : this.parameters.industry ? this.parameters.industry + '|' : ''}${this.parameters.support === 'all' || this.parameters.support === 'All' ? 'profileType all|' : this.parameters.support ? this.parameters.support + '|' : ''}${this.parameters.category === 'all' || this.parameters.category === 'All' ? 'supportType all' : this.parameters.category ? this.parameters.category : ''}]`;
        dataLayerModel = {
          'event': 'historyChange-v2',
          'keyword': `[${this.parameters.keyword ? this.parameters.keyword : ''}]`,
          'businessType': `[${this.parameters.orgType ? this.parameters.orgType : ''}]`,
          'industryType': `[${this.parameters.industry ? this.parameters.industry : ''}]`,
          'profileType': `[${this.parameters.support ? this.parameters.support : ''}]`,
          'supportType': `[${this.parameters.category ? this.parameters.category : ''}]`,
          'filters': filterValues ? filterValues : ''
        }
        break;

      case 'students':
      case 'individuals':
        filterValues = `[${this.parameters.keyword ? this.parameters.keyword + '|' : ''}${this.parameters.support === 'all' || this.parameters.support === 'All' ? 'profileType all|' : this.parameters.support ? this.parameters.support + '|' : ''}${this.parameters.persona === 'all' || this.parameters.persona === 'All' ? 'subProfileType all|' : this.parameters.persona ? this.parameters.persona + '|' : ''}${this.parameters.category === 'all' || this.parameters.category === 'All' ? 'supportType all' : this.parameters.category ? this.parameters.category : ''}]`;
        dataLayerModel = {
          'event': 'historyChange-v2',
          'keyword': `[${this.parameters.keyword ? this.parameters.keyword : ''}]`,
          'profileType': `[${this.parameters.support ? this.parameters.support : ''}]`,
          'subprofileType': `[${this.parameters.persona ? this.parameters.persona : ''}]`,
          'supportType': `[${this.parameters.category ? this.parameters.category : ''}]`,
          'filters': filterValues ? filterValues : ''
        }
        break;
    
      default:
        break;
    }

    window.dataLayer = window.dataLayer || [];
    dataLayer.push(
      dataLayerModel
    );
  }
}

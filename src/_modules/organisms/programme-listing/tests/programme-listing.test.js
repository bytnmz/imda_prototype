'use strict';

import ProgrammeListing from '../programme-listing';

describe('ProgrammeListing View', function() {

  beforeEach(() => {
    this.programmeListing = new ProgrammeListing();
  });

  it('Should run a few assertions', () => {
    expect(this.programmeListing).toBeDefined();
  });

});

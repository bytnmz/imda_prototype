'use strict';

export default class AboutUsIntro {
  constructor($selector, selfInit = true) {
    this.name = 'about-us-intro';
    console.log('about-us-intro module');

    if(selfInit) this.init($selector);
  }

  init($selector) {

  }
}


import $ from 'jquery';
import 'slick-carousel';

export default class CarouselBanner {

  constructor() {

    let sliderSpeed = $('.carousel-banner').data('slider-speed') || 5000;

    let noOfBanners = $('.banner-slide').length;

    if (noOfBanners === 1) {
      $('.slider-nav').hide();
    } else {
      $('.slider-nav').show();
    }

    $('.banner-slider', '.carousel-banner').slick({
      autoplay: true,
      autoplaySpeed: sliderSpeed,
      adaptiveHeight: true,
      prevArrow:$('.slider-nav--prev'),
      nextArrow:$('.slider-nav--next')
    });
  }
}

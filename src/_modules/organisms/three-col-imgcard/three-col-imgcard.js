'use strict';

import { initCarousel } from '../../../_scripts/helpers';

export default class ThreeColImgcard {
  constructor($selector, selfInit = true, config) {
    const { breakpoints } = config;
    this.$selector = $selector;
    this.slickInit = false;
    this.carouselOptions = {
      dots: true,
      infinite: true,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      mobileFirst: true,
      customPaging: (slick, i) => {
        return `<button type="button"><span class="dot-indicator"></span><span class="dot-page">0${i + 1}</span></button>`;
      }
    }

    if(selfInit) this.init();
  }

  init() {
    const carousel = initCarousel($('[data-carousel]', this.$selector), this.carouselOptions, false);

    if($(window).width() < 1024 && !this.slickInit) {
      carousel.slickInit();
      this.slickInit = true;
    }

    let timer;
    $(window).on('resize', () => {
      clearTimeout(timer);

      timer = setTimeout(() => {
        if($(window).width() >= 1024 && this.slickInit) {
          carousel.unslick();
          this.slickInit = false;
        }
        
        if($(window).width() < 1024 && !this.slickInit) {
          carousel.slickInit();
          this.slickInit = true;
        }
      }, 200);
    });
  }
}

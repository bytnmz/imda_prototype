'use strict';

import $ from 'jquery';

export default class SgdsMasthead {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    $('.sgds-masthead-button', this.$selector).on('click', (e) => {
      e.preventDefault();

      if(!$('.sgds-masthead-identify-icon', this.$selector).hasClass('open')) {
        // close
        $('.sgds-masthead-identify-icon', this.$selector).addClass('open');
        $('.sgds-masthead-button', this.$selector).attr('aria-expanded', 'false');
        $('.sgds-masthead-content', this.$selector).addClass('hide');
      }
      else {
        // open
        $('.sgds-masthead-identify-icon', this.$selector).removeClass('open');
        $('.sgds-masthead-button', this.$selector).attr('aria-expanded', 'true');
        $('.sgds-masthead-content', this.$selector).removeClass('hide');
      }
    });
  }
}

'use strict';

import StandardContent from '../standard-content';

describe('StandardContent View', function() {

  beforeEach(() => {
    this.standardContent = new StandardContent();
  });

  it('Should run a few assertions', () => {
    expect(this.standardContent).toBeDefined();
  });

});

'use strict';

import $ from 'jquery';
import dot from 'dot';
import 'jquery-match-height';
import ListingBase from '../../../_scripts/listingBase.js';
import ListingFilter from '../../molecules/listing-filter/listing-filter';
import LoadingIcon from '../../atoms/loading-icon/loading-icon';

export default class DigitalsolListing extends ListingBase  {
  constructor(config) {
    let $digitalsolListing = $('.digitalsol-listing');
    let $filters = $('.digitalsol-listing__filters', $digitalsolListing);
    let $content = $('.digitalsol-listing__content', $digitalsolListing);
    let $pagination = $('.digitalsol-listing__pagination', $digitalsolListing);
    let $list = $('.digitalsol-listing__list', $digitalsolListing);
    let $item = $('.digitalsol-listing__item');

    let dataModel = {
      keyword: '',
      solutions: '',
      provider: '',
      page: 1
    };

    let parametersModel = {
      keyword: '',
      solutions: '',
      provider: '',
      page: 1
    };

    super($pagination, dataModel, parametersModel);

    this.config = config;

    let filters = new ListingFilter($filters);
    // Setup basic
    this.$template = $('#digitalsolListingTemplate').html();
    this.$content = $content;
    this.$list = $list;
    this.endpoint = $digitalsolListing.data('endpoint');
    this.$item = $item;
    this.$loadingIcon = new LoadingIcon($('.loading-icon', $digitalsolListing));

    // Define fields
    let $search = $('#keyword', $filters);
    let $solutions = $('#solutions', $filters);
    let $provider = $('#provider', $filters);

    // get all parameters value
    let queries = this.getUrlQueries();

    let page = 1;
    if (queries['page']) {
      page = decodeURIComponent(queries['page']);
    }

    if(queries['keyword']) {
      $search.val(decodeURIComponent(queries['keyword']));
    }

    if (queries['solutions']) {
      $solutions.val(queries['solutions']);
      $solutions.trigger('change.customSelect');
    }

    if (queries['provider']) {
      $provider.val(queries['provider']);
      $provider.trigger('change.customSelect');
    }

    // Setup Data Structure
    this.data.keyword = $search.val();
    this.data.solutions = $solutions.val();
    this.data.provider = $provider.val();
    this.data.page = page;

    this.parameters.keyword = this.data.keyword;
    this.parameters.solutions = this.data.solutions;
    this.parameters.provider = this.data.provider;
    this.parameters.page = this.data.page;

    this._updateData = () => {
      this.$content.addClass('content-updating');
      this.$loadingIcon.playAnimation();

      let scrollPos = $(window).scrollTop();
      if (scrollPos >= this.$content.offset().top) {
        $('html, body').animate({
          scrollTop: this.$content.offset().top
        });
      }

      let callback = (res) => {
        this._renderTemplate(res);
        if (res.items.length == 0) {
          this.$pagination.hide();
        } else {
          this.$pagination.show();
          this._renderPagination(res.totalpages);
        }
      };

      this.updateData({
        endpoint: this.endpoint,
        data: this.data,
        callback: callback
      });
    };

    window.onpopstate = (e) => {
      this.data.keyword = e.state.keyword;
      this.data.solutions = e.state.solutions;
      this.data.provider = e.state.provider;
      this.data.page = e.state.page;

      this.parameters.keyword = this.data.keyword;
      this.parameters.solutions = this.data.solutions;
      this.parameters.provider = this.data.provider;
      this.parameters.page = this.data.page;

      $search.val(this.data.keyword);

      $solutions.val(this.data.solutions);
      $solutions.trigger('change.customSelect');

      $provider.val(this.data.provider);
      $provider.trigger('change.customSelect');

      this._updateData();
    };

    // Add listener
    let checkingTimeout;
    $search.on('keypress', e => {
      if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        this.data.keyword = $search.val();
        this.parameters.keyword = this.data.keyword;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
      }
    });

    $search.parent().find('.btn-submit').on('click', e => {
      e.preventDefault();
      this.data.keyword = $search.val();
      this.parameters.keyword = this.data.keyword;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });

    $solutions.on('change', e => {
      this.data.solutions = $solutions.val();
      this.parameters.solutions = this.data.solutions;
      this.data.page = 1;
      this.parameters.page = 1;
      this._updateData();
      this.updateURL();
    });

    $provider.on('change', e => {
        this.data.provider = $provider.val();
        this.parameters.provider = this.data.provider;
        this.data.page = 1;
        this.parameters.page = 1;
        this._updateData();
        this.updateURL();
      });

    // Make call upon page load
    this._updateData();
    this.updateURL(true);

  }

  // _updateData() {
  //   this.$content.addClass('content-updating');

  //   let scrollPos = $(window).scrollTop();
  //   if (scrollPos >= this.$content.offset().top) {
  //     $('html, body').animate({
  //       scrollTop: this.$content.offset().top
  //     });
  //   }

  //   let callback = (res) => {
  //     this._renderTemplate(res);
  //     if (res.items.length == 0) {
  //       this.$pagination.hide();
  //     } else {
  //       this.$pagination.show();
  //       this._renderPagination(res.totalpages);
  //     }
  //   };

  //   this.updateData({
  //     endpoint: this.endpoint,
  //     data: this.data,
  //     callback: callback
  //   });
  // }

  _renderTemplate(data) {
    let { breakpoints } = this.config;

    let dotTemplate = dot.template(this.$template)(data.items);

    this.$list.html(dotTemplate);

    setTimeout(() => {
      this.$content.addClass('content-loaded');
      this.$content.removeClass('content-updating');
      this.$loadingIcon.restartFrame();
    }, 250);

    setTimeout(() => {
      $('.digitalsol-listing__item > .panel-card').matchHeight({
        byRow: true
      });
    }, 350);
  }

  _renderPagination(totalpages) {
    let callback = () => {
      this.$pagination.find('a').map((i, ele) => {
        let $this = $(ele);

        $this.on('click', e => {
          e.preventDefault();
          this.data.page = $this.data('page');
          this.parameters.page = this.data.page;

          this._updateData();
          this.updateURL();
        });
      });
    };

    this.pagination.update(this.data.page, totalpages, callback);
  }
}

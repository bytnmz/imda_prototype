'use strict';

import DigitalsolListing from '../digitalsol-listing';

describe('DigitalsolListing View', function() {

  beforeEach(() => {
    this.digitalsolListing = new DigitalsolListing();
  });

  it('Should run a few assertions', () => {
    expect(this.digitalsolListing).toBeDefined();
  });

});

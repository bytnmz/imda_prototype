'use strict';

export default class Introduction {
  constructor() {
    this.name = 'introduction';
    console.log('%s module', this.name.toLowerCase());
  }
}

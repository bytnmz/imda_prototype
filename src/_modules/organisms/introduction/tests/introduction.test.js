'use strict';

import Introduction from '../introduction';

describe('Introduction View', function() {

  beforeEach(() => {
    this.introduction = new Introduction();
  });

  it('Should run a few assertions', () => {
    expect(this.introduction).toBeDefined();
  });

});

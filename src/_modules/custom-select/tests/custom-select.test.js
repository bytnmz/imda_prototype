'use strict';

import CustomSelect from '../custom-select';

describe('CustomSelect View', function() {

  beforeEach(() => {
    this.customSelect = new CustomSelect();
  });

  it('Should run a few assertions', () => {
    expect(this.customSelect).toBeDefined();
  });

});

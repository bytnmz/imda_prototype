<?php
  $category = $_GET['categories'];
  $industry = $_GET['industries'];
  $support = $_GET['support'];

  $totalPages = 10;
  if ($category !== "") {
    $categories = explode("|", $category);
  } else {
    $category = "all";
  }

  if ($industry !== "") {
    $industries = explode("|", $industry);
  } else {
    $industry = "all";
  }

  $categoriesMap = array(
    "programmes" => "programmes",
    "grants" => "grants",
    "scholarships" => "scholarships"
  );

  $supportsMap = array(
    "for individuals" => "individuals",
    "for organisations" => "organisations"
  );

  $industriesMap = array(
    "media" => "media",
    "telecommunications" => "telecommunications",
    "technology" => "technology",
    "broadcast" => "broadcast",
    "newspaper & print" => "newspaper",
    "postal" => "postal"
  );

  $items = array(
    array(
      "url" => "/industry-support-detail",
      "title" => "CODE@SG Movement - Developing Computational Thinking As A National Capability",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Logistics",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "senior",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Infocomm Clubs Programme",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Industry Preparation for Pre-graduate (iPREP) Programme",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Development Assistance",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "GoSecure",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "ICT SMEs Capability Development",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Enable IT Programme",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "student",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Home Access",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "senior",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "NEU PC Plus Programme",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "student",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Enable IT Programme",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "student",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Silver Infocomm Initiative",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "senior",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Digital TV Assistance Scheme",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "senior",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "PlayMaker",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "senior",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Digital Maker",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Programmes",
      "industry" => "Finance",
      "icon" => "icon-individual",
      "target" => "For Individuals",
      "persona" => "senior",
      "color" => "rose"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "National Cybersecurity Postgraduate Scholarship",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Scholarships",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "green"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "SG:D Scholarship (Polytechnic)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Scholarships",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "green"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "SG:D Scholarship (Undergraduate)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Scholarships",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "green"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "SG:D Scholarship (Postgraduate)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Scholarships",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "green"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Research Grants",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Grants",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "orange"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Overseas Participation Grant",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Grants",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "orange"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Southeast Asia Co-production Grant",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Grants",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "orange"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "Short Film Grant",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Grants",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "orange"
    ),
    array(
      "url" => "/industry-support-detail",
      "title" => "New Talent Feature Grant",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Grants",
      "industry" => "Finance",
      "icon" => "icon-organisation",
      "target" => "For Organisations",
      "color" => "orange"
    )
  );

  $itemArray = array();

  if ($category == 'all') {
    $i = 0;
    foreach ($items as $item) {
      if ($i >= 10) {
        break;
      }
      if ($supportsMap[strtolower($item['target'])] == $support) {
        array_push($itemArray, $item);
        $i++;
      }
    }
  } else {
    $i = 0;
    foreach ($items as $item) {
      if ($i >= 10) {
        break;
      }

      $index = array_search($categoriesMap[strtolower($item['category'])], $categories);

      if ($index !== false && ($supportsMap[strtolower($item['target'])] == $support || $support == 'all')) {
        array_push($itemArray, $item);
        $i++;
      }
    }
  }


  header('Content-type:application/json;charset=utf-8');
  echo json_encode(array("totalpages" => $totalPages, "items" => $itemArray));

?>

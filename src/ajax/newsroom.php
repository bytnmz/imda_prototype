<?php
  $type = $_GET['type'];
  $totalPages = 10;

  $typesMap = array(
    "media releases" => "media",
    "speeches" => "speeches"
  );

  $items = array(
    array(
      "url" => "/article-page",
      "title" => "IMDA Singtel and DBS launch new platform to strengthen SMEs omni-channel capabilities",
      "type" => "Media Releases",
      "date" => "14 Jan 2019",
      "description" => "New 99%SME platform enables retailers to access new markets and business insights, last-mile delivery options and stronger customer engagement features"
    ),
    array(
      "url" => "/article-page",
      "title" => "New Singapore Digital Scholarship offers global exposure and more",
      "type" => "Speeches",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "Best of the Best",
      "type" => "Media Releases",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "What you need to know about the Singapore Media Festival 2018",
      "type" => "Speeches",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "Insider Secrets of the AI Escape Bus Challenge",
      "type" => "Speeches",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "The AI Future is a game",
      "type" => "Media Releases",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "The AI Future is a game",
      "type" => "Media Releases",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "What you need to know about the Singapore Media Festival 2018",
      "type" => "Speeches",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "Insider Secrets of the AI Escape Bus Challenge",
      "type" => "Media Releases",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "IMDA Singtel and DBS launch new platform to strengthen SMEs omni-channel capabilities",
      "type" => "Media Releases",
      "date" => "14 Jan 2019",
      "description" => "New 99%SME platform enables retailers to access new markets and business insights, last-mile delivery options and stronger customer engagement features"
    ),
    array(
      "url" => "/article-page",
      "title" => "New Singapore Digital Scholarship offers global exposure and more",
      "type" => "Speeches",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "Best of the Best",
      "type" => "Media Releases",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "What you need to know about the Singapore Media Festival 2018",
      "type" => "Speeches",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "Insider Secrets of the AI Escape Bus Challenge",
      "type" => "Speeches",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "The AI Future is a game",
      "type" => "Media Releases",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "The AI Future is a game",
      "type" => "Media Releases",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "What you need to know about the Singapore Media Festival 2018",
      "type" => "Speeches",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    ),
    array(
      "url" => "/article-page",
      "title" => "Insider Secrets of the AI Escape Bus Challenge",
      "type" => "Media Releases",
      "date" => "15 Jan 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, reiciendis."
    )
  );

  $itemArray = array();

  if ($type == 'all') {
    foreach ($items as $key=>$item) {
      if ($key >= 10) {
        break;
      }
      array_push($itemArray, $item);
    }
  } else {
    $i = 0;
    foreach ($items as $item) {
      if ($i >= 10) {
        break;
      }

      if ($typesMap[strtolower($item['type'])] == $type) {
        array_push($itemArray, $item);
        $i++;
      }
    }
  }

  header('Content-type:application/json;charset=utf-8');
  echo json_encode(array("totalpages" => $totalPages, "items" => $itemArray));

?>

<?php
  $category = $_GET['categories'];
  $industry = $_GET['industries'];
  $totalPages = 10;
  if ($category != "") {
    $categories = explode("|", $category);
  } else {
    $category = "all";
  }

  if ($industry != "") {
    $industries = explode("|", $industry);
  } else {
    $industry = "all";
  }

  $categoriesMap = array(
    "acts and regulations" => "regulations",
    "standards and guidelines" => "standards",
    "codes of practice" => "codes",
    "policies and frameworks" => "policies",
    "licenses" => "licenses",
    "enforcement decisions" => "enforcement"
  );

  $industriesMap = array(
    "media" => "media",
    "telecommunications" => "telecommunications",
    "technology" => "technology",
    "broadcast" => "broadcast",
    "newspaper & print" => "newspaper",
    "postal" => "postal",
    "retail" => "retail",
    "arts" => "arts",
    "films" => "films",
    "internet" => "internet"
  );

  $items = array(
    array(
      "url" => "/regulation-detail",
      "title" => "Info-communications Media Development Authority Act",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Films",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Telecom Competition Code",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Telecommunications",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Unmanned Aircraft System Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Technology",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Equipment Registration Framework",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for The Provision of Arts Entertainment",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Films",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Broadcasting Act (Cap. 28)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Broadcast",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "The Electronic Transactions Act",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Technology",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Films Act (Cap. 107)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Films",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Newspaper and Printing Presses Act (Cap. 206)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Newspaper & Print",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Personal Data Protection Act",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Films",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Postal Services Act (Cap. 237A)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Postal",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Public Entertainments Act (Cap. 257)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Films",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Undesirable Publications Act (Cap. 338)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Films",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Telecommunications Act (Cap. 323)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Telecommunications",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "The Spam Control Act (Cap. 311A)",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Films",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Remote Gambling Act: Clarifications on the Scope of Social Games",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Acts and Regulations",
      "industry" => "Films",
      "color" => "rose"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Telecommunication Cybersecurity Code of Practice",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Telecommunications",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Postal Codes of Practice and Notifications",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Postal",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Internal Wiring",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Code of Practice for Info-communication Facilities in Buildings",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Code of Practice for Provision of Premium Rate Services",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Next Generation Nationwide Broadband Network NetCo Interconnection",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Content Code for Nationwide Managed Transmission Linear Television",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Content Code for Over-the-Top, Video-on-Demand and Niche Services",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Television and Radio Advertising and Sponsorship Code",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Code of Practice for Television Broadcast Standards",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Free-To-Air Radio Programme Code",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Codes of Practice",
      "industry" => "Films",
      "color" => "blue"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Accounting Separation Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Deployment and Repair of Submarine Cable Systems",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Dispute Resolution Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Guidelines for Service Provisioning over the Next Generation Nationwide Broadband Network",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Telecom Competition Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Telecom Consolidation and Tender Offer Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Reclassification and Exemption Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Contract Period and Early Termination Charges Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Fixed Number Portability Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Film Classification Guidelines",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "CODE@SG Movement - Developing Computational Thinking As A National Capability",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for the Provision of Broadcasting Services",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for the Distribution/Exhibition of Films",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence/Permit for Newspaper and Printing Press",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for The Provision of Telecommunication Services",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for The Operation of Radio-communication Station Network",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for The Sale of Telecommunication Equipment",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for The Installation of Internal Telecommunication Wiring",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for Provision of Service of Detecting Underground Telecommunication Cable",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for Provision of Postal Services",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Licence for use of Satellite Orbital Slot",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "National Cybersecurity Postgraduate Scholarship",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Films",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "National Infocomm Competition NIC",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Standards and Guidelines",
      "industry" => "Arts",
      "color" => "orange"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Spectrum Management and Coordination",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Numbering",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Interconnection and Access",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Competition Management",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Nationwide Broadband Network",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Postal",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Content Regulation",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Past public and industry consultation papers",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Policies and Frameworks",
      "industry" => "Films",
      "color" => "purple"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Media Manpower Plan",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Licenses",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Lorem ipsum dolor sit amet",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Enforcement Decisions",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Lorem ipsum dolor sit amet",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Enforcement Decisions",
      "industry" => "Internet",
      "color" => "green"
    ),
    array(
      "url" => "/regulation-detail",
      "title" => "Lorem ipsum dolor sit amet",
      "topics" => array(
        array(
          "url" => "#",
          "name" => "Digital Economy"
        ),
        array(
          "url" => "#",
          "name" => "Startups"
        )
      ),
      "description" => "A Smart Nation of the future, with a rich array of tech products and services, will require different skills. We will need people familiar with tech skills and who are also sensitive to how tech can be applied to improve living.",
      "category" => "Enforcement Decisions",
      "industry" => "Internet",
      "color" => "green"
    )
  );

  $tempItems = array();
  $itemArray = array();

  if ($category == 'all') {
    $i = 0;
    if ($industry == 'all') {
      foreach ($items as $key=>$item) {
        if ($i > 10) {
          break;
        }
        array_push($itemArray, $item);
        $i++;
      }
    } else {
      foreach ($items as $key=>$item) {
        if ($i > 10) {
          break;
        }

        $index = array_search($industriesMap[strtolower($item['industry'])], $industries);

        if ($index !== false) {
          array_push($itemArray, $item);
          $i++;
        }
      }
    }
  } else {
    $i = 0;
    foreach ($items as $item) {
      if ($i > 10) {
        break;
      }

      $index = array_search($categoriesMap[strtolower($item['category'])], $categories);

      if ($index !== false) {
        if ($industry == 'all') {
          array_push($itemArray, $item);
          $i++;
        } else {
          $industryIndex = array_search($industriesMap[strtolower($item['industry'])], $industries);
          if ($industryIndex !== false) {
            array_push($itemArray, $item);
            $i++;
          }
        }
      }
    }
  }


  header('Content-type:application/json;charset=utf-8');
  echo json_encode(array("totalpages" => $totalPages, "items" => $itemArray));

?>

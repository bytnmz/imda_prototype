<?php
  $type = 'solutions';
  $totalPages = 10;

  $items = array(
    array(
      "title" => "Automating manufacturing process with digitalisation",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-1.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "green-yellow"
    ),
    array(
      "title" => "Redeveloping advertising analytics with blockchain",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-2.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "blue-green"
    ),
    array(
      "title" => "Robotising the system for the restaurant",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-3.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "purple-red"
    ),
    array(
      "title" => "Automating manufacturing process with digitalisation",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-4.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "green-yellow"
    ),
    array(
      "title" => "Face recognition system for Changi Airport in 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-5.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "red-orange"
    ),
    array(
      "title" => "Redeveloping advertising analytics with blockchain",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-6.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "green-yellow"
    ),
    array(
      "title" => "VI Dimension Pte Ltd",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-4.jpg",
      "company" => "Blockchain",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "green-yellow"
    ),
    array(
      "title" => "iVideoSmart Pte Ltd",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-5.jpg",
      "company" => "Robotic",
      "industry" => "Robotic",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "red-orange"
    ),
    array(
      "title" => "Taiger Singapore Pte Ltd",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-6.jpg",
      "company" => "Cybersecurity",
      "industry" => "Cybersecurity",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "green-yellow"
    ),
    array(
      "title" => "Singapore Airline Pte Ltd",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-1.jpg",
      "company" => "Aviation",
      "industry" => "Aviation",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "green-yellow"
    ),
    array(
      "title" => "Lion Transport Pte Ltd",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-2.jpg",
      "company" => "Blockchain",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "blue-green"
    ),
    array(
      "title" => "Amasing Robot Pte Ltd",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-3.jpg",
      "company" => "Robotic",
      "industry" => "Robotic",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "purple-red"
    ),
    array(
      "title" => "Automating manufacturing process with digitalisation",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-1.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "green-yellow"
    ),
    array(
      "title" => "Redeveloping advertising analytics with blockchain",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-2.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "blue-green"
    ),
    array(
      "title" => "Face recognition system for Changi Airport in 2019",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-5.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "solutions",
      "color" => "red-orange"
    ),
    array(
      "title" => "Automating manufacturing process with digitalisation",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-4.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "green-yellow"
    ),
    array(
      "title" => "Redeveloping advertising analytics with blockchain",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-6.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "green-yellow"
    ),
    array(
      "title" => "Robotising the system for the restaurant",
      "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit perspiciatis aut fugiat illo dolor, vel aliquam asperiores maiores obcaecati quos!",
      "image" => "/images/stories/story-3.jpg",
      "company" => "VI Dimension Pte Ltd - ARVAS",
      "industry" => "Blockchain",
      "url" => "/supported-digital-detail",
      "type" => "companies",
      "color" => "purple-red"
    )
  );

  $itemArray = array();

  if ($type == 'all') {
    foreach ($items as $key=>$item) {
      if ($key >= 6) {
        break;
      }
      array_push($itemArray, $item);
    }
  } else {
    $i = 0;
    foreach ($items as $item) {
      if ($i >= 6) {
        break;
      }

      if ($item['type'] == $type) {
        array_push($itemArray, $item);
        $i++;
      }
    }
  }


  header('Content-type:application/json;charset=utf-8');
  echo json_encode(array("totalpages" => $totalPages, "items" => $itemArray));

?>
